package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Contant type that can be used
 * <li>{@link #FRIEND}</li> ;
 * <li>{@link #FRIEND_REQUEST}</li> ;
 * <li>{@link #BLACK_LIST}</li> ;
 * <li>{@link #NONE}</li> .
 */
@XmlType(name = "сontactType")
public enum ContactType {
    /**
     * Type is friend.
     */
    @XmlEnumValue("FRIEND")
    FRIEND,
    /**
     * Type is friend request.
     */
    @XmlEnumValue("FRIEND_REQUEST")
    FRIEND_REQUEST,
    /**
     * Type is black list.
     */
    @XmlEnumValue("BLACK_LIST")
    BLACK_LIST,
    /**
     * Type is none.
     */
    @XmlEnumValue("NONE")
    NONE
}
