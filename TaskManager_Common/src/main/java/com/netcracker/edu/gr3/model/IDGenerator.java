package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class "IDGenerator" is used to generate a sequence of unique values
 * with the ability to specify the initial value and the sequence step.
 *
 * @author Katherine Denisova, Olga Sudakova, Vitalia Tihonova
 * @version 1.0.0
 */
@XmlRootElement(name = "generator")
@XmlAccessorType(XmlAccessType.FIELD)
public class IDGenerator {
    /**
     * Last value of the generated id in sequence.
     * At the initial stage, the last id is assigned the starting value
     * passed as a method parameter.
     */
    @XmlElement(name = "lastId")
    private long lastId;
    /**
     * Step by which the last number in the sequence will increase
     * to generate the next id.
     */
    @XmlElement(name = "increment")
    private long increment;

    /**
     * Default constructor - сreates a new object with default start value for
     * {@link IDGenerator#lastId} and {@link IDGenerator#increment}.
     */
    public IDGenerator() {
        this.lastId = 0;
        this.increment = 1;
    }

    /**
     * Constructor - creates a new object with user-defined values for
     * {@link IDGenerator#lastId} and {@link IDGenerator#increment}.
     *
     * @param startId   - initial value of the sequence
     * @param increment - sequence increment
     */
    public IDGenerator(long startId, long increment) {
        this.lastId = startId - increment;
        this.increment = increment;
    }

    /**
     * Method to get field value {@link IDGenerator#lastId}.
     *
     * @return Last value of the generated ID in sequence
     */
    public long getLastId() {
        return lastId;
    }

    /**
     * Method for determining a last value of the generated ID in sequence
     * {@link IDGenerator#lastId}.
     *
     * @param lastId - last value of the generated ID in sequence
     */
    public void setLastId(long lastId) {
        this.lastId = lastId;
    }

    /**
     * Method to get field value {@link IDGenerator#increment}.
     *
     * @return Sequence increment
     */
    public long getIncrement() {
        return increment;
    }

    /**
     * Method for determining a sequence increment
     * {@link IDGenerator#increment}.
     *
     * @param increment - sequence increment
     */
    public void setIncrement(long increment) {
        this.increment = increment;
    }

    /**
     * Method of generating a unique object id.
     *
     * @return Unique object id
     */
    public long generateID() {
        lastId += increment;
        return lastId;
    }
}

