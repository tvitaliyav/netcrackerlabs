/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.util.adapters;

import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Class "ClassAdapter" converts objects of type Class to XML and vice versa.
 * Inherited from XmlAdapter.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public class ClassAdapter extends XmlAdapter<String, Class> {

    /**
     * Overriding the method XmlAdapter.unmarshal(ValueType v) for getting an
     * object of type Class from an XML file.
     *
     * @param v - string from xml file
     * @return Object of type Class
     * @throws TaskManagerException if failed to load object from file
     */
    @Override
    public Class unmarshal(String v) throws TaskManagerException {
        try {
            return Class.forName(v);
        } catch (ClassNotFoundException ex) {
            throw new TaskManagerException(ErrorsCodes
                    .CLASS_NOT_FOUND_EXCEPTION);
        }
    }

    /**
     * Overriding the method XmlAdapter.marshal(BoundType v) to convert an
     * object of type Class to an xml file.
     *
     * @param v - object of type Class
     * @return Object of type Class converted to a string
     * @throws TaskManagerException if failed to converted object to a string
     */
    @Override
    public String marshal(Class v) throws TaskManagerException {
        return v.getName();
    }
}
