/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.observer;

/**
 * Interface "Observable" to represent the observed object.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public interface Observable {

    /**
     * This method adds an observer to the set of observers for this object,
     * provided that it is not the same as some observer already in the set.
     *
     * @param observer - observer
     */
    void addObserver(Observer observer);

    /**
     * This method deletes an observer from the set of observers of this object.
     *
     * @param observer - observer
     */
    void removeObserver(Observer observer);

    /**
     * This method notifies all observers of changes in object state.
     *
     * @param event - ObserverEvent
     */
    void notifyObservers(ObserverEvent event);
}
