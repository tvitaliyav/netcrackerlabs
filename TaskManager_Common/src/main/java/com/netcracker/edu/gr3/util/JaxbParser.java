/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.util;

import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.model.Contact;
import com.netcracker.edu.gr3.model.ContactType;
import com.netcracker.edu.gr3.model.ContactGroup;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.model.Event;
import com.netcracker.edu.gr3.model.Permission;
import com.netcracker.edu.gr3.model.Task;
import com.netcracker.edu.gr3.model.TaskPriority;
import com.netcracker.edu.gr3.model.TaskStatus;
import com.netcracker.edu.gr3.model.User;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.netcracker.edu.gr3.observer.ObserverEvent;
import org.apache.log4j.Logger;

/**
 * Class "JaxbParser" allows you to store and retrieve data in memory in any XML
 * format.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public final class JaxbParser {

    /**
     * JAXBContext instance for creating serializers and deserializers.
     */
    private static JAXBContext context = null;
    /**
     * Static variable for marshalling Java objects in XML.
     */
    private static Marshaller marshaller = null;
    /**
     * Static variable for demoralization from XML back to Java object.
     */
    private static Unmarshaller unmarshaller = null;
    /**
     * Property is LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(JaxbParser.class);

    /**
     * Default private constructor - сreates a new object.
     */
    private JaxbParser() {
        LOGGER.info("Create JaxbParser object");
    }

    /**
     * Method to get field value {@link JaxbParser#context}.
     *
     * @return JAXBContext instance for creating serializers and deserializers
     * @throws TaskManagerException if failed to get JaxbContext
     */
    private static JAXBContext getContext() throws TaskManagerException {
        try {
            LOGGER.info("Method getContext called");
            if (context == null) {
                LOGGER.info("Context not initialized");
                context = JAXBContext.newInstance(Calendar.class, Contact.class,
                        ContactType.class, ContactGroup.class, Event.class,
                        Permission.class, Task.class, TaskPriority.class,
                        TaskStatus.class, User.class, DataStore.class,
                        ObserverEvent.class);
                LOGGER.info("Context has been initialized");
            }
            return context;
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .JAXB_CONTEXT_INITIALIZTION_ERROR);
        } finally {
            LOGGER.info("Method getContext end");
        }
    }

    /**
     * Method to get field value {@link JaxbParser#marshaller}.
     *
     * @return Object for marshalling Java objects in XML
     * @throws TaskManagerException if failed to get marshall
     */
    private static Marshaller getMarshaller() throws TaskManagerException {
        try {
            LOGGER.info("Method getMarshaller called");
            if (marshaller == null) {
                LOGGER.info("Marshaller not initialized");
                marshaller = getContext().createMarshaller();
                LOGGER.info("Marshaller has been initialized");
            }
            return marshaller;
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .MARSHALLER_INITIALIZATION_ERROR);
        } finally {
            LOGGER.info("Method getMarshaller end");
        }

    }

    /**
     * Method to get field value {@link JaxbParser#unmarshaller}.
     *
     * @return Object for demoralization from XML back to Java object
     * @throws TaskManagerException if failed to get unmarshall
     */
    private static Unmarshaller getUnmarshaller() throws TaskManagerException {
        try {
            LOGGER.info("Method getUnmarshaller called");
            if (unmarshaller == null) {
                LOGGER.info("Unmarshaller not initialized");
                unmarshaller = getContext().createUnmarshaller();
                LOGGER.info("Unmarshaller has been initialized");
            }
            return unmarshaller;
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .UNMARSHALLER_INITIALIZATION_ERROR);
        } finally {
            LOGGER.info("Method getUnmarshaller end");
        }
    }

    /**
     * Method to save an object to an XML file.
     *
     * @param pathFile - path to file
     * @param object   - object that you want to save the file
     * @throws TaskManagerException if failed to save object to file
     */
    public static void saveToFile(String pathFile, Object object)
            throws TaskManagerException {
        try {
            LOGGER.info("Method saveToFile called");
            getMarshaller().marshal(object, new FileWriter(pathFile));
            LOGGER.info("Method saveToFile end");
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .JAXB_SAVE_TO_FILE_EXCEPTION);
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .JAXB_SAVE_TO_FILE_EXCEPTION);
        }
    }

    /**
     * Method to load from XML back to Java object.
     *
     * @param <T>         is type object loaded from file.
     * @param pathFile    - path to file
     * @param classObject - class of object that you want to load from file
     * @return Object loaded from file
     * @throws TaskManagerException if failed to load object from file
     */
    public static <T> T loadFromFile(String pathFile, Class<T> classObject)
            throws TaskManagerException {
        try {
            LOGGER.info("Method loadFromFile called");
            return (T) getUnmarshaller().unmarshal(new FileReader(pathFile));
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes.DATA_FILE_NOT_EXISTS);
        } catch (JAXBException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .JAXB_LOAD_FROM_FILE_EXCEPTION);
        } finally {
            LOGGER.info("Method loadFromFile end");
        }
    }
}
