/**
 * Package to implement the "Observer" design pattern.
 * Package classes are required to monitor the state of objects in the system.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 * @since 1.0
 */
package com.netcracker.edu.gr3.observer;
