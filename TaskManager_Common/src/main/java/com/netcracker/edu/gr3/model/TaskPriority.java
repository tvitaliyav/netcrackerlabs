package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

 /**
 * Task priority that can be used
 * <li>{@link #LOW}</li> ;
 * <li>{@link #NORMAL}</li> ;
 * <li>{@link #MAJOR}</li> ;
 * <li>{@link #CRITICAL}</li> ;
 * <li>{@link #BLOCKER}</li> .
 */
@XmlType(name = "taskPriority")
public enum TaskPriority {
    /**
     * Priority is low.
     */
    @XmlEnumValue("LOW")
    LOW,
    /**
     * Priority is major.
     */
    @XmlEnumValue("NORMAL")
    NORMAL,
    /**
     * Priority is major.
     */
    @XmlEnumValue("MAJOR")
    MAJOR,
    /**
     * Priority is critical.
     */
    @XmlEnumValue("CRITICAL")
    CRITICAL,
    /**
     * Priority is blocker.
     */
    @XmlEnumValue("BLOCKER")
    BLOCKER
}
