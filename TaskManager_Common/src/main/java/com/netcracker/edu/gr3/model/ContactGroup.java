package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class "ContactGroup" defines the group to which the user belongs in contacts.
 * <b>contactGroupId</b> , <b>name</b>, <b>userId</b>
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
@XmlRootElement(name = "сontactGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactGroup {

    /**
     * Unique contact group ID of type long.
     */
    @XmlAttribute(name = "id")
    private long contactGroupId;
    /**
     * Group name of type String.
     */
    @XmlElement(name = "name")
    private String name;
    /**
     * Unique id of the user who is a member of the group of type long.
     */
    @XmlElement(name = "userId")
    private long userId;

    /**
     * Default constructor - сreates a new object.
     */
    public ContactGroup() {
    }

    /**
     * Method to get field value {@link #contactGroupId}.
     *
     * @return Returns an unique contact group ID
     */
    public long getContactGroupId() {
        return contactGroupId;
    }

    /**
     * Method for determining an unique contact group ID
     * {@link #contactGroupId}.
     *
     * @param contactGroupId - unique contact group ID
     */
    public void setContactGroupId(long contactGroupId) {
        this.contactGroupId = contactGroupId;
    }

    /**
     * Method to get field value {@link #name}.
     *
     * @return Returns a group name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method for determining a group name {@link #name}.
     *
     * @param name - group name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to get field value {@link #userId}.
     *
     * @return Returns an unique id of the user who is a member of the group.
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Method for determining an unique id of the user {@link #userId}.
     *
     * @param userId - unique id of the user who is a member of the group
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Overriding the method Object.toString().
     *
     * @return Returns the string representation of an contact group object
     */
    @Override
    public String toString() {
        StringBuilder finalString = new StringBuilder("ContactGroup{ "
                + "contactGroupId: ");
        return finalString.append(contactGroupId)
                .append("; Name: ")
                .append(name)
                .append("; UserId: ")
                .append(userId)
                .append("}")
                .toString();
    }

    /**
     * Overriding the method Object.hashCode() - allows to get a unique whole
     * number for this object.
     *
     * @return Returns an integer
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + (int) contactGroupId
                + name.hashCode()
                + (int) userId;
        return result;
    }

    /**
     * Overriding the method Object.equals() defines the equivalence relation of
     * objects.
     *
     * @param obj - the object with which you want to compare this object
     * @return Returns true - if two instances of an object are equal, false -
     * if two instances of an object are not equal
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        final ContactGroup guest = (ContactGroup) obj;
        return this.contactGroupId == guest.contactGroupId
                && this.name == guest.name
                && this.userId == guest.userId;
    }
}
