/**
 * This package includes classes-adapters for adapting a bound type to
 * a value type or vice versa. The methods of classes are invoked
 * by the JAXB binding framework during marshaling and unmarshalling.
 */
package com.netcracker.edu.gr3.util.adapters;
