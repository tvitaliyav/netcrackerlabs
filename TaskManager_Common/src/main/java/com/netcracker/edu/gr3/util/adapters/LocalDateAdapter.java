/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.util.adapters;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import java.time.LocalDate;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Class "LocalDateAdapter" converts objects of type LocalDate to XML and vice
 * versa. Inherited from XmlAdapter.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    /**
     * Overriding the method XmlAdapter.unmarshal(ValueType v) for getting an
     * object of type LocalDate from an XML file.
     *
     * @param v - string from xml file
     * @return Object of type LocalDate
     * @throws TaskManagerException if failed to load object from file
     */
    @Override
    public LocalDate unmarshal(String v) throws TaskManagerException {
        return LocalDate.parse(v);
    }

    /**
     * Overriding the method XmlAdapter.marshal(BoundType v) to convert an
     * object of type LocalDate to an xml file.
     *
     * @param v - object of type LocalDate
     * @return Object of type LocalDate converted to a string
     * @throws TaskManagerException if failed to converted object to a string
     */
    @Override
    public String marshal(LocalDate v) throws TaskManagerException {
        return v.toString();
    }

}
