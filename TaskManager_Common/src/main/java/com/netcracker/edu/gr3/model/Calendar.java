package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * This class is needed to storing Calendar models with properties.
 * <b>calendarId</b> , <b>name</b> and <b>userId</b>
 *
 * @author Denisova Ekaterina
 * @version 1.0.
 */

@XmlRootElement(name = "сalendar")
@XmlAccessorType(XmlAccessType.FIELD)
public class Calendar {
    /**
     * Property is calendarId.
     */
    @XmlAttribute(name = "id")
    private long calendarId;
    /**
     * Property is name.
     */
    @XmlElement(name = "name")
    private String name;
    /**
     * Property is userId.
     *
     * @see User .
     */
    @XmlElement(name = "user_id")
    private long userId;

    /**
     * Create new object.
     *
     * @see Calendar#Calendar()
     * @see Calendar#Calendar(String, long).
     */
    public Calendar() {
    }

    /**
     * Create new object.
     *
     * @param name   is calendar name(type)
     * @param userId is id User object
     * @see User
     * @see Calendar#Calendar()
     * @see Calendar#Calendar(String, long).
     */
    public Calendar(String name, long userId) {
        this.name = name;
        this.userId = userId;
    }

    /**
     * This method gets the value of the property calendarId
     * that can be set using the {@link #setCalendarId(long)}.
     *
     * @return the value outputs as a long.
     */
    public long getCalendarId() {
        return calendarId;
    }

    /**
     * This method sets the value of the calendarId property
     * that can be obtained using the method {@link #getCalendarId()}.
     *
     * @param calendarId is calendarId Calendar object.
     */
    public void setCalendarId(long calendarId) {
        this.calendarId = calendarId;
    }

    /**
     * This method gets the value of the property name
     * that can be set using the {@link #setName(String)}.
     *
     * @return the value outputs as a String.
     */
    public String getName() {
        return name;
    }

    /**
     * This method sets the value of the calendarId property
     * that can be obtained using the method {@link #getName()}.
     *
     * @param name is name Calendar object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method gets the value of the property userId
     * that can be set using the {@link #setUserId(long)}.
     *
     * @return the value outputs as a long.
     */
    public long getUserId() {
        return userId;
    }

    /**
     * This method sets the value of the calendarId property
     * that can be obtained using the method {@link #getUserId()}.
     *
     * @param userId is calendarId User object
     * @see User .
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * The method does override a method toString declared in a supertype.
     *
     * @return a string representation of the object Calendar.
     */
    @Override
    public String toString() {
        StringBuilder finalString = new StringBuilder();
        return finalString.append("Calendar{ Name: ")
                .append(name).append("; User: ")
                .append(userId)
                .append("}")
                .toString();
    }

    /**
     * The method does override a method hashCode declared in a supertype.
     *
     * @return a unique int of the object Calendar.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) calendarId;
        return result;
    }

    /**
     * The method does override a method equals declared in a supertype.
     *
     * @return a value of checking equality of two objects.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        final Calendar guest = (Calendar) obj;
        return this.calendarId == guest.calendarId
                && this.name.equals(guest.name)
                && this.userId == guest.userId;
    }
}
