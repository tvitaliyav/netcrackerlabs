/**
 * This package contains Models classes used for pattern MVC.
 *
 * @author Ekaterina Denisova
 * @version 1.0.0
 * @since 1.0
 */

package com.netcracker.edu.gr3.model;
