/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.util.adapters;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Class "LocalDateAdapter" converts objects of type LocalDateTime to XML and
 * vice versa. Inherited from XmlAdapter.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    /**
     * Overriding the method XmlAdapter.unmarshal(ValueType v) for getting an
     * object of type LocalDateTime from an XML file.
     *
     * @param v - string from xml file
     * @return Object of type LocalDateTime
     * @throws TaskManagerException if failed to load object from file
     */
    @Override
    public LocalDateTime unmarshal(String v) throws TaskManagerException {
        return LocalDateTime.parse(v);
    }

    /**
     * Overriding the method XmlAdapter.marshal(BoundType v) to convert an
     * object of type LocalDateTime to an xml file.
     *
     * @param v - object of type LocalDateTime
     * @return Object of type LocalDateTime converted to a string
     * @throws TaskManagerException if failed to converted object to a string
     */
    @Override
    public String marshal(LocalDateTime v) throws TaskManagerException {
        return v.toString();
    }

}
