package com.netcracker.edu.gr3.model;

import com.netcracker.edu.gr3.util.adapters.DurationAdapter;
import com.netcracker.edu.gr3.util.adapters.LocalDateTimeAdapter;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Class "Task" defines user`s task.
 *
 * @author Ekatherina Denisova, Olga Sudakova, Vitalia Tihonova
 * @version 1.0.0
 */
@XmlRootElement(name = "task")
@XmlAccessorType(XmlAccessType.FIELD)
public class Task {

    /**
     * Unique task ID of type long.
     */
    @XmlAttribute(name = "id")
    private long taskId;
    /**
     * Unique user ID of type long.
     */
    @XmlElement(name = "userId")
    private long userId;
    /**
     * Task status of type TaskStatus.
     */
    @XmlElement(name = "status")
    private TaskStatus status;
    /**
     * Task priority of type TaskPriority.
     */
    @XmlElement(name = "priority")
    private TaskPriority priority;
    /**
     * Task name of type String.
     */
    @XmlElement(name = "name")
    private String name;
    /**
     * Task description of type String.
     */
    @XmlElement(name = "description")
    private String description;
    /**
     * Task start date description of type LocalDateTime.
     */
    @XmlElement(name = "startDate")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime startDate;
    /**
     * Task end date description of type LocalDateTime.
     */
    @XmlElement(name = "endDate")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime endDate;
    /**
     * Task due date description of type LocalDateTime.
     */
    @XmlElement(name = "dueDate")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime dueDate;
    /**
     * Task lead time description of type LocalDateTime.
     */
    @XmlElement(name = "estimationExecutionTime")
    @XmlJavaTypeAdapter(value = DurationAdapter.class)
    private Duration estimationExecutionTime;
    /**
     * Unique parent task ID of type long.
     */
    @XmlElement(name = "parentId")
    private long parentId;

    /**
     * Default constructor - сreates a new empty object with task status NEW.
     */
    public Task() {
        status = TaskStatus.NEW;
    }

    /**
     * Method to get field value {@link Task#taskId}.
     *
     * @return Returns a unique task ID
     */
    public long getTaskId() {
        return taskId;
    }

    /**
     * Method for determining a unique user ID {@link Task#taskId}.
     *
     * @param taskId - unique task ID
     */
    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    /**
     * Method to get field value {@link Task#userId}.
     *
     * @return Returns a unique user ID
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Method for determining a unique user ID {@link Task#userId}.
     *
     * @param userId - unique user ID
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Method to get field value {@link Task#status}.
     *
     * @return Returns a task status
     */
    public TaskStatus getStatus() {
        return status;
    }

    /**
     * Method for determining a unique user ID {@link Task#status}.
     *
     * @param status - task status
     */
    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    /**
     * Method to get field value {@link Task#priority}.
     *
     * @return Returns a task priority
     */
    public TaskPriority getPriority() {
        return priority;
    }

    /**
     * Method for determining a unique user ID {@link Task#priority}.
     *
     * @param priority - task status
     */
    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    /**
     * Method to get field value {@link Task#name}.
     *
     * @return Returns a task name
     */
    public String getName() {
        return name;
    }

    /**
     * Method for determining a unique user ID {@link Task#name}.
     *
     * @param name - task name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to get field value {@link Task#description}.
     *
     * @return Returns a task description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method for determining a unique user ID {@link Task#description}.
     *
     * @param description - task description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Method to get field value {@link Task#startDate}.
     *
     * @return Returns a task start date
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }

    /**
     * Method for determining a unique user ID {@link Task#startDate}.
     *
     * @param startDate - task start date
     */
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * Method to get field value {@link Task#endDate}.
     *
     * @return Returns a task end date
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }

    /**
     * Method for determining a unique user ID {@link Task#endDate}.
     *
     * @param endDate - task end date
     */
    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    /**
     * Method to get field value {@link Task#dueDate}.
     *
     * @return Returns a task due date
     */
    public LocalDateTime getDueDate() {
        return dueDate;
    }

    /**
     * Method for determining a unique user ID {@link Task#dueDate}.
     *
     * @param dueDate - task due date
     */
    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Method to get field value {@link Task#estimationExecutionTime}.
     *
     * @return Returns a task lead time
     */
    public Duration getEstimationExecutionTime() {
        return estimationExecutionTime;
    }

    /**
     * Method for determining a unique user ID
     * {@link Task#estimationExecutionTime}.
     *
     * @param estimationExecutionTime - task lead time
     */
    public void setEstimationExecutionTime(Duration estimationExecutionTime) {
        this.estimationExecutionTime = estimationExecutionTime;
    }

    /**
     * Method to get field value {@link Task#parentId}.
     *
     * @return Returns a unique parent task ID
     */
    public long getParentId() {
        return parentId;
    }

    /**
     * Method for determining a unique user ID {@link Task#parentId}.
     *
     * @param subtaskList - unique parent task ID
     */
    public void setParentId(long subtaskList) {
        this.parentId = subtaskList;
    }

    /**
     * Overriding the method Object.toString().
     *
     * @return Returns the string representation of an task object
     */
    @Override
    public String toString() {
        return new StringBuilder("Task ").append(taskId)
                .append("  for ").append(userId).append(": ")
                .append(name).append(" ").append(description)
                .append(" ").append(status.toString())
                .append(" with priority: ").append(priority
                        .toString()).append(". Subtask list: ")
                .append(parentId).toString();
    }

    /**
     * Overriding the method Object.equals() defines the equivalence relation of
     * objects.
     *
     * @param object - the object with which you want to compare this object
     * @return Returns true - if two instances of an object are equal, false -
     * if two instances of an object are not equal
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        Task task = (Task) object;
        if (this.taskId == task.taskId && this.name.equals(task.name)
                && this.description.equals(task.description)
                && this.status.equals(task.status)
                && this.priority.equals(task.priority)
                && this.endDate.equals(task.endDate)
                && this.startDate.equals(task.startDate)
                && this.dueDate.equals(task.dueDate)
                && this.estimationExecutionTime
                .equals(task.estimationExecutionTime)
                && this.parentId == task.parentId) {
            return true;
        }
        return false;
    }

    /**
     * Overriding the method Object.hashCode() - allows to get a unique whole
     * number for this object.
     *
     * @return Returns an integer
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + name.hashCode()
                + (int) parentId
                + (int) taskId + (int) userId + status.hashCode()
                + priority.hashCode();
        return result;
    }
}
