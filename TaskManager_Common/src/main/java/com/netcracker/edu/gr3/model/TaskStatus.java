package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Task status that can be used
 * <li>{@link #NEW}</li> ;
 * <li>{@link #PLANNING}</li> ;
 * <li>{@link #PLANNED}</li> ;
 * <li>{@link #IN_PROGRESS}</li> ;
 * <li>{@link #ON_HOLD}</li> ;
 * <li>{@link #DONE}</li> ;
 * <li>{@link #NOT_DONE}</li> .
 */

@XmlType(name = "taskStatus")
public enum TaskStatus {
    /**
     * Status is new.
     */
    @XmlEnumValue("NEW")
    NEW,
    /**
     * Status is planning.
     */
    @XmlEnumValue("PLANNING")
    PLANNING,
    /**
     * Status is planned.
     */
    @XmlEnumValue("PLANNED")
    PLANNED,
    /**
     * Status is in progress.
     */
    @XmlEnumValue("IN_PROGRESS")
    IN_PROGRESS,
    /**
     * Status is on hold.
     */
    @XmlEnumValue("ON_HOLD")
    ON_HOLD,
    /**
     * Status is done.
     */
    @XmlEnumValue("DONE")
    DONE,
    /**
     * Status is not done.
     */
    @XmlEnumValue("NOT_DONE")
    NOT_DONE
}

