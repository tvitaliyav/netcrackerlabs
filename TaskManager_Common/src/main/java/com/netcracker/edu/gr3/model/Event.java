package com.netcracker.edu.gr3.model;

import com.netcracker.edu.gr3.util.adapters.LocalDateAdapter;
import com.netcracker.edu.gr3.util.adapters.LocalDateTimeAdapter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Class "Event" defines user-assigned event.
 *
 * @author Katherine Denisova, Olga Sudakova, Vitalia Tihonova
 * @version 1.0.0
 */
@XmlRootElement(name = "Event")
@XmlAccessorType(XmlAccessType.FIELD)
public class Event {

    /**
     * Unique event ID of type long.
     */
    @XmlAttribute(name = "id")
    private long eventId;
    /**
     * Title of event of type String.
     */
    @XmlElement(name = "titleEvent")
    private String titleEvent;
    /**
     * Description of event of type String.
     */
    @XmlElement(name = "description")
    private String description;
    /**
     * Unique calendar ID calendar to which the event belongs of type long.
     */
    @XmlElement(name = "calendarId")
    private long calendarId;
    /**
     * Event start date and time of type LocalDateTime.
     */
    @XmlElement(name = "beginDateTime")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime beginDateTime;
    /**
     * Event end date and time of type LocalDateTime.
     */
    @XmlElement(name = "endDateTime")
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime endDateTime;
    /**
     * Interval showing after how many days the event is repeated.
     */
    @XmlElement(name = "intervalRepetition")
    private int intervalRepetition;
    /**
     * Number of days to repeat the event. How many times to repeat the event.
     */
    @XmlElement(name = "numberRepetition")
    private int numberRepetition;
    /**
     * Until what date to repeat the event.
     */
    @XmlElement(name = "endDateRepetition")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    private LocalDate endDateRepetition;
    /**
     * Unique parent event ID of type long.
     */
    @XmlElement(name = "event_id")
    private long parentEventId;
    /**
     * Сron expression to automatically trigger an event at a specific time.
     */
    private String cron;

    /**
     * Default constructor - сreates a new empty object.
     */
    public Event() {

    }

    /**
     * Method to get field value {@link Event#eventId}.
     *
     * @return Returns an unique event ID
     */
    public long getEventId() {
        return eventId;
    }

    /**
     * Method for determining a unique event ID {@link Event#eventId}.
     *
     * @param eventId - unique event ID
     */
    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    /**
     * Method to get field value {@link Event#titleEvent}.
     *
     * @return Returns a title of event
     */
    public String getTitleEvent() {
        return titleEvent;
    }

    /**
     * Method for determining a title of event {@link Event#titleEvent}.
     *
     * @param titleEvent - title of event
     */
    public void setTitleEvent(String titleEvent) {
        this.titleEvent = titleEvent;
    }

    /**
     * Method to get field value {@link Event#description}.
     *
     * @return Returns a description of event
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method for determining a description of event {@link Event#description}.
     *
     * @param description - description of event
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Method to get field value {@link Event#calendarId}.
     *
     * @return Returns an unique calendar ID calendar to which the event belongs
     */
    public long getCalendarId() {
        return calendarId;
    }

    /**
     * Method for determining an unique calendar ID calendar to which the event
     * belongs {@link Event#calendarId}.
     *
     * @param calendarId - an unique calendar ID calendar to which the event
     * belongs
     */
    public void setCalendarId(long calendarId) {
        this.calendarId = calendarId;
    }

    /**
     * Method to get field value {@link Event#beginDateTime}.
     *
     * @return Returns an event start date and time
     */
    public LocalDateTime getBeginDateTime() {
        return beginDateTime;
    }

    /**
     * Method for determining an event start time {@link Event#beginDateTime}.
     *
     * @param beginDateTime - event start date and time
     */
    public void setBeginDateTime(LocalDateTime beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    /**
     * Method to get field value {@link Event#endDateTime}.
     *
     * @return Returns an event end date and time
     */
    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    /**
     * Method for determining an event end date and time
     * {@link Event#endDateTime}.
     *
     * @param endDateTime - event end date and time
     */
    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    /**
     * Method to get field value {@link Event#intervalRepetition}.
     *
     * @return Returns an interval showing after how many days the event is
     * repeated
     */
    public int getIntervalRepetition() {
        return intervalRepetition;
    }

    /**
     * Method for determining an interval repetition of event
     * {@link Event#intervalRepetition}.
     *
     * @param intervalRepetition - interval showing after how many days the
     * event is repeated
     */
    public void setIntervalRepetition(int intervalRepetition) {
        this.intervalRepetition = intervalRepetition;
    }

    /**
     * Method to get field value {@link Event#numberRepetition}.
     *
     * @return Returns a number of days to repeat the event
     */
    public int getNumberRepetition() {
        return numberRepetition;
    }

    /**
     * Method for determining a number of days to repeat the event
     * {@link Event#numberRepetition}.
     *
     * @param numberRepetition - number of days to repeat the event
     */
    public void setNumberRepetition(int numberRepetition) {
        this.numberRepetition = numberRepetition;
    }

    /**
     * Method to get field value {@link Event#endDateRepetition}.
     *
     * @return Returns a date until which the event will be repeated
     */
    public LocalDate getEndDateRepetition() {
        return endDateRepetition;
    }

    /**
     * Method for determining a date until which the event will be repeated
     * {@link Event#endDateRepetition}.
     *
     * @param endDateRepetition - date until which the event will be repeated
     */
    public void setEndDateRepetition(LocalDate endDateRepetition) {
        this.endDateRepetition = endDateRepetition;
    }

    /**
     * Method to get field value {@link Event#parentEventId}.
     *
     * @return Returns a parent event ID
     */
    public long getParentEventId() {
        return parentEventId;
    }

    /**
     * Method for determining a parent event ID {@link Event#parentEventId}.
     *
     * @param parentEventId - parent event ID
     */
    public void setParentEventId(long parentEventId) {
        this.parentEventId = parentEventId;
    }

    /**
     * Method to get field value {@link Event#cron}.
     *
     * @return Returns a cron expression
     */
    public String getCron() {
        return cron;
    }

    /**
     * Method for determining a cron expression {@link Event#cron}.
     *
     * @param cron - cron expression
     */
    public void setCron(String cron) {
        this.cron = cron;
    }

    /**
     * Overriding the method Object.toString().
     *
     * @return Returns the string representation of an event object
     */
    @Override
    public String toString() {
        return "Event { "
                + "title " + titleEvent
                + ", description " + description
                + ", calendar id " + calendarId
                + ", begin time " + beginDateTime
                + ", end time " + endDateTime
                + ", interval of repetition " + intervalRepetition
                + ", number of repetition " + numberRepetition
                + ", end date of repetition " + endDateRepetition
                + ", parent event id" + parentEventId
                + '}';
    }

    /**
     * Overriding the method Object.hashCode() - allows to get a unique whole
     * number for this object.
     *
     * @return Returns an integer
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + (int) eventId;
        result = prime * result + (int) titleEvent.hashCode();
        result = prime * result + (int) calendarId;
        result = prime * result + (int) beginDateTime.hashCode();
        result = prime * result + (int) endDateTime.hashCode();
        result = prime * result + (int) intervalRepetition;
        result = prime * result + (int) numberRepetition;

        return result;
    }

    /**
     * Overriding the method Object.equals() defines the equivalence relation of
     * objects.
     *
     * @param o - the object with which you want to compare this object
     * @return Returns true - if two instances of an object are equal, false -
     * if two instances of an object are not equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final Event objEvent = (Event) o;

        return this.eventId == objEvent.eventId
                && this.titleEvent.equals(objEvent.titleEvent)
                && this.description.equals(objEvent.description)
                && this.calendarId == objEvent.calendarId
                && this.beginDateTime.equals(objEvent.beginDateTime)
                && this.endDateTime.equals(objEvent.endDateTime)
                && this.intervalRepetition == objEvent.intervalRepetition
                && this.numberRepetition == objEvent.numberRepetition
                && this.endDateRepetition.equals(objEvent.endDateRepetition)
                && this.parentEventId == objEvent.parentEventId;
    }

}
