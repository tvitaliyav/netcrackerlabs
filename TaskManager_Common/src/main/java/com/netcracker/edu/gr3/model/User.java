package com.netcracker.edu.gr3.model;

import com.netcracker.edu.gr3.util.adapters.LocalDateAdapter;
import java.time.LocalDate;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Class "User" defines user of calendar.
 *
 * @author Katherine Denisova, Olga Sudakova, Vitalia Tihonova
 * @version 1.0.0
 */
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    /**
     * Unique user ID of type long.
     */
    @XmlAttribute(name = "id")
    private long userId;

    /**
     * Username of type String.
     */
    @XmlElement(name = "login")
    private String login;

    /**
     * User password of type String.
     */
    @XmlElement(name = "password")
    private String password;

    /**
     * User name of type String.
     */
    @XmlElement(name = "firstName")
    private String firstName;

    /**
     * User surname of type String.
     */
    @XmlElement(name = "lastName")
    private String lastName;

    /**
     * User's date of birth of type LocalDate.
     */
    @XmlElement(name = "dateOfBirth")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    private LocalDate dateOfBirth;

    /**
     * User phone number of type String.
     */
    @XmlElement(name = "phoneNumber")
    private String phoneNumber;

    /**
     * User email address of type String.
     */
    @XmlElement(name = "email")
    private String email;

    /**
     * Link to the calendar id that will be used by the default user
     * in his task manager.
     */
    @XmlElement(name = "calendarId")
    private long calendarId;

    /**
     * Default constructor - сreates a new empty object.
     */
    public User() {
    }

    /**
     * Method to get field value {@link User#userId}.
     *
     * @return Returns an unique user ID
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Method for determining an unique user ID {@link User#userId}.
     *
     * @param userId - unique user ID
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Method to get field value {@link User#login}.
     *
     * @return Returns a username.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Method for determining a username {@link User#login}.
     *
     * @param login - username
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Method to get field value {@link User#password}.
     *
     * @return Returns a user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Method for determining a user password {@link User#password}.
     *
     * @param password - user password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Method to get field value {@link User#firstName}.
     *
     * @return Returns a user name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Method for determining a user name {@link User#firstName}.
     *
     * @param firstName - user name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Method to get field value {@link User#lastName}.
     *
     * @return Returns a user surname
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Method for determining a user surname {@link User#lastName}.
     *
     * @param lastName - user surname
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Method to get field value {@link User#dateOfBirth}.
     *
     * @return Returns a user's date of birth
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Method for determining a user's date of birth {@link User#dateOfBirth}.
     *
     * @param dateOfBirth - user's date of birth
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Method to get field value {@link User#phoneNumber}.
     *
     * @return Returns a user phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Method for determining a user phone number {@link User#phoneNumber}.
     *
     * @param phoneNumber - user phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Method to get field value {@link User#email}.
     *
     * @return Returns a user email address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Method for determining a user email address {@link User#email}.
     *
     * @param email - user email address
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to get field value {@link User#calendarId}.
     *
     * @return Returns a default calendar that the user uses
     * in their task manager
     */
    public long getCalendarId() {
        return calendarId;
    }

    /**
     * Method for determining a user default calendar {@link User#calendarId}.
     *
     * @param calendarId - default calendar that the user uses
     * in their task manager
     */
    public void setCalendarId(long calendarId) {
        this.calendarId = calendarId;
    }

    /**
     * Overriding the method Object.toString().
     *
     * @return Returns the string representation of an user object
     */
    @Override
    public String toString() {
        return "User{"
                + "FirstName: " + firstName
                + ", LastName: " + lastName
                + ", Date of birth: " + dateOfBirth
                + ", Phone number " + phoneNumber
                + ", e-mail: " + email
                + '}';
    }

    /**
     * Overriding the method Object.equals() defines the equivalence relation
     * of objects.
     *
     * @param o - the object with which you want to compare this object
     * @return Returns true - if two instances of an object are equal,
     * false - if two instances of an object are not equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User objUser = (User) o;
        return this.userId == objUser.userId
                && this.login.equals(objUser.login)
                && this.password.equals(objUser.password)
                && this.firstName.equals(objUser.firstName)
                && this.lastName.equals(objUser.lastName)
                && this.dateOfBirth.equals(objUser.dateOfBirth)
                && this.phoneNumber.equals(objUser.phoneNumber)
                && this.email.equals(objUser.email)
                && this.calendarId == objUser.calendarId;
    }

    /**
     * Overriding the method Object.hashCode() -
     * allows to get a unique whole number for this object.
     *
     * @return Returns an integer
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) userId;
        result = prime * result + login.hashCode();
        result = prime * result + password.hashCode();
        result = prime * result + firstName.hashCode();
        result = prime * result + lastName.hashCode();
        result = prime * result + dateOfBirth.hashCode();
        result = prime * result + phoneNumber.hashCode();
        result = prime * result + email.hashCode();
        result = prime * result + (int) calendarId;
        return result;
    }
}

