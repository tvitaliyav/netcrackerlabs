package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * This class is needed to storing Contact models with properties.
 * <b>fromUserId</b> , <b>toUserId</b>, <b>contactType</b> and
 * <b>contactGroupId</b>
 *
 * @author Denisova Ekaterina
 * @version 1.0.
 */
@XmlRootElement(name = "сontact")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact {

    /**
     * Property is fromUserId.
     */
    @XmlElement(name = "fromUserId")
    private long fromUserId;
    /**
     * Property is toUserId.
     */
    @XmlElement(name = "toUserId")
    private long toUserId;
    /**
     * Property is contactType.
     *
     * @see ContactType .
     */
    @XmlElement(name = "contactType")
    private ContactType contactType;
    /**
     * Property is contactGroup.
     */
    @XmlElement(name = "contactGroupId")
    private long contactGroupId;

    /**
     * Create new object.
     *
     * @see Contact#Contact()
     * @see Contact#Contact(long, long, ContactType)
     * @see Contact#Contact(long, long).
     */
    public Contact() {
        this(0, 0, ContactType.NONE);
    }

    /**
     * Create new object.
     *
     * @param fromUserId  is the user ID from which the contact originates
     * @param toUserId    is the user ID that the contact reaches
     * @param contactType is a type of contact between users
     * @see User
     * @see Contact#Contact()
     * @see Contact#Contact(long, long, ContactType)
     * @see Contact#Contact(long, long).
     */
    public Contact(long fromUserId, long toUserId, ContactType contactType) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.contactType = contactType;
    }

    /**
     * Create new object.
     *
     * @param fromUserId is the user ID from which the contact originates
     * @param toUserId   is the user ID that the contact reaches
     * @see User
     * @see Contact#Contact()
     * @see Contact#Contact(long, long, ContactType)
     * @see Contact#Contact(long, long).
     */
    public Contact(long fromUserId, long toUserId) {
        this(fromUserId, toUserId, ContactType.NONE);
    }

    /**
     * This method gets the value of the property contactType that can be set
     * using the {@link #setContactType(ContactType)}.
     *
     * @return the value outputs as a ContactType
     * @see ContactType .
     */
    public ContactType getContactType() {
        return contactType;
    }

    /**
     * This method sets the value of the contactType property that can be
     * obtained using the method {@link #getContactType()}.
     *
     * @param contactType is contactType Calendar object.
     */
    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    /**
     * Method to get field value {@link long#fromUserId}.
     *
     * @return Returns a unique user ID
     */
    public long getFromUserId() {
        return fromUserId;
    }

    /**
     * Method for determining a unique user ID {@link long#fromUserId}.
     *
     * @param fromUserId - unique task ID
     */
    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    /**
     * Method to get field value {@link long#toUserId}.
     *
     * @return Returns a unique user ID
     */
    public long getToUserId() {
        return toUserId;
    }

    /**
     * Method for determining a unique user ID {@link long#toUserId}.
     *
     * @param toUserId - unique task ID
     */
    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * Method to get field value {@link #contactGroupId}.
     *
     * @return Returns an id of the group to which the contact belongs.
     */
    public long getContactGroupId() {
        return contactGroupId;
    }

    /**
     * Method for determining a group to which the contact belongs
     * {@link #contactGroupId}.
     *
     * @param contactGroupId - group to which the contact belongs
     */
    public void setContactGroupId(long contactGroupId) {
        this.contactGroupId = contactGroupId;
    }

    /**
     * The method does override a method toString declared in a supertype.
     *
     * @return a string representation of the object Contact.
     */
    @Override
    public String toString() {
        StringBuilder finalString = new StringBuilder("Contact{ fromUserId: ");
        return finalString.append(fromUserId)
                .append("; toUserId: ")
                .append(toUserId)
                .append("; TypeContact: ")
                .append(contactType)
                .append("; contactGroupId: ")
                .append(contactGroupId)
                .append("}")
                .toString();
    }

    /**
     * The method does override a method hashCode declared in a supertype.
     *
     * @return a unique int of the object Contact.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + (int) toUserId
                + (int) fromUserId
                + contactType.hashCode()
                + (int) contactGroupId;
        return result;
    }

    /**
     * The method does override a method equals declared in a supertype.
     *
     * @return a value of checking equality of two objects.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        final Contact guest = (Contact) obj;
        return this.fromUserId == guest.fromUserId
                && this.toUserId == guest.toUserId
                && this.contactType == guest.contactType
                && this.contactGroupId == guest.contactGroupId;
    }
}
