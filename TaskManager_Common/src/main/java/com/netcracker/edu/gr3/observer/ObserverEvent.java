/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.observer;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElementWrapper;

import java.util.Map;

/**
 * Сlass "ObserverEvent" stores information about the observed event.
 * <b>event</b>
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
@XmlRootElement(name = "observerEvent")
@XmlAccessorType(XmlAccessType.FIELD)
public class ObserverEvent {
    /**
     * Property is event.
     */
    @XmlElementWrapper(name = "event")
    private Map<String, Object> event;

    /**
     * Default constructor - сreates a new empty object.
     */
    public ObserverEvent() {
    }

    /**
     * Method to get field value {@link #event}.
     *
     * @return Returns an object associated with the key
     */
    public Map<String, Object> getEvent() {
        return event;
    }

    /**
     * Method to determine the value of a map object {@link @link #event}.
     *
     * @param event - object associated with the key
     */
    public void setEvent(Map<String, Object> event) {
        this.event = event;
    }
}
