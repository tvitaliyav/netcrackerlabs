/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.exception;

/**
 * Class "TaskManagerException" designed to work with project errors.
 * Inherited from class "Exception".
 *
 * @author Katherine Denisova, Olga Sudakova, Vitalia Tihonova
 * @version 1.0.0
 */
public class TaskManagerException extends Exception {
    /**
     * Marking the errors through its code of type int.
     */
    private int errorCode;

    /**
     * Default constructor - сreates a new empty object by calling
     * the superclass constructor.
     */
    public TaskManagerException() {
        super();
    }

    /**
     * Сonstructor creates a new empty object by error code
     * {@link TaskManagerException#errorCode}.
     *
     * @param errorCode - unique code to identify the error
     */
    public TaskManagerException(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Сonstructor creates a new empty object by calling
     * the superclass constructor.
     *
     * @param message - error description
     */
    public TaskManagerException(String message) {
        super(message);
    }

    /**
     * Сonstructor creates a new empty object by calling
     * the superclass constructor.
     *
     * @param message - error description
     * @param cause   - cause of the error
     */
    public TaskManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Сonstructor creates a new empty object by calling
     * the superclass constructor.
     *
     * @param cause - cause of the error
     */
    public TaskManagerException(Throwable cause) {
        super(cause);
    }

    /**
     * Method to get field value {@link TaskManagerException#errorCode}.
     *
     * @return Returns a unique code to identify the error
     */
    public long getErrorCode() {
        return errorCode;
    }

    /**
     * Method for determining a unique code to identify the error
     * {@link TaskManagerException#errorCode}.
     *
     * @param errorCode - unique code to identify the error
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}


