/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.observer;

/**
 * Interface "Observer" is the observer interface,
 * in the implementation of which the object will be able
 * to receive information about changes in the observed objects.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public interface Observer {
    /**
     * Method called when the observed object changes.
     *
     * @param event - observed object
     */
    void notify(ObserverEvent event);
}
