package com.netcracker.edu.gr3.model;

import com.netcracker.edu.gr3.util.adapters.ClassAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Class "Permission" defines user`s permission.
 *
 * @author Katherine Denisova, Olga Sudakova, Vitaliya Tikhonova
 * @version 1.0.0
 */
@XmlRootElement(name = "permission")
@XmlAccessorType(XmlAccessType.FIELD)
public class Permission {
    /**
     * User`s permission of type byte.
     */
    @XmlElement(name = "permission")
    private byte permission;
    /**
     * Unique user ID of type long.
     */
    @XmlElement(name = "userId")
    private long userId;
    /**
     * Unique object ID of type long.
     */
    @XmlElement(name = "objectId")
    private long objectId;
    /**
     * Type o object of type Class.
     */
    @XmlElement(name = "objectType")
    @XmlJavaTypeAdapter(value = ClassAdapter.class)
    private Class objectType;

    /**
     * Default constructor - сreates a new empty object.
     */
    public Permission() {
    }
    /**
     * Constructor - create a new object {@link Permission#permission}.
     *
     * @param permission - user permission
     */
    public Permission(byte permission) {
        this.permission = permission;
    }

    /**
     * Method to get field value {@link Permission#permission}.
     *
     * @return Returns a user`s permission
     */
    public byte getPermission() {
        return permission;
    }

    /**
     * Method for determining a user surname {@link Permission#permission}.
     *
     * @param permission - user permission
     */
    public void setPermission(byte permission) {
        this.permission = permission;
    }

    /**
     * Method to get field value {@link Permission#objectId}.
     *
     * @return Returns a object ID
     */
    public long getObjectId() {
        return objectId;
    }

    /**
     * Method for determining a user surname {@link Permission#objectId}.
     *
     * @param objectId - object ID
     */
    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    /**
     * Method to get field value {@link Permission#userId}.
     *
     * @return Returns a user ID
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Method for determining a user surname {@link Permission#userId}.
     *
     * @param userId - user ID
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Method to get field value {@link Permission#objectType}.
     *
     * @return Returns a object type
     */
    public Class getObjectType() {
        return objectType;
    }

    /**
     * Method for determining a user surname {@link Permission#objectType}.
     *
     * @param object - object type
     */
    public void setObjectType(Class object) {
        this.objectType = object;
    }

    /**
     * Overriding the method Object.toString().
     *
     * @return Returns the string representation of an user object
     */
    public String toString() {
        String permissionString = new String(new byte[]{permission});
        return permissionString;
    }

    /**
     * Overriding the method Object.equals() defines the equivalence relation
     * of objects.
     *
     * @param object - the object with which you want to compare this object
     * @return Returns true - if two instances of an object are equal,
     * false - if two instances of an object are not equal
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        Permission permision = (Permission) object;
        if (this.permission == permision.getPermission()
                && this.objectType == permision.getObjectType()
                && this.objectId == permision.getObjectId()
                && this.userId == permision.getUserId()) {
            return true;
        }
        return false;
    }

    /**
     * Overriding the method Object.hashCode() -
     * allows to get a unique whole number for this object.
     *
     * @return Returns an integer
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + permission
                + objectType.hashCode();
        return result;
    }
}
