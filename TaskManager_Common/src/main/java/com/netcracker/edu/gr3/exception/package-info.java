/**
 * Exception classes used to determine errors through its code.
 *
 * @since 1.0
 * @author Olga Sudakova
 * @version 1.0.0
 */
package com.netcracker.edu.gr3.exception;
