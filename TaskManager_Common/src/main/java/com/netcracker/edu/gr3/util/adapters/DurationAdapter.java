/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.util.adapters;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import java.time.Duration;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Class "DurationAdapter" converts objects of type Duration to XML and vice
 * versa. Inherited from XmlAdapter.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public class DurationAdapter extends XmlAdapter<String, Duration> {

    /**
     * Overriding the method XmlAdapter.unmarshal(ValueType v) for getting an
     * object of type Duration from an XML file.
     *
     * @param v - string from xml file
     * @return Object of type Duration
     * @throws TaskManagerException if failed to load object from file
     */
    @Override
    public Duration unmarshal(String v) throws TaskManagerException {
        return Duration.parse(v);
    }

    /**
     * Overriding the method XmlAdapter.marshal(BoundType v) to convert an
     * object of type Duration to an xml file.
     *
     * @param v - object of type Duration
     * @return Object of type Duration converted to a string
     * @throws TaskManagerException if failed to converted object to a string
     */
    @Override
    public String marshal(Duration v) throws TaskManagerException {
        return v.toString();
    }

}
