package com.netcracker.edu.gr3.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * This class aggregates all classes of models and serves to store data.
 *
 * @author Ekaterina Denisova, Olga Sudakova
 * @version 1.0.0
 */
@XmlRootElement(name = "model")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataStore {

    /**
     * Property is users.
     *
     * @see User .
     */

    @XmlElementWrapper(name = "users")
    @XmlElement(name = "user")
    private Collection<User> users;
    /**
     * Property is calendars.
     *
     * @see Calendar .
     */
    @XmlElementWrapper(name = "calendars")
    @XmlElement(name = "calendar")
    private Collection<Calendar> calendars;
    /**
     * Property is events.
     *
     * @see Event .
     */
    @XmlElementWrapper(name = "events")
    @XmlElement(name = "event")
    private Collection<Event> events;
    /**
     * Property is tasks.
     *
     * @see Task .
     */
    @XmlElementWrapper(name = "tasks")
    @XmlElement(name = "task")
    private Collection<Task> tasks;
    /**
     * Property is contacts.
     *
     * @see Contact .
     */
    @XmlElementWrapper(name = "contacts")
    @XmlElement(name = "contact")
    private Collection<Contact> contacts;
    /**
     * Property is contactGroups.
     *
     * @see ContactGroup .
     */
    @XmlElementWrapper(name = "contactGroups")
    @XmlElement(name = "contactGroup")
    private Collection<ContactGroup> contactGroups;
    /**
     * Property is permissions.
     *
     * @see Permission .
     */
    @XmlElementWrapper(name = "permissions")
    @XmlElement(name = "permission")
    private Collection<Permission> permissions;
    /**
     * Property is map.
     *
     * @see Class .
     * @see IDGenerator .
     */
    @XmlElementWrapper(name = "generators")
    private Map<Class, IDGenerator> map;

    /**
     * Create new object.
     *
     * @see DataStore#DataStore().
     */
    public DataStore() {
        users = new ArrayList<>();
        calendars = new ArrayList<>();
        events = new ArrayList<>();
        tasks = new ArrayList<>();
        contacts = new ArrayList<>();
        contactGroups = new ArrayList<>();
        permissions = new ArrayList<>();
        map = new HashMap<>();
        map.put(User.class, new IDGenerator());
        map.put(ContactGroup.class, new IDGenerator());
        map.put(Calendar.class, new IDGenerator());
        map.put(Event.class, new IDGenerator());
        map.put(Task.class, new IDGenerator());
    }

    /**
     * This method gets the value of the property users that can be set using
     * the {@link #setUsers(Collection)}.
     *
     * @return the value outputs as a Collection<User>.
     * @see User .
     */
    public Collection<User> getUsers() {
        return users;
    }

    /**
     * This method sets the value of the users property that can be obtained
     * using the method {@link #getUsers()}.
     *
     * @param users is users Collection<User> object.
     */
    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    /**
     * This method gets the value of the property calendars that can be set
     * using the {@link #setCalendars(Collection)}.
     *
     * @return the value outputs as a Collection<Calendar>.
     * @see Calendar .
     */
    public Collection<Calendar> getCalendars() {
        return calendars;
    }

    /**
     * This method sets the value of the calendars property that can be obtained
     * using the method {@link #getCalendars()}.
     *
     * @param calendars is calendars Collection<Calendar> object.
     */
    public void setCalendars(Collection<Calendar> calendars) {
        this.calendars = calendars;
    }

    /**
     * This method gets the value of the property events that can be set using
     * the {@link #setEvents(Collection)}.
     *
     * @return the value outputs as a Collection<Event>.
     * @see Event .
     */
    public Collection<Event> getEvents() {
        return events;
    }

    /**
     * This method sets the value of the events property that can be obtained
     * using the method {@link #getEvents()}.
     *
     * @param events is events Collection<Event> object.
     */
    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

    /**
     * This method gets the value of the property tasks that can be set using
     * the {@link #setTasks(Collection)}.
     *
     * @return the value outputs as a Collection<Task>.
     * @see Task .
     */
    public Collection<Task> getTasks() {
        return tasks;
    }

    /**
     * This method sets the value of the tasks property that can be obtained
     * using the method {@link #getTasks()}.
     *
     * @param tasks is tasks Collection<Task> object.
     */
    public void setTasks(Collection<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * This method gets the value of the property contacts that can be set using
     * the {@link #setContacts(Collection)}.
     *
     * @return the value outputs as a Collection<Contacts>.
     * @see Contact .
     */
    public Collection<Contact> getContacts() {
        return contacts;
    }

    /**
     * This method sets the value of the contacts property that can be obtained
     * using the method {@link #getContacts()}.
     *
     * @param contacts is contacts Collection<Contact> object.
     */
    public void setContacts(Collection<Contact> contacts) {
        this.contacts = contacts;
    }

    /**
     * This method gets the value of the property contactGroups that can be set
     * using the {@link #setContactGroups(Collection)}.
     *
     * @return the value outputs as a Collection<ContactGroup>.
     * @see ContactGroup .
     */
    public Collection<ContactGroup> getContactGroups() {
        return contactGroups;
    }

    /**
     * This method sets the value of the contactGroups property that can be
     * obtained using the method {@link #getContactGroups()}.
     *
     * @param contactGroups is groups Collection<ContactGroup> object.
     */
    public void setContactGroups(Collection<ContactGroup> contactGroups) {
        this.contactGroups = contactGroups;
    }

    /**
     * This method gets the value of the property permissions that can be set
     * using the {@link #setPermissions(Collection)}.
     *
     * @return the value outputs as a Collection<Permission>.
     * @see Permission .
     */
    public Collection<Permission> getPermissions() {
        return permissions;
    }

    /**
     * This method sets the value of the permissions property that can be
     * obtained using the method {@link #getPermissions()}.
     *
     * @param permissions is permissions Collection<Permission> object.
     */
    public void setPermissions(Collection<Permission> permissions) {
        this.permissions = permissions;
    }

    /**
     * This method gets the value of the property map that can be set using the
     * {@link #setMap(Map)}.
     *
     * @return the value outputs as a Map<Class, IDGenerator>.
     * @see Class .
     * @see IDGenerator .
     */
    public Map<Class, IDGenerator> getMap() {
        return map;
    }

    /**
     * This method sets the value of the map property that can be obtained using
     * the method {@link #getMap()}.
     *
     * @param map is map Map<Class, IDGenerator> object.
     */
    public void setMap(Map<Class, IDGenerator> map) {
        this.map = map;
    }
}
