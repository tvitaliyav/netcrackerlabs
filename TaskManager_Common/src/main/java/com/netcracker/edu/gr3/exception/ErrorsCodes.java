package com.netcracker.edu.gr3.exception;

/**
 * Class of constants "ErrorsCodes" designed to indicate error codes.
 *
 * @author Katherine Denisova, Olga Sudakova, Vitalia Tihonova
 * @version 1.0.0
 */
public final class ErrorsCodes {
    /**
     * Static immutable field for code when initialization ControllerProvider of
     * type int.
     */
    public static final int CONTROLLER_PROVIDER_INITIALIZATION_ERROR = 2;
    /**
     * Static immutable field for code when getting Controller in
     * ControllerProvider of type int.
     */
    public static final int CONTROLLER_PROVIDER_GET_CONTROLLER_ERROR = 3;

    /**
     * Static immutable field for error code when getting JAXBcontext of type
     * int.
     */
    public static final int JAXB_CONTEXT_INITIALIZTION_ERROR = 4;
    /**
     * Static immutable field for error code when getting Marshaller of type
     * int.
     */
    public static final int MARSHALLER_INITIALIZATION_ERROR = 5;
    /**
     * Static immutable field for error code when getting Unmarshaller of type
     * int.
     */
    public static final int UNMARSHALLER_INITIALIZATION_ERROR = 6;
    /**
     * Static immutable field for error code when saving class objects to file
     * of type int .
     */
    public static final int JAXB_SAVE_TO_FILE_EXCEPTION = 7;
    /**
     * Static immutable field for error code when loading class object from file
     * of type int .
     */
    public static final int JAXB_LOAD_FROM_FILE_EXCEPTION = 8;
    /**
     * Static immutable field for error code when file not exist.
     */
    public static final int DATA_FILE_NOT_EXISTS = 9;
    /**
     * Static immutable field for error code when class not found.
     */
    public static final int CLASS_NOT_FOUND_EXCEPTION = 10;
    /**
     * Static immutable field for error code when
     * com.netcracker.edu.gr3.model not loaded from xml file.
     */
    public static final int LOAD_MODEL_FROM_FILE_EXCEPTION = 11;
    /**
     * Static immutable field for error code when object not initializated.
     */
    public static final int OBJECT_INITIALIZATION_EXCEPTION = 12;
    /**
     * Static immutable field for error code when
     * com.netcracker.edu.gr3.model not saved to file.
     */
    public static final int SAVE_MODEL_TO_FILE_EXCEPTION = 13;
    /**
     * Static immutable field for error code when there are not such user.
     */
    public static final int USER_NOT_FOUND_EXCEPTION = 14;
    /**
     * Static immutable field for error code when there are not unique login.
     */
    public static final int NOT_UNIQUE_LOGIN_EXCEPTION = 15;
    /**
     * Static immutable field for error code when update user`s login.
     */
    public static final int NEW_LOGIN_EXCEPTION = 16;
    /**
     * Static immutable field for error code when update user`s email.
     */
    public static final int NEW_EMAIL_EXCEPTION = 17;
    /**
     * Static immutable field for error code when there are
     * not user with this login-password pair.
     */
    public static final int
            USER_WITH_LOGIN_AND_PASSWORD_NOT_FOUND_EXCEPTION = 18;

    /**
     * Static immutable field for error code when there are not unique email.
     */
    public static final int NOT_UNIQUE_EMAIL_EXCEPTION = 19;
    /**
     * Static immutable field for code when initialization
     * LanguageComboBox of type int.
     */
    public static final int
            LANGUAGE_COMBO_BOX_INITIALIZATION_ERROR = 20;
    /**
     * Static immutable field for code when there are
     * different password for registration.
     */
    public static final int DIFFERENT_PASSWORD_ERROR = 21;
    /**
     * Static immutable field for code when there
     * are data birth very big or small.
     */
    public static final int INCORRECT_DATA_BIRTH_ERROR = 22;
    /**
     * Static immutable field for error code
     * when there are not such calendar.
     */
    public static final int CALENDAR_NOT_FOUND_EXCEPTION = 23;
    /**
     * Static immutable field for error code
     * when there are not such contact group.
     */
    public static final int CONTACT_GROUP_NOT_FOUND_EXCEPTION = 24;
    /**
     * Static immutable field for error code
     * when there are not unique contact group name and user id.
     */
    public static final int
            NOT_UNIQUE_CONTACT_GROUP_NAME_AND_USER_ID_EXCEPTION = 25;
    /**
     * Static immutable field for error code when the calendar
     * name is not unique among the calendars of the users.
     */
    public static final int CALENDAR_NOT_UNIQUE_NAME = 26;
    /**
     * Static immutable field for error code when there are not such event.
     */
    public static final int EVENT_NOT_FOUND_EXCEPTION = 27;
    /**
     * Static immutable field for error code when there are not validated email.
     */
    public static final int NOT_VALIDATED_EMAIL = 28;
    /**
     * Static immutable field for error code when
     * there are not unique contact group name.
     */
    public static final int NOT_UNIQUE_CONTACT_GROUP_NAME_EXCEPTION = 29;
    /**
     * Static immutable field for error code when
     * there are this class not supported for permission.
     */
    public static final int NOT_SUPPORTED_FOR_PERMISSION = 30;
    /**
     * Static immutable field for error code when there are not such task.
     */
    public static final int TASK_NOT_FOUND_EXCEPTION = 31;
    /**
     * Static immutable field for error code when there
     * are incorrect action in user details.
     */
    public static final int INCORRECT_ACTION_IN_USER_DETAILS = 32;
    /**
     * Static immutable field for error code when there
     * are incorrect action in contact group.
     */
    public static final int INCORRECT_ACTION_IN_CONTACT_GROUP = 33;
    /**
     * Static immutable field for error code when there
     * are incorrect action in task.
     */
    public static final int INCORRECT_ACTION_IN_TASK = 34;
    /**
     * Static immutable field for error code when there
     * are incorrect action in calendar.
     */
    public static final int INCORRECT_ACTION_IN_CALENDAR = 35;
    /**
     * Static immutable field for error code when there
     * are incorrect event time span.
     */
    public static final int INCORRECT_EVENT_TIME_SPAN = 36;
    /**
     * Static immutable field for error code when there
     * are incorrect task time span.
     */
    public static final int INCORRECT_TASK_TIME_SPAN = 37;
    /**
     * Static immutable field for error code when there
     * are incorrect action in event.
     */
    public static final int INCORRECT_ACTION_IN_EVENT = 38;
    /**
     * Static immutable field for code of unknown error of type int.
     */
    public static final int UNKNOWN = 1;


    /**
     * Default constructor - сreates a new object.
     */
    private ErrorsCodes() {

    }
}
