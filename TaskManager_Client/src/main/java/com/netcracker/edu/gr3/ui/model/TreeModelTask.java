package com.netcracker.edu.gr3.ui.model;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.TaskControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Task;
import com.netcracker.edu.gr3.util.ControllerProvider;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;

public class TreeModelTask implements TreeModel {
    private Object rootNode;
    public static final Object ROOT_TASK = new Object();

    public TreeModelTask(Object rootNode) {
        this.rootNode = rootNode;
    }

    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        try {
            if (parent.equals(ROOT_TASK)) {
                return ((ArrayList<Task>) ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class).getTaskByUserIdAndParentId(ControllerProvider.getProvider().
                        getController(ApplicationControllerInterface.class).getCurrentUser(), 0)).get(index);
            } else {
                return ((ArrayList<Task>) ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class)
                        .getTaskByParentId(((Task) parent).getTaskId())).get(index);
            }
        } catch (TaskManagerException e) {
            return null;
        }
    }

    @Override
    public int getChildCount(Object parent) {
        try {
            if (parent.equals(ROOT_TASK)) {
                return ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class).getTaskByUserIdAndParentId(ControllerProvider.getProvider().
                        getController(ApplicationControllerInterface.class).getCurrentUser(), 0).size();
            } else {
                return ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class)
                        .getTaskByParentId(((Task) parent).getTaskId()).size();
            }
        } catch (TaskManagerException e) {
            return 0;
        }
    }

    @Override
    public boolean isLeaf(Object node) {
        try {
            if (node.equals(ROOT_TASK)) {
                return ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class).getTaskByUserIdAndParentId(ControllerProvider.getProvider().
                        getController(ApplicationControllerInterface.class).getCurrentUser(), 0).size() == 0;
            } else
                return ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class)
                        .getTaskByParentId(((Task) node).getTaskId()).size() == 0;
        } catch (TaskManagerException e) {
            return true;
        }
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        try {
            if (parent.equals(ROOT_TASK)) {
                return ((ArrayList<Task>) ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class).getTaskByUserIdAndParentId(ControllerProvider.getProvider().
                        getController(ApplicationControllerInterface.class).getCurrentUser(), 0)).indexOf(child);
            } else {
                return ((ArrayList<Task>) ControllerProvider.getProvider().
                        getController(TaskControllerInterface.class)
                        .getTaskByParentId(((Task) parent).getTaskId())).indexOf(child);
            }
        } catch (TaskManagerException e) {
            return -1;
        }
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
