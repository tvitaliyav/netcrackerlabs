package com.netcracker.edu.gr3.ui.dialog;
import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.CalendarControllerInterface;
import com.netcracker.edu.gr3.controllers.EventControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.model.Event;
import com.netcracker.edu.gr3.util.ControllerProvider;
import com.toedter.calendar.JDateChooser;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.sql.Date;
import java.sql.Time;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class EventDialog extends JDialog {
    public static final String CREATE_EVENT = "Create Event";
    public static final String CHANGE_EVENT = "Change Event";
    private String action;
    private long eventId;
    private JPanel eventPanel;
    private JLabel labelTitle;
    private JTextField textTitle;
    private JLabel labelDescription;
    private JTextField textDescription;
    private JLabel labelCalendar;
    private JComboBox<Calendar> comboBoxCalendar;
    private JLabel labelBeginDate;
    private JDateChooser chooserBeginDate;
    private JLabel labelBeginTime;
    private JSpinner beginTime;
    private JLabel labelEndDate;
    private JDateChooser chooserEndDate;
    private JLabel labelEndTime;
    private JSpinner  endTime;
    private JButton buttonCreateEvent;
    private JButton buttonCancel;
    private JButton buttonDelete;

    public EventDialog(String action) {
        try {
            if(action.equals(CREATE_EVENT)) {
                new EventDialog(action, 0).setVisible(true);
            }
            else throw new TaskManagerException(ErrorsCodes.INCORRECT_ACTION_IN_EVENT);
        }
        catch (TaskManagerException e){
            handleException(e);
        }
    }
    public EventDialog(String action, long eventId) {
        try {
            this.action = action;
            this.eventId=eventId;
            setModal(true);
            setResizable(false);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            eventPanel = new JPanel(new MigLayout());
            labelTitle = new JLabel();
            textTitle = new JTextField();
            labelDescription = new JLabel();
            textDescription = new JTextField();
            labelCalendar = new JLabel();
            comboBoxCalendar = new JComboBox<>();
            ArrayList<Calendar> list = (ArrayList<Calendar>) ControllerProvider
                    .getProvider().getController(CalendarControllerInterface.class)
                    .getCalendarsByUserId(ControllerProvider.getProvider()
                            .getController(ApplicationControllerInterface.class)
                            .getCurrentUser());
            comboBoxCalendar.setModel(new DefaultComboBoxModel
                    (list.toArray(new Calendar[list.size()])));
            comboBoxCalendar.setRenderer(new ListCellRenderer<Calendar>() {
                @Override
                public Component getListCellRendererComponent(JList<? extends Calendar> list, Calendar value, int index, boolean isSelected, boolean cellHasFocus) {
                    return new JLabel((value.getName()));
                }
            });
            labelBeginDate = new JLabel();
            chooserBeginDate = new JDateChooser();
            labelBeginTime = new JLabel();
            beginTime = new JSpinner( new SpinnerDateModel() );
            beginTime.setEditor(new JSpinner.DateEditor(beginTime, "HH:mm"));
            labelEndDate = new JLabel();
            chooserEndDate = new JDateChooser();
            labelEndTime = new JLabel();
            endTime = new JSpinner( new SpinnerDateModel() );
            endTime.setEditor(new JSpinner.DateEditor(endTime, "HH:mm"));
            buttonCreateEvent = new JButton();
            buttonCancel = new JButton();
            buttonDelete=new JButton();
            localizeView();
            if(action.equals(CHANGE_EVENT)){
                try {
                    Event currentEvent= ControllerProvider.getProvider().getController(
                            EventControllerInterface.class).getEventById(eventId);
                    textTitle.setText(currentEvent.getTitleEvent());
                    comboBoxCalendar.setSelectedItem(ControllerProvider.getProvider().getController(
                            CalendarControllerInterface.class).getCalendarById(currentEvent.getCalendarId()));
                    textDescription.setText(currentEvent.getDescription());
                    if (currentEvent.getBeginDateTime() != null) {
                        chooserBeginDate.setDate(Date.valueOf(currentEvent.getBeginDateTime().toLocalDate()));
                        beginTime.setValue(Time.valueOf(currentEvent.getBeginDateTime().toLocalTime()));
                    }
                    if(currentEvent.getEndDateTime()!=null) {
                        chooserEndDate.setDate(Date.valueOf(currentEvent.getEndDateTime().toLocalDate()));
                        endTime.setValue(Time.valueOf(currentEvent.getEndDateTime().toLocalTime()));
                    }
                   } catch (TaskManagerException e) {
                    handleException(e);
                }
            }
            eventPanel.add(labelTitle);
            eventPanel.add(textTitle,"growx,pushx,wrap");
            eventPanel.add(labelCalendar);
            eventPanel.add(comboBoxCalendar,"growx,pushx,wrap");
            eventPanel.add(labelDescription);
            eventPanel.add(textDescription,"growx,pushx,wrap");
            eventPanel.add(labelBeginDate);
            eventPanel.add(chooserBeginDate,"growx,pushx,wrap");
            eventPanel.add(labelBeginTime);
            eventPanel.add(beginTime,"growx,pushx,wrap");
            eventPanel.add(labelEndDate);
            eventPanel.add(chooserEndDate,"growx,pushx,wrap");
            eventPanel.add(labelEndTime);
            eventPanel.add(endTime,"growx,pushx,wrap");
            eventPanel.add(buttonCreateEvent);
            eventPanel.add(buttonCancel);
            if(action.equals(CHANGE_EVENT))eventPanel.add(buttonDelete);
            add(eventPanel);
            buttonCancel.addActionListener(e -> dispose());
            buttonDelete.addActionListener(e -> {
                try {
                    ControllerProvider.getProvider().getController(
                            EventControllerInterface.class).deleteEvent(eventId);
                    dispose();
                } catch (TaskManagerException e1) {
                    handleException(e1);
                }
            });
            buttonCreateEvent.addActionListener(e -> {
                try {
                    if (!textTitle.getText().equals("") && chooserBeginDate.getDate() != null
                            && chooserEndDate.getDate() != null) {
                        Event event;
                        if(action.equals(CREATE_EVENT)) {
                            event = new Event();
                        }
                        else event = ControllerProvider.getProvider().getController(
                                EventControllerInterface.class).getEventById(eventId);
                        event.setTitleEvent(textTitle.getText());
                        if (!textDescription.getText().equals("")) {
                            event.setDescription(textDescription.getText());
                        }
                        event.setCalendarId(((Calendar) comboBoxCalendar.getSelectedItem()).getCalendarId());
                        LocalDateTime dateTimeBegin = LocalDateTime
                                .ofInstant(Instant.ofEpochMilli(
                                        chooserBeginDate.getDate().getTime()), ZoneId.systemDefault());
                        java.util.Calendar calendar = java.util.Calendar.getInstance();
                        calendar.setTime((java.util.Date) beginTime.getValue());
                        dateTimeBegin = dateTimeBegin.withHour(calendar.get(java.util.Calendar.HOUR));
                        dateTimeBegin = dateTimeBegin.withMinute(calendar.get(java.util.Calendar.MINUTE));
                        event.setBeginDateTime(dateTimeBegin);
                        LocalDateTime dateTimeEnd = LocalDateTime
                                .ofInstant(Instant.ofEpochMilli(
                                        chooserEndDate.getDate().getTime()), ZoneId.systemDefault());
                        calendar.setTime((java.util.Date) endTime.getValue());
                        dateTimeEnd = dateTimeEnd.withHour(calendar.get(java.util.Calendar.HOUR));
                        dateTimeEnd = dateTimeEnd.withMinute(calendar.get(java.util.Calendar.MINUTE));
                        event.setEndDateTime(dateTimeEnd);
                        if(action.equals(CREATE_EVENT)) ControllerProvider.getProvider().getController(
                                EventControllerInterface.class).createEvent(event);
                        else ControllerProvider.getProvider().getController(
                                EventControllerInterface.class).updateEvent(event);
                        dispose();
                    }
                }catch (TaskManagerException e1) {
                    handleException(e1);
                }
            });

        } catch (TaskManagerException e) {
            handleException(e);
        }

    }
    private void handleException(TaskManagerException exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        JOptionPane.showMessageDialog(this,
                bundleError.getString(
                        String.valueOf(exception.getErrorCode())));
    }
    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.eventDialog", Locale.getDefault());
        if(action.equals(CREATE_EVENT))setTitle(bundle.getString("window.title"));
        else  setTitle(bundle.getString("window.title.two"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        labelTitle.setText(bundle.getString("labelTitle.name"));
        labelDescription.setText(bundle.getString("labelDescription.name"));
        labelCalendar.setText(bundle.getString("labelCalendar.name"));
        labelBeginDate.setText(bundle.getString("labelBeginDate.name"));
        labelBeginTime.setText(bundle.getString("labelBeginTime.name"));
        labelEndTime.setText(bundle.getString("labelEndTime.name"));
        labelEndDate.setText(bundle.getString("labelEndDate.name"));
        buttonCancel.setText(bundle.getString("buttonCancel.name"));
        buttonCancel.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCancel.width")),
                Integer.parseInt(bundle.getString("buttonCancel.height"))));
        if(action.equals(CHANGE_EVENT)){
            buttonCreateEvent.setText(bundle.getString("buttonChangeEvent.name"));
            buttonDelete.setText(bundle.getString("buttonDelete.name"));
            buttonDelete.setPreferredSize(new Dimension(
                    Integer.parseInt(bundle.getString("buttonDelete.width")),
                    Integer.parseInt(bundle.getString("buttonDelete.height"))));
        }
        else{
            buttonCreateEvent.setText(bundle.getString("buttonCreateEvent.name"));
        }
        buttonCreateEvent.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCreateEvent.width")),
                Integer.parseInt(bundle.getString("buttonCreateEvent.height"))));
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }
}
