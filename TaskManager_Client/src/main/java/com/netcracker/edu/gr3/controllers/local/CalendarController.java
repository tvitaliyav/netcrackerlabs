package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.CalendarControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class "CalendarController" implements
 * the interface CalendarControllerInterface.
 *
 * @author Olga Sudakova, Ekaterina Denisova
 * @version 1.0.0
 */
@Component
public class CalendarController implements CalendarControllerInterface {
    /**
     * Object that provides work with DataStore.
     */
    @Autowired
    private DataStore dataStore;
    /**
     * Object that provides work with ApplicationControllerInterface.
     */
    @Autowired
    private ApplicationControllerInterface appControllerInterface;
    /**
     * Property is LOGGER.
     */
    private static final Logger LOGGER = Logger
            .getLogger(UserController.class);

    /**
     * @inheritDoc
     */
    @Override
    public Calendar createCalendar(Calendar calendar)
            throws TaskManagerException {
        LOGGER.info("Method createCalendar called");
        if (getCalendarsByUserId(calendar.getUserId()).stream()
                .filter(newCalendar -> newCalendar.getName()
                        .equals(calendar.getName())).count() != 0) {
            throw new TaskManagerException(ErrorsCodes
                    .CALENDAR_NOT_UNIQUE_NAME);
        }
        LOGGER.debug("Calendar name uniqueness verified");
        calendar.setCalendarId(dataStore.getMap()
                .get(Calendar.class).generateID());
        LOGGER.debug("ID was generated");
        dataStore.getCalendars().add(calendar);
        LOGGER.info("Calendar was added, method createCalendar end");
        appControllerInterface.notifyObservers(new ObserverEvent());
        return calendar;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Calendar getCalendarById(long calendarId)
            throws TaskManagerException {
        try {
            LOGGER.info("Method getCalendarById called");
            return dataStore.getCalendars().stream()
                    .filter(calendar -> calendar.getCalendarId() == calendarId)
                    .findFirst().orElseThrow(() ->
                            new TaskManagerException(ErrorsCodes.
                                    CALENDAR_NOT_FOUND_EXCEPTION));
        } finally {
            LOGGER.info("Method getCalendarById end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Calendar> getCalendarsByUserId(long userId) throws TaskManagerException {
        try {
            LOGGER.info("Method getCalendarsByUserId called");
            User user = dataStore.getUsers()
                    .stream().filter(user1 -> user1.getUserId() == userId).findFirst()
                    .orElseThrow(() ->
                            new TaskManagerException(ErrorsCodes.
                                    USER_NOT_FOUND_EXCEPTION));
            return dataStore.getCalendars().stream()
                    .filter(calendar -> calendar.getUserId() == userId)
                    .collect(Collectors.toCollection(ArrayList::new));
        } finally {
            LOGGER.info("Method getCalendarsByUserId end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Calendar updateCalendar(Calendar calendar)
            throws TaskManagerException {
        LOGGER.info("Method updateCalendar called");
        if (getCalendarsByUserId(calendar.getUserId()).stream()
                .filter(newCalendar -> newCalendar.getName()
                        .equals(calendar.getName())).count() != 0) {
            throw new TaskManagerException(ErrorsCodes
                    .CALENDAR_NOT_UNIQUE_NAME);
        }
        LOGGER.debug("Calendar name uniqueness verified");
        Calendar updatedCalendar = getCalendarById(calendar.getCalendarId());
        updatedCalendar.setName(calendar.getName());
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("Method updateCalendar end");
        return updatedCalendar;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteCalendar(long calendarId) throws TaskManagerException {
        LOGGER.info("Method deleteCalendar called");
        dataStore.getCalendars()
                .removeIf(calendar -> calendar.getCalendarId() == calendarId
                        && dataStore.getUsers().stream().noneMatch(user -> user.getCalendarId() == calendarId));
        dataStore.getEvents().removeIf(event -> event.getCalendarId() == calendarId
                && dataStore.getUsers().stream().noneMatch(user -> user.getCalendarId() == calendarId));
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("Method deleteCalendar end");
    }
}
