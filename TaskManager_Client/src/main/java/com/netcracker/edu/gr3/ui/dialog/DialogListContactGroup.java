package com.netcracker.edu.gr3.ui.dialog;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.ContactGroupControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.ContactGroup;
import com.netcracker.edu.gr3.util.ControllerProvider;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;

public class DialogListContactGroup extends JDialog {
    private static JList<ContactGroup> list;
    private static ContactGroup selectContactGroup;
    public DialogListContactGroup() throws TaskManagerException {
        ResourceBundle bundle= ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.ui.dialogListContactGroup", Locale.getDefault());
        setModal(true);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                selectContactGroup = null;
                dispose();
            }
        });
        setSize(Integer.parseInt(bundle.getString("window.width")),
                Integer.parseInt(bundle.getString("window.height")));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setTitle(bundle.getString("window.title"));
        setBounds(locationX, locationY, windowWidth, windowHeight);
        setLayout(new MigLayout());
        Vector<ContactGroup> vector = new Vector<>();
        vector.addAll(ControllerProvider.getProvider()
                .getController(ContactGroupControllerInterface.class)
                .getContactGroupsByUserId(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()));
        JButton button = new JButton("OK");
        list = new JList<ContactGroup>(vector);
        list.setCellRenderer(new ListCellRenderer<ContactGroup>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends ContactGroup> list,
                                                          ContactGroup value, int index, boolean isSelected,
                                                          boolean cellHasFocus) {
                JLabel label = new JLabel(value.getName());
                label.setOpaque(true);
                if(isSelected){
                    label.setBackground(Color.decode("#B0C4DE"));
                }
                else label.setBackground(Color.WHITE);
                return label;
            }
        });
        add(new JScrollPane(list),"wrap, pushx, growx, pushy, growy");
        add(button);
        button.addActionListener(e -> {
            selectContactGroup = list.getSelectedValue();
            dispose();
        });
    }

    public static ContactGroup getSelectContactGroup() {
        return selectContactGroup;
    }
}
