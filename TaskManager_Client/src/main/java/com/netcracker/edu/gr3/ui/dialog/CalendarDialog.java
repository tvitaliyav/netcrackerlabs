package com.netcracker.edu.gr3.ui.dialog;

import com.netcracker.edu.gr3.controllers.CalendarControllerInterface;
import com.netcracker.edu.gr3.controllers.local.ApplicationController;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.util.ControllerProvider;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class CalendarDialog extends JDialog {
    public static final String CREATE_CALENDAR = "Create Calendar";
    public static final String CHANGE_CALENDAR = "Change Calendar";
    private static final int COLUMNS_TEXT_FIELD = 20;
    private String action;
    private long idCalendar;
    private JPanel calendarPanel;
    private JLabel labelName;
    private JButton buttonCreateCalendar;
    private JButton buttonCancel;
    public CalendarDialog(String action){
        try {
            if(action.equals(CREATE_CALENDAR)) {
                new CalendarDialog(action,0).setVisible(true);
            }
            else throw new TaskManagerException(ErrorsCodes.INCORRECT_ACTION_IN_CALENDAR);
        }
        catch (TaskManagerException e){
            handleException(e);
        }
    }
    public CalendarDialog(String action, long idCalendar) {
        this.action = action;
        this.idCalendar = idCalendar;
        setModal(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        calendarPanel = new JPanel(new MigLayout());
        JPanel informationPanel = new JPanel(new MigLayout());
        labelName = new JLabel();
        JTextField textName = new JTextField(COLUMNS_TEXT_FIELD);
        if(action.equals(CHANGE_CALENDAR)) {
            try {
                textName.setText(
                        ControllerProvider.getProvider().
                                getController(CalendarControllerInterface.class)
                                .getCalendarById(idCalendar).getName());
            } catch (TaskManagerException e) {
                handleException(e);
            }
        }
        JPanel panelButtons = new JPanel(new MigLayout());
        buttonCreateCalendar = new JButton();
        buttonCancel = new JButton();
        localizeView();
        informationPanel.add(labelName);
        informationPanel.add(textName,"wrap,pushx, growx");
        panelButtons.add(buttonCreateCalendar);
        panelButtons.add(buttonCancel);
        calendarPanel.add(informationPanel,"wrap");
        calendarPanel.add(panelButtons);
        add(calendarPanel);
        buttonCancel.addActionListener(e -> SwingUtilities.invokeLater(this::dispose));
        buttonCreateCalendar.addActionListener(e ->{
            if(!textName.getText().equals(""))
                try {
                    Calendar calendar=new Calendar();
                    calendar.setName(textName.getText());
                    calendar.setUserId(ControllerProvider.getProvider().
                            getController(ApplicationController.class).getCurrentUser());
                    if(action.equals(CREATE_CALENDAR)) {
                        ControllerProvider.getProvider().
                                getController(CalendarControllerInterface.class)
                                .createCalendar(calendar);
                    }
                    else {
                        calendar.setCalendarId(idCalendar);
                        ControllerProvider.getProvider().
                                getController(CalendarControllerInterface.class)
                                .updateCalendar(calendar);
                    }
                    this.dispose();
                } catch (TaskManagerException e1) {
                    handleException(e1);
                }
        });
    }
    private void handleException(TaskManagerException exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        JOptionPane.showMessageDialog(this,
                bundleError.getString(
                        String.valueOf(exception.getErrorCode())));

    }
    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.calendarDialog", Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        calendarPanel.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        labelName.setText(bundle.getString("labelName.name"));
        if(action.equals(CREATE_CALENDAR))buttonCreateCalendar.setText(
                bundle.getString("buttonCreateCalendar.name"));
        else buttonCreateCalendar.setText(bundle.getString("buttonChangeCalendar.name"));
        buttonCreateCalendar.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCreateCalendar.width")),
                Integer.parseInt(bundle.getString("buttonCreateCalendar.height"))));
        buttonCancel.setText(bundle.getString("buttonCancel.name"));
        buttonCancel.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCancel.width")),
                Integer.parseInt(bundle.getString("buttonCancel.height"))));
    }
}
