package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Permission;

import java.util.Collection;

/**
 * Interface "PermissionControllerInterface" defines actions
 * with permissions.
 *
 * @author Vitaliya Tikhonova
 * @version 1.0.0
 */
public interface PermissionControllerInterface {
    /**
     * Method for creating new permission or change old.
     *
     * @param permission - permission for create/update
     * @return Permission
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Permission setPermission(Permission permission)
            throws TaskManagerException;

    /**
     * Method to get permission by type of object and userID.
     *
     * @param objectType - type of object
     * @param userId     - unique user id
     * @return Collection of permissions
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Permission> getPermissionsByObjectTypeAndUserID(Class objectType, long userId)
            throws TaskManagerException;

    /**
     * Method to get permission by type of object and objectID.
     *
     * @param objectType - type of object
     * @param objectId   - unique object id.
     * @return Permission
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Permission getPermissionByObjectTypeAndObjectID(Class objectType, long objectId)
            throws TaskManagerException;
}
