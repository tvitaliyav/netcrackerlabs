package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.observer.Observer;
import com.netcracker.edu.gr3.observer.ObserverEvent;

import java.util.Collection;

/**
 * Interface "UserControllerInterface" defines actions with user.
 *
 * @author Vitaliya Tikhonova, Ekaterina Denisova
 * @version 1.0.0
 */

public interface UserControllerInterface {
    /**
     * Method for creating a new user.
     *
     * @param user - user for creating
     * @return user
     * @throws TaskManagerException Inherited from class "Exception"
     */
    User createUser(User user) throws TaskManagerException;

    /**
     * Method for authorization user.
     *
     * @param login    - user`s login
     * @param password - user`s password
     * @return user
     * @throws TaskManagerException Inherited from class "Exception"
     */
    User auth(String login, String password) throws TaskManagerException;

    /**
     * Method to get all users.
     * <p>
     * Inherited from class "Exception"
     *
     * @return Collection of users
     */
    Collection<User> getAllUsers();

    /**
     * Method to get user by ID.
     *
     * @param id - unique user`s ID
     * @return user
     * @throws TaskManagerException Inherited from class "Exception"
     */
    User getUserById(long id) throws TaskManagerException;

    /**
     * Method to get user by username.
     *
     * @param username -  unique user`s username
     * @return user
     * @throws TaskManagerException Inherited from class "Exception"
     */
    User getUserByUsername(String username) throws TaskManagerException;

    /**
     * Method for update user.
     *
     * @param user - new user
     * @return user
     * @throws TaskManagerException Inherited from class "Exception"
     */
    User updateUser(User user) throws TaskManagerException;
}
