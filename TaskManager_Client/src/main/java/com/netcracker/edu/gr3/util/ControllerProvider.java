package com.netcracker.edu.gr3.util;

import com.netcracker.edu.gr3.exception.TaskManagerException;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * This singleton class is needed to get controllers.
 *
 * @author Denisova Ekaterina, Olga Sudakova
 * @version 1.0.0
 */
public final class ControllerProvider {

    /**
     * Property is LOGGER.
     *
     * @see Logger .
     */
    private static final Logger LOGGER = Logger
            .getLogger(ControllerProvider.class);
    /**
     * Property is provider.
     */
    private static ControllerProvider provider = null;
    /**
     * Property is XML_CONFIG.
     */
    private static final String XML_CONFIG = "controllers.xml";

    /**
     * Property is context.
     */
    private static ApplicationContext context = null;

    /**
     * Create new object.
     *
     * @see ControllerProvider#ControllerProvider().
     * @throws TaskManagerException .
     */
    private ControllerProvider() throws TaskManagerException {
        LOGGER.info("Create ControllerProvider object called");
        getSpringContextFromXMLConfig();
        LOGGER.info("Create ControllerProvider object end");
    }

    /**
     * Method to get field value {@link #provider}.
     *
     * @return the value outputs as a ControllerProvider.
     * @throws TaskManagerException .
     * @see ControllerProvider .
     */
    public static ControllerProvider getProvider() throws TaskManagerException {
        LOGGER.info("Method getProvider called");
        if (provider == null) {
            LOGGER.info("ControllerProvider not initialized");
            provider = new ControllerProvider();
            LOGGER.info("ControllerProvider has been initialized");
        }
        LOGGER.info("Method getProvider end");
        return provider;
    }

    /**
     * This method lets get spring context from a xml file named XML_CONFIG.
     *
     * @throws TaskManagerException .
     */
    private void getSpringContextFromXMLConfig()
            throws TaskManagerException {
        try {
            LOGGER.info("Method loadSpringConfigFromXML called");
            if (context == null) {
                LOGGER.info("Context not initialized");
                context = new GenericXmlApplicationContext(XML_CONFIG);
                LOGGER.info("Context has been initialized");
            }
        } finally {
            LOGGER.info("Method loadSpringConfigFromXML end");
        }
    }

    /**
     * This method gets bean of controller through the Spring context.
     *
     * @param controller is class of controller.
     * @param <T> is type getting controller.
     * @return the bean outputs as a controller.
     * @throws TaskManagerException .
     */
    public <T> T getController(Class<T> controller)
            throws TaskManagerException {
        try {
            LOGGER.info("Method getController called");
            return context.getBean(controller);
        } finally {
            LOGGER.info("Method getController end");
        }
    }
}
