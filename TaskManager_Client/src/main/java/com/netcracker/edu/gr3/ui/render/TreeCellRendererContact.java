package com.netcracker.edu.gr3.ui.render;

import com.netcracker.edu.gr3.model.ContactGroup;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.ui.model.TreeModelContact;

import javax.swing.*;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;


public class TreeCellRendererContact  extends JLabel implements TreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            ResourceBundle bundle = ResourceBundle.getBundle(
                    "com.netcracker.edu.gr3.ui.treeModelContact", Locale.getDefault());
            if (value instanceof User) {
                value = new StringBuilder (((User) value).getFirstName())
                        .append(" ")
                        .append(((User) value).getLastName());
                setIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/user.jpg")));
            }
            if (value instanceof ContactGroup) {
                value = ((ContactGroup) value).getName();
                setIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/group.png")));
            }
            if (value.equals(TreeModelContact.ROOT_INCOMING)) {
                value = bundle.getString("ROOT_INCOMING");
                setIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/incoming.png")));
            }
            if (value.equals(TreeModelContact.ROOT_OUTCOMING)) {
                value = bundle.getString("ROOT_OUTCOMING");
                setIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/outcoming.png")));
            }
            if (value.equals(TreeModelContact.ROOT_BLACK_LIST)) {
                value = bundle.getString("ROOT_BLACK_LIST");
                setIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/blacklist.png")));
            }

        setText(value.toString());
        setOpaque(true);
        if(selected)setBackground(Color.decode("#B0C4DE"));
        else setBackground(Color.WHITE);
        return this;
    }
}
