package com.netcracker.edu.gr3.ui.model;

import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.ui.render.RenderingComboBox;

import javax.swing.JComboBox;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

/**
 * This class is needed to storing JComboBox Locale.
 *
 * @author Denisova Ekaterina
 * @version 1.0.
 */
public class LanguageComboBox extends JComboBox<Locale> {
    /**
     * Property is FILE_NAME.
     */
    private static final String FILE_NAME =
            "application.properties";

    /**
     * Create new object.
     *
     * @throws TaskManagerException .
     * @see LanguageComboBox#LanguageComboBox().
     */
    public LanguageComboBox() throws TaskManagerException {
        super();
        try {
            InputStream input = getClass().getClassLoader()
                    .getResourceAsStream(FILE_NAME);
            Properties properties = new Properties();
            properties.load(input);
            properties.setProperty("currentLanguage", "en");
            String supportLanguages = properties
                    .getProperty("supportLanguages");
            String[] arraySupportLanguages = supportLanguages.split(";");
            for (String supportLanguage : arraySupportLanguages) {
                super.addItem(Locale.forLanguageTag(supportLanguage));
                if (Locale.getDefault().toString().equals(
                        Locale.forLanguageTag(supportLanguage).toString())) {
                    properties.setProperty("currentLanguage", supportLanguage);
                }
            }
            setSelectedItem(Locale.forLanguageTag(properties
                    .getProperty("currentLanguage")));
            setRenderer(new RenderingComboBox());
        } catch (IOException e) {
            throw new TaskManagerException(ErrorsCodes
                    .LANGUAGE_COMBO_BOX_INITIALIZATION_ERROR);
        }
    }

    /**
     * Method for determining a object to which the selected item belongs
     * {@link #getSelectedItem()}.
     *
     * @param anObject is Locale object
     */
    @Override
    public void setSelectedItem(Object anObject) {
        Locale.setDefault((Locale) anObject);
        super.setSelectedItem(anObject);
    }
}
