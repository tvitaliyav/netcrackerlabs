package com.netcracker.edu.gr3.ui.render;

import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.ui.model.CheckNode;
import com.netcracker.edu.gr3.ui.model.TreeModelCalendar;

import javax.swing.*;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

public class TreeCellRendererCalendar extends JCheckBox implements TreeCellRenderer {
    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {
        if(value instanceof CheckNode) {
            setSelected(((CheckNode) value).isSelected());
        }
        else{
            value = new CheckNode(value,false);
        }
        if (((CheckNode) value).getObject() instanceof Calendar) {
            value = ((Calendar)((CheckNode) value)
                    .getObject()).getName();
        }
        setIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/calendarnotselected.jpg")));
        setSelectedIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/calendar.png")));
        setRolloverIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/calendarnotselected.jpg")));
        setRolloverSelectedIcon(new ImageIcon(ClassLoader.getSystemResource("com/netcracker/edu/gr3/images/calendar.png")));
        setText(value.toString());
        setOpaque(true);
        if(selected){
            setBackground(Color.decode("#B0C4DE"));
        }
        else {
            setBackground(Color.WHITE);
        }
        return this;
    }

}
