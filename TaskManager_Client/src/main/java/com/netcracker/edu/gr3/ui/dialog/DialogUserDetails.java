package com.netcracker.edu.gr3.ui.dialog;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.UserControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.ui.model.LanguageComboBox;
import com.netcracker.edu.gr3.util.ControllerProvider;
import com.toedter.calendar.JDateChooser;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.time.*;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class DialogUserDetails extends JDialog {

    public static final String MY_PROFILE = "My Profile";
    public static final String VIEW_PROFILE = "View Profile";
    private String action;
    private JLabel labelUsername;
    private JLabel labelPassword;
    private JLabel labelRepeatPassword;
    private JLabel labelFirstName;
    private JLabel labelLastName;
    private JLabel labelDateOfBirth;
    private JLabel labelPhoneNumber;
    private JLabel labelEmail;
    private JDateChooser dateChooser;
    private JPanel userDetailsPanel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private long userId;

    public DialogUserDetails(String action) {
        try {
            if (action.equals(MY_PROFILE)) {
                this.userId = ControllerProvider.getProvider()
                        .getController(ApplicationControllerInterface.class)
                        .getCurrentUser();
                this.action = action;
                createDialog();
                this.setVisible(true);
            } else throw new TaskManagerException(ErrorsCodes.INCORRECT_ACTION_IN_USER_DETAILS);
        } catch (TaskManagerException e) {
            handleException(e);
        }
    }

    public DialogUserDetails(String action, long userId) {
        try {
            this.userId = userId;
            this.action = action;
            createDialog();
            this.setVisible(true);
        } catch (TaskManagerException e) {
            handleException(e);
        }
    }
    private void createDialog() throws TaskManagerException{
        User currentUser = ControllerProvider.getProvider().
                getController(UserControllerInterface.class).getUserById(userId);
        setModal(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        userDetailsPanel = new JPanel(new MigLayout());
        labelUsername = new JLabel();
        JTextField textUsername = new JTextField();
        textUsername.setEditable(false);
        labelPassword = new JLabel();
        JPasswordField passwordField = new JPasswordField();
        labelRepeatPassword = new JLabel();
        JPasswordField repeatPasswordField = new JPasswordField();
        labelFirstName = new JLabel();
        JTextField textFirstName = new JTextField();
        labelLastName = new JLabel();
        JTextField textLastName = new JTextField();
        labelDateOfBirth = new JLabel();
        dateChooser = new JDateChooser();
        dateChooser.setDate(Date.from(currentUser
                .getDateOfBirth().atStartOfDay(ZoneId.systemDefault())
                .toInstant()));
        JTextField textDate = new JTextField(currentUser
                .getDateOfBirth().toString());
        labelPhoneNumber = new JLabel();
        JTextField textPhoneNumber = new JTextField();
        labelEmail = new JLabel();
        JTextField textEmail = new JTextField();
        buttonOK = new JButton();
        buttonCancel = new JButton();
        textUsername.setText(currentUser.getLogin());
        textFirstName.setText(currentUser.getFirstName());
        textLastName.setText(currentUser.getLastName());
        textEmail.setText(currentUser.getEmail());
        textPhoneNumber.setText(currentUser.getPhoneNumber());
        localizeView();
        if (action.equals(MY_PROFILE)) {
            JPanel panelLang = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            JComboBox comboBoxLang = null;
            try {
                comboBoxLang = new LanguageComboBox();
                comboBoxLang.addActionListener(event -> localizeView());
            } catch (TaskManagerException e) {
                handleException(e);
            }
            panelLang.add(comboBoxLang);
            userDetailsPanel.add(panelLang, "wrap");
        } else {
            textUsername.setEnabled(false);
            textUsername.setDisabledTextColor(Color.BLACK);
            textFirstName.setEnabled(false);
            textFirstName.setDisabledTextColor(Color.BLACK);
            textLastName.setEnabled(false);
            textLastName.setDisabledTextColor(Color.BLACK);
            textPhoneNumber.setEnabled(false);
            textPhoneNumber.setDisabledTextColor(Color.BLACK);
            textEmail.setEnabled(false);
            textEmail.setDisabledTextColor(Color.BLACK);
            textDate.setEnabled(false);
            textDate.setDisabledTextColor(Color.BLACK);
        }
        userDetailsPanel.add(labelUsername);
        userDetailsPanel.add(textUsername, "wrap,pushx, growx");
        userDetailsPanel.add(labelFirstName);
        userDetailsPanel.add(textFirstName, "wrap,pushx, growx");
        userDetailsPanel.add(labelLastName);
        userDetailsPanel.add(textLastName, "wrap,pushx, growx");
        userDetailsPanel.add(labelDateOfBirth);
        if (action.equals(MY_PROFILE)) {
            userDetailsPanel.add(dateChooser, "wrap,pushx, growx");
        } else {
            userDetailsPanel.add(textDate, "wrap,pushx, growx");
        }
        userDetailsPanel.add(labelEmail);
        userDetailsPanel.add(textEmail, "wrap,pushx, growx");
        userDetailsPanel.add(labelPhoneNumber);
        userDetailsPanel.add(textPhoneNumber, "wrap,pushx, growx");
        if (action.equals(MY_PROFILE)) {
            userDetailsPanel.add(labelPassword);
            userDetailsPanel.add(passwordField, "wrap,pushx, growx");
            userDetailsPanel.add(labelRepeatPassword);
            userDetailsPanel.add(repeatPasswordField, "wrap,pushx, growx");
        }
        if (action.equals(MY_PROFILE)) {
            userDetailsPanel.add(buttonCancel);
            buttonCancel.addActionListener(e ->
                    SwingUtilities.invokeLater(this::dispose));
        }
        userDetailsPanel.add(buttonOK);
        add(userDetailsPanel);
        buttonOK.addActionListener(e -> {
            try {
                if (action.equals(MY_PROFILE)) {
                    StringBuilder password = new StringBuilder();
                    for(char pas: passwordField.getPassword()){
                        password.append(pas);
                    }
                    StringBuilder repeatPassword = new StringBuilder();
                    for(char pas: repeatPasswordField.getPassword()){
                        repeatPassword.append(pas);
                    }
                    if (password.toString().equals(
                            repeatPassword.toString())) {
                        currentUser.setLogin(textUsername.getText());
                        currentUser.setFirstName(textFirstName.getText());
                        currentUser.setLastName(textLastName.getText());
                        currentUser.setEmail(textEmail.getText());
                        currentUser.setPhoneNumber(textPhoneNumber.getText());
                        if (!password.toString().equals("")) {
                            currentUser.setPassword(password.toString());
                        }
                        currentUser.setDateOfBirth(LocalDateTime
                                .ofInstant(Instant.ofEpochMilli(
                                        dateChooser.getDate().getTime()),
                                        ZoneId.systemDefault()).toLocalDate());
                        ControllerProvider.getProvider().
                                getController(UserControllerInterface.class)
                                .updateUser(currentUser);
                    } else {
                        throw new TaskManagerException(ErrorsCodes
                                .DIFFERENT_PASSWORD_ERROR);
                    }
                }
                dispose();
            } catch (TaskManagerException e1) {
                handleException(e1);
            }
        });
    }

    private void handleException(TaskManagerException exception) {

        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error",
                Locale.getDefault());
        JOptionPane.showMessageDialog(this,
                bundleError.getString(
                        String.valueOf(exception.getErrorCode())));
    }

    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.userDetailsDialog", Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth;
        int windowHeight;
        userDetailsPanel.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        labelUsername.setText(bundle.getString("labelUsername.name"));
        labelPassword.setText(bundle.getString("labelPassword.name"));
        labelRepeatPassword.setText(bundle
                .getString("labelRepeatPassword.name"));
        labelFirstName.setText(bundle.getString("labelFirstName.name"));
        labelLastName.setText(bundle.getString("labelLastName.name"));
        labelDateOfBirth.setText(bundle.getString("labelDateOfBirth.name"));
        labelPhoneNumber.setText(bundle.getString("labelPhoneNumber.name"));
        labelEmail.setText(bundle.getString("labelEmail.name"));
        if (action.equals(MY_PROFILE)) {
            windowWidth = Integer.parseInt(bundle.getString("window.width"));
            windowHeight = Integer.parseInt(bundle.getString("window.height"));
            buttonOK.setText(bundle.getString("buttonSaveChange.name"));
            buttonOK.setMaximumSize(new Dimension(
                    Integer.parseInt(bundle.getString("buttonSaveChange.width")),
                    Integer.parseInt(bundle.getString("buttonSaveChange.height"))));
            buttonCancel.setText(bundle.getString("buttonCancel.name"));
            buttonCancel.setPreferredSize(new Dimension(
                    Integer.parseInt(bundle.getString("buttonCancel.width")),
                    Integer.parseInt(bundle.getString("buttonCancel.height"))));
        } else {
            windowWidth = Integer.parseInt(bundle.getString("window.width.two"));
            windowHeight = Integer.parseInt(bundle.getString("window.height.two"));
            buttonOK.setText(bundle.getString("buttonClose.name"));
            buttonOK.setMaximumSize(new Dimension(
                    Integer.parseInt(bundle.getString("buttonClose.width")),
                    Integer.parseInt(bundle.getString("buttonClose.height"))));
        }
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
    }
}
