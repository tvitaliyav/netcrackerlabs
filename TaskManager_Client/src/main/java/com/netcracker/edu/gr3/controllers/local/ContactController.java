package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.ContactControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Contact;
import com.netcracker.edu.gr3.model.ContactType;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import com.netcracker.edu.gr3.util.ModelFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class "ContactController" implements the
 * interface ContactControllerInterface.
 *
 * @author Ekatherina Denisova, Olga Sudakova, Vitaliya Tikhonova
 * @version 1.0.0
 */
@Component
public class ContactController implements ContactControllerInterface {
    /**
     * Object that provides work with DataStore.
     */
    @Autowired
    private DataStore dataStore;
    /**
     * Object that provides work with ApplicationControllerInterface.
     */
    @Autowired
    private ApplicationControllerInterface appControllerInterface;
    /**
     * @inheritDoc
     */
    @Override
    public Contact createContact(Contact contact) throws TaskManagerException {

        if (contact.getContactType() == ContactType.BLACK_LIST) {
            if(getContactByUsers(contact.getToUserId(), contact.getFromUserId())!=null)
                getContactByUsers(contact.getToUserId(), contact.getFromUserId())
                        .setContactType(ContactType.NONE);
            updateContact(getContactByUsers(contact.getToUserId(), contact.getFromUserId()));
            return updateContact(contact);
        }
        if (contact.getContactType() == ContactType.FRIEND_REQUEST) {
            if (ModelFacade.getInstance().getModel().getContacts().stream()
                    .filter(contact1 -> contact1.getFromUserId() == contact.getToUserId()
                            && contact1.getToUserId() == contact.getFromUserId()
                            && contact1.getContactType() == ContactType.FRIEND_REQUEST).count() == 1) {
                contact.setContactType(ContactType.FRIEND);
                getContactByUsers(contact.getToUserId(), contact.getFromUserId()).setContactType(ContactType.FRIEND);
                updateContact(getContactByUsers(contact.getToUserId(), contact.getFromUserId()));
                return updateContact(contact);
            }
            return updateContact(contact);
        }
        return updateContact(contact);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Contact> getFriendsByUserIdAndContactGroup(long userId,
                                                                 long contactGroupId)
            throws TaskManagerException {
        return dataStore.getContacts()
                .stream().filter(contact -> userId == contact.getFromUserId()
                        && contactGroupId == contact.getContactGroupId()
                        && contact.getContactType()== ContactType.FRIEND)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Contact> getBlackListContacts(long userId)
            throws TaskManagerException {
        return dataStore.getContacts()
                .stream().filter(contact -> userId == contact.getFromUserId()
                        && contact.getContactType() == ContactType.BLACK_LIST)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Contact> getOutcomingFriendRequest(long fromUserId)
            throws TaskManagerException {
        return dataStore.getContacts()
                .stream().filter(contact -> fromUserId == contact.getFromUserId()
                        && contact.getContactType() == ContactType.FRIEND_REQUEST)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Contact> getIncomingFriendRequest(long toUserId)
            throws TaskManagerException {
        return dataStore.getContacts()
                .stream().filter(contact -> toUserId == contact.getToUserId()
                        && contact.getContactType() == ContactType.FRIEND_REQUEST)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Contact> getContactsByUserId(long userId)
            throws TaskManagerException {
        return dataStore.getContacts()
                .stream().filter(contact -> userId == contact.getFromUserId())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Contact getContactByUsers(long fromUserId, long toUserId)
            throws TaskManagerException {
        Contact contact = dataStore.getContacts()
                .stream().filter(currentContact -> fromUserId == currentContact.getFromUserId()
                        && toUserId == currentContact.getToUserId()).findFirst().orElse(null);
        if (contact == null) {
            contact = new Contact(fromUserId, toUserId, ContactType.NONE);
            dataStore.getContacts()
                    .add(contact);
        }
        return contact;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Contact updateContact(Contact contact) throws TaskManagerException {
        Contact updatedContact = getContactByUsers(contact.getFromUserId(),
                contact.getToUserId());
        updatedContact.setContactType(contact.getContactType());
        if(contact.getContactType()==ContactType.FRIEND) {
            updatedContact.setContactGroupId(contact.getContactGroupId());
        }
        appControllerInterface.notifyObservers(new ObserverEvent());
        return contact;
    }
}
