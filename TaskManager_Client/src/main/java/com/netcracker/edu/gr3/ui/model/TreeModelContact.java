package com.netcracker.edu.gr3.ui.model;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.ContactControllerInterface;
import com.netcracker.edu.gr3.controllers.ContactGroupControllerInterface;
import com.netcracker.edu.gr3.controllers.UserControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Contact;
import com.netcracker.edu.gr3.model.ContactGroup;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.util.ControllerProvider;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;

public class TreeModelContact implements TreeModel {
    private Object rootNode;
    public static final Object ROOT_CONTACT_GROUP_AND_CONTACT = new Object();
    public static final Object ROOT_IN_OUT_BLACK_LIST = new Object();
    public static final Object ROOT_INCOMING = new Object();
    public static final Object ROOT_OUTCOMING = new Object();
    public static final Object ROOT_BLACK_LIST = new Object();
    public TreeModelContact(Object rootNode) {
        this.rootNode = rootNode;
    }

    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        try {
            if (parent.equals(ROOT_IN_OUT_BLACK_LIST)) {
                if (index == 0)
                    return ROOT_INCOMING;
                if (index == 1)
                    return ROOT_OUTCOMING;
                if (index == 2)
                    return ROOT_BLACK_LIST;
            }
            if (parent.equals(ROOT_INCOMING)) {
                return ControllerProvider.getProvider().
                                getController(UserControllerInterface.class).getUserById(
                                ((ArrayList<Contact>) (ControllerProvider.getProvider().
                                        getController(ContactControllerInterface.class).getIncomingFriendRequest(
                                        ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser()))).get(index).getFromUserId());
            }
            if (parent.equals((ROOT_OUTCOMING))) {
                return ControllerProvider.getProvider().
                        getController(UserControllerInterface.class).getUserById(
                                ((ArrayList<Contact>) (ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).getOutcomingFriendRequest(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()))).get(index).getToUserId());
            }
            if (parent.equals(ROOT_BLACK_LIST)) {
                return ControllerProvider.getProvider().
                        getController(UserControllerInterface.class).getUserById(
                                ((ArrayList<Contact>) (
                        ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class)
                                .getBlackListContacts(
                                        ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser()))).get(index).getToUserId());
            }
            ArrayList<ContactGroup> nodesContactGroup = (ArrayList<ContactGroup>) (
                    ControllerProvider.getProvider()
                            .getController(ContactGroupControllerInterface.class)
                            .getContactGroupsByUserId(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser()));
            if(nodesContactGroup!=null) {
                if (parent.equals(ROOT_CONTACT_GROUP_AND_CONTACT)) {
                    if(index<nodesContactGroup.size()) {
                        return nodesContactGroup.get(index);
                    }
                    else{
                        return ControllerProvider.getProvider().
                                getController(UserControllerInterface.class).getUserById(
                                        ((ArrayList<Contact>) (ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class).
                                getFriendsByUserIdAndContactGroup(
                                        ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser(), 0)))
                                .get(index-nodesContactGroup.size()).getToUserId());
                    }
                } else {
                    return ControllerProvider.getProvider().
                            getController(UserControllerInterface.class).getUserById(
                                    ((ArrayList<Contact>) (ControllerProvider.getProvider().
                            getController(ContactControllerInterface.class).
                            getFriendsByUserIdAndContactGroup(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser(), ((ContactGroup) parent).
                                            getContactGroupId()))).get(index).getToUserId());

                }
            }
            else return null;

        } catch (TaskManagerException e) {
            return null;
        }
    }

    @Override
    public int getChildCount(Object parent) {
        try {
            if (parent.equals(ROOT_IN_OUT_BLACK_LIST)) {
                return 3;
            }
            if (parent.equals(ROOT_INCOMING)) {
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).getIncomingFriendRequest(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()).size();
            }
            if (parent.equals((ROOT_OUTCOMING))) {
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).getOutcomingFriendRequest(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()).size();
            }
            if (parent.equals(ROOT_BLACK_LIST)) {
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class)
                        .getBlackListContacts(
                                ControllerProvider.getProvider()
                                        .getController(ApplicationControllerInterface.class)
                                        .getCurrentUser()).size();
            }
            ArrayList<ContactGroup> nodesContactGroup = (ArrayList<ContactGroup>) (
                    ControllerProvider.getProvider()
                            .getController(ContactGroupControllerInterface.class)
                            .getContactGroupsByUserId(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser()));
            if(nodesContactGroup!=null) {
                if (parent.equals(ROOT_CONTACT_GROUP_AND_CONTACT)) {
                    return nodesContactGroup.size()+ ControllerProvider.getProvider().
                            getController(ContactControllerInterface.class)
                            .getFriendsByUserIdAndContactGroup(ControllerProvider.getProvider()
                                    .getController(ApplicationControllerInterface.class)
                                    .getCurrentUser(),0).size();
                } else {
                     return ControllerProvider.getProvider().
                            getController(ContactControllerInterface.class).
                            getFriendsByUserIdAndContactGroup(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser(), ((ContactGroup) parent).
                                            getContactGroupId()).size();
                }
            }
            else return 0;

        } catch (TaskManagerException e) {
            return 0;
        }
    }

    @Override
    public boolean isLeaf(Object node) {
        if (node instanceof User) return true;
        try {
            if (node.equals(ROOT_IN_OUT_BLACK_LIST)) {
                return false;
            }
            if (node.equals(ROOT_INCOMING)) {
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).getIncomingFriendRequest(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()).size() == 0;
            }
            if (node.equals((ROOT_OUTCOMING))) {
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).getOutcomingFriendRequest(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()).size() == 0;
            }
            if (node.equals(ROOT_BLACK_LIST)) {
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class)
                        .getBlackListContacts(
                                ControllerProvider.getProvider()
                                        .getController(ApplicationControllerInterface.class)
                                        .getCurrentUser()).size() == 0;
            }
            ArrayList<ContactGroup> nodesContactGroup = (ArrayList<ContactGroup>) (
                    ControllerProvider.getProvider()
                            .getController(ContactGroupControllerInterface.class)
                            .getContactGroupsByUserId(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser()));
            if (node.equals(ROOT_CONTACT_GROUP_AND_CONTACT)) {
                return false;
            } else {
                if (nodesContactGroup!=null)
                return ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).
                        getFriendsByUserIdAndContactGroup(
                                ControllerProvider.getProvider()
                                        .getController(ApplicationControllerInterface.class)
                                        .getCurrentUser(), ((ContactGroup) node).
                                        getContactGroupId()).size() == 0;
                else return true;
            }
        } catch (TaskManagerException e) {
            return true;
        }
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent == null || child == null)
            return -1;
            if (parent.equals(ROOT_IN_OUT_BLACK_LIST)) {
                if (child.equals(ROOT_INCOMING))
                    return 0;
                if (child.equals(ROOT_OUTCOMING))
                    return 1;
                if (child.equals(ROOT_BLACK_LIST))
                    return 2;
            }
            if (parent.equals(ROOT_INCOMING)) {
                try {
                    return ((ArrayList<Contact>) (ControllerProvider.getProvider().
                            getController(ContactControllerInterface.class).getIncomingFriendRequest(
                            ControllerProvider.getProvider()
                                    .getController(ApplicationControllerInterface.class)
                                    .getCurrentUser()))).indexOf(
                            ControllerProvider.getProvider().
                                    getController(ContactControllerInterface.class)
                                    .getContactByUsers(ControllerProvider.getProvider()
                                                    .getController(ApplicationControllerInterface.class)
                                                    .getCurrentUser(),
                                            ((User) child).getUserId()));
                } catch (TaskManagerException e) {
                    return -1;

                }
            }
        if (parent.equals((ROOT_OUTCOMING))) {
            try {
                return ((ArrayList<Contact>) (ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).getOutcomingFriendRequest(
                        ControllerProvider.getProvider()
                                .getController(ApplicationControllerInterface.class)
                                .getCurrentUser()))).indexOf(
                        ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class)
                                .getContactByUsers(ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser(),
                                        ((User) child).getUserId()));
            } catch (TaskManagerException e) {
                return -1;
            }
        }
        if (parent.equals(ROOT_BLACK_LIST)) {
            try {
                return ((ArrayList<Contact>) (
                        ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class)
                                .getBlackListContacts(
                                        ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser()))).indexOf(
                        ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class)
                                .getContactByUsers(ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser(),
                                        ((User) child).getUserId()));
            } catch (TaskManagerException e) {
                return -1;
            }
        }
        try {
            ArrayList<ContactGroup> nodesContactGroup = (ArrayList<ContactGroup>) (
                    ControllerProvider.getProvider()
                            .getController(ContactGroupControllerInterface.class)
                            .getContactGroupsByUserId(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser()));

            if (parent.equals(ROOT_CONTACT_GROUP_AND_CONTACT)) {
                if (child instanceof ContactGroup)
                    return nodesContactGroup.indexOf(child);
                else {
                    return ((ArrayList<Contact>) (ControllerProvider.getProvider().
                            getController(ContactControllerInterface.class).
                            getFriendsByUserIdAndContactGroup(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser(), 0))).indexOf(
                            ControllerProvider.getProvider().
                                    getController(ContactControllerInterface.class)
                                    .getContactByUsers(ControllerProvider.getProvider()
                                                    .getController(ApplicationControllerInterface.class)
                                                    .getCurrentUser(),
                                            ((User) child).getUserId()));
                }
            } else {
                return ((ArrayList<Contact>) (ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).
                        getFriendsByUserIdAndContactGroup(
                                ControllerProvider.getProvider()
                                        .getController(ApplicationControllerInterface.class)
                                        .getCurrentUser(), ((ContactGroup) parent).
                                        getContactGroupId()))).indexOf(
                        ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class)
                                .getContactByUsers(ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser(),
                                        ((User) child).getUserId()));
            }
        }
        catch (TaskManagerException e){
            return -1;
        }

    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
