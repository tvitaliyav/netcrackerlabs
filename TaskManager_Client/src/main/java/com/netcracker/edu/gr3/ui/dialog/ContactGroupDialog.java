package com.netcracker.edu.gr3.ui.dialog;

import com.netcracker.edu.gr3.controllers.ContactGroupControllerInterface;
import com.netcracker.edu.gr3.controllers.local.ApplicationController;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.ContactGroup;
import com.netcracker.edu.gr3.util.ControllerProvider;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class ContactGroupDialog extends JDialog {
    public static final String CREATE_CONTACT_GROUP = "Create Contact Group";
    public static final String CHANGE_CONTACT_GROUP = "Change Contact Group";
    private String action;
    private long idContactGroup;
    private JPanel contactGroupPanel;
    private JLabel labelName;
    private JTextField textName;
    private JButton buttonCreateGroup;
    private JButton buttonCancel;
    public ContactGroupDialog(String action){
        try {
            if(action.equals(CREATE_CONTACT_GROUP)) {
                new ContactGroupDialog(action,0).setVisible(true);
            }
            else throw new TaskManagerException(ErrorsCodes.INCORRECT_ACTION_IN_CONTACT_GROUP);
        }
        catch (TaskManagerException e){
            handleException(e);
        }
    }
    public ContactGroupDialog(String action, long contactGroupId) {
        this.action = action;
        this.idContactGroup = contactGroupId;
        setModal(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        contactGroupPanel = new JPanel(new MigLayout());
        JPanel informationPanel = new JPanel(new MigLayout());
        labelName = new JLabel();
        textName = new JTextField();
        JPanel panelButtons = new JPanel(new MigLayout());
        buttonCreateGroup = new JButton();
        buttonCancel = new JButton();
        localizeView();
        if(action.equals(CHANGE_CONTACT_GROUP)) {
            try {
                textName.setText(
                        ControllerProvider.getProvider().
                                getController(ContactGroupControllerInterface.class)
                                .getContactGroupById(contactGroupId).getName());
            } catch (TaskManagerException e) {
                handleException(e);
            }
        }
        informationPanel.add(labelName);
        informationPanel.add(textName,"wrap,pushx, growx");
        panelButtons.add(buttonCreateGroup);
        panelButtons.add(buttonCancel);
        contactGroupPanel.add(informationPanel,"wrap,pushx, growx");
        contactGroupPanel.add(panelButtons);
        add(contactGroupPanel);
        buttonCancel.addActionListener(e -> SwingUtilities.invokeLater(this::dispose));
        buttonCreateGroup.addActionListener(e ->{
            try {
                if (!textName.getText().equals("")) {
                    ContactGroup contactGroup = new ContactGroup();
                    if(action.equals(CHANGE_CONTACT_GROUP)) {
                        contactGroup.setContactGroupId(contactGroupId);
                        contactGroup.setName(textName.getText());
                        ControllerProvider.getProvider().
                                getController(ContactGroupControllerInterface.class)
                                .updateContactGroup(contactGroup);
                    }
                    else {
                        contactGroup.setName(textName.getText());
                        contactGroup.setUserId(ControllerProvider.getProvider().
                                getController(ApplicationController.class).getCurrentUser());
                        ControllerProvider.getProvider().
                                getController(ContactGroupControllerInterface.class)
                                .createContactGroup(contactGroup);
                    }
                    dispose();
                }
            } catch (TaskManagerException e1) {
                handleException(e1);
            }
        });
    }
    private void handleException(TaskManagerException exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        JOptionPane.showMessageDialog(this,
                bundleError.getString(
                        String.valueOf(exception.getErrorCode())));

    }
    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.contactGroupDialog", Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        contactGroupPanel.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        labelName.setText(bundle.getString("labelName.name"));
        if(action.equals(CHANGE_CONTACT_GROUP))buttonCreateGroup.setText(bundle.getString("buttonChangeGroup.name"));
        else buttonCreateGroup.setText(bundle.getString("buttonCreateGroup.name"));
        buttonCreateGroup.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCreateGroup.width")),
                Integer.parseInt(bundle.getString("buttonCreateGroup.height"))));
        buttonCancel.setText(bundle.getString("buttonCancel.name"));
        buttonCancel.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCancel.width")),
                Integer.parseInt(bundle.getString("buttonCancel.height"))));
    }
    void setIdContactGroup(long idContactGroup) {
        this.idContactGroup = idContactGroup;
    }
}
