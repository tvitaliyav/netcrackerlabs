package com.netcracker.edu.gr3.ui.model;

public class CheckNode {
    private Object object;
    private boolean selected;

    public CheckNode(Object object, boolean selected) {
        this.object = object;
        this.selected = selected;
    }
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}