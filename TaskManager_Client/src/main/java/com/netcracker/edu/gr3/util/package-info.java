/**
 * The package provides the ability to configure
 * controllers and marshalling / unmarshalling objects.
 */
package com.netcracker.edu.gr3.util;
