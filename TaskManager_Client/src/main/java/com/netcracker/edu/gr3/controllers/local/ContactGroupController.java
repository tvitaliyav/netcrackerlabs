package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.ContactGroupControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Contact;
import com.netcracker.edu.gr3.model.ContactGroup;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class "ContactGroupController" implements the
 * interface ContactGroupControllerInterface.
 *
 * @author Ekatherina Denisova, Olga Sudakova, Vitaliya Tikhonova
 * @version 1.0.0
 */

@Component
public class ContactGroupController implements ContactGroupControllerInterface {
    /**
     * Object that provides work with DataStore.
     */
    @Autowired
    private DataStore dataStore;
    /**
     * Object that provides work with ApplicationControllerInterface.
     */
    @Autowired
    private ApplicationControllerInterface appControllerInterface;
    /**
     * Property is LOGGER.
     *
     * @see Logger .
     */
    private static final Logger LOGGER = Logger
            .getLogger(ContactGroupController.class);
    /**
     * @inheritDoc
     */
    @Override
    public ContactGroup createContactGroup(ContactGroup contactGroup)
            throws TaskManagerException {
        LOGGER.info("Method createContactGroup called");
        if (dataStore.getContactGroups()
                .stream().anyMatch(contactGroup1 ->
                        contactGroup1.getName().
                                equals(contactGroup.getName()) &&
                                contactGroup1.getUserId() == contactGroup.getUserId())) {
            throw new TaskManagerException(ErrorsCodes
                    .NOT_UNIQUE_CONTACT_GROUP_NAME_AND_USER_ID_EXCEPTION);
        }
        contactGroup.setContactGroupId(dataStore.getMap()
                .get(ContactGroup.class).generateID());
        LOGGER.debug("ID was generated");
        dataStore.getContactGroups().add(contactGroup);
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("contactGroup was added, method createContactGroup end");
        return contactGroup;
    }

    /**
     * @inheritDoc
     */
    @Override
    public ContactGroup getContactGroupById(long id)
            throws TaskManagerException {
        LOGGER.info("Method getContactGroupById called");
        return dataStore.getContactGroups()
                .stream().filter(contactGroup ->
                        contactGroup.getContactGroupId()==id)
                .findFirst().orElseThrow(() ->
                new TaskManagerException(ErrorsCodes.
                        CONTACT_GROUP_NOT_FOUND_EXCEPTION));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<ContactGroup> getContactGroupsByUserId(long userId)
            throws TaskManagerException {
        LOGGER.info("Method getContactGroupByUserId called");
        return dataStore.getContactGroups()
                .stream().filter(contactGroup ->
                        contactGroup.getUserId()==userId)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @inheritDoc
     */
    @Override
    public ContactGroup updateContactGroup(ContactGroup contactGroup)
            throws TaskManagerException {
        LOGGER.info("Method getContactGroupById called");
        if (dataStore.getContactGroups()
                .stream().anyMatch(contactGroup1 ->
                        contactGroup1.getName().equals(contactGroup.getName())
                                &&contactGroup1.getUserId()==contactGroup.getUserId())) {
            throw new TaskManagerException(ErrorsCodes
                    .NOT_UNIQUE_CONTACT_GROUP_NAME_AND_USER_ID_EXCEPTION);
        }
        ContactGroup updateContactGroup =  getContactGroupById(contactGroup.getContactGroupId());
        updateContactGroup.setName(contactGroup.getName());
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("Change name, method getUserById end");
        return updateContactGroup;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteContactGroup(long id) throws TaskManagerException {
        LOGGER.info("Method deleteContactGroup called");
        ArrayList<Contact> contacts = dataStore.getContacts()
                .stream().filter(contact -> contact.getContactGroupId()==id)
                .collect(Collectors.toCollection(ArrayList::new));
        for(Contact contact: contacts){
            contact.setContactGroupId(0);
        }
        dataStore.getContactGroups()
                .removeIf(contactGroup -> contactGroup.getContactGroupId()==id);
        appControllerInterface.notifyObservers(new ObserverEvent());

    }
}
