package com.netcracker.edu.gr3.ui.model;

import com.netcracker.edu.gr3.controllers.*;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.model.Contact;
import com.netcracker.edu.gr3.model.ContactType;
import com.netcracker.edu.gr3.ui.MainForm;
import com.netcracker.edu.gr3.ui.dialog.*;
import com.netcracker.edu.gr3.util.ControllerProvider;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class PopupMenuHelper {
    public PopupMenuHelper() {
    }

    public static JPopupMenu popupMenuBlackList(long userId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem contactAddInFriends = new JMenuItem();
        JMenuItem contactDeleteFromList = new JMenuItem();
        JMenuItem contactProfile = new JMenuItem();
        contactAddInFriends.setText(bundle.getString("popupmenu.contact.addInFriends"));
        contactDeleteFromList.setText(bundle.getString("popupmenu.contact.deleteFromList"));
        contactProfile.setText(bundle.getString("popupmenu.contact.profile"));
        contactDeleteFromList.addActionListener(e -> deleteFromList(userId));
        contactAddInFriends.addActionListener(e -> addInFriends(userId));
        contactProfile.addActionListener(e -> getContactProfile(userId));
        jPopupMenu.add(contactAddInFriends);
        jPopupMenu.add(contactDeleteFromList);
        jPopupMenu.add(contactProfile);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuIncoming(long userId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem contactAddInBlackList = new JMenuItem();
        JMenuItem contactDeleteFromList = new JMenuItem();
        JMenuItem contactAddInFriends = new JMenuItem();
        JMenuItem contactProfile = new JMenuItem();
        contactAddInBlackList.addActionListener(e -> addInBlackList(userId));
        contactDeleteFromList.addActionListener(e -> deleteFromList(userId));
        contactAddInFriends.addActionListener(e -> addInFriends(userId));
        contactProfile.addActionListener(e -> getContactProfile(userId));
        contactAddInBlackList.setText(bundle.getString("popupmenu.contact.addInBlackList"));
        contactAddInFriends.setText(bundle.getString("popupmenu.contact.addInFriends"));
        contactDeleteFromList.setText(bundle.getString("popupmenu.contact.deleteFromList"));
        contactProfile.setText(bundle.getString("popupmenu.contact.profile"));
        contactDeleteFromList.addActionListener(e1 -> deleteFromList(userId));
        jPopupMenu.add(contactAddInFriends);
        jPopupMenu.add(contactAddInBlackList);
        jPopupMenu.add(contactDeleteFromList);
        jPopupMenu.add(contactProfile);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuOutcoming(long userId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem contactAddInBlackList = new JMenuItem();
        JMenuItem contactDeleteFromList = new JMenuItem();
        JMenuItem contactProfile = new JMenuItem();
        contactAddInBlackList.addActionListener(e -> addInBlackList(userId));
        contactDeleteFromList.addActionListener(e -> deleteFromList(userId));
        contactProfile.addActionListener(e -> getContactProfile(userId));
        contactAddInBlackList.setText(bundle.getString("popupmenu.contact.addInBlackList"));
        contactDeleteFromList.setText(bundle.getString("popupmenu.contact.deleteFromList"));
        contactProfile.setText(bundle.getString("popupmenu.contact.profile"));
        jPopupMenu.add(contactAddInBlackList);
        jPopupMenu.add(contactDeleteFromList);
        jPopupMenu.add(contactProfile);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuContactFriend(long userId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem contactAddInGroup = new JMenuItem();
        JMenuItem contactDeleteFromFriends = new JMenuItem();
        JMenuItem contactAddInBlackList = new JMenuItem();
        JMenuItem contactProfile = new JMenuItem();
        contactAddInGroup.addActionListener(e -> addInGroup(userId));
        contactDeleteFromFriends.addActionListener(e -> deleteContactFromFriends(userId));
        contactAddInBlackList.addActionListener(e -> addInBlackList(userId));
        contactProfile.addActionListener(e -> getContactProfile(userId));
        contactAddInGroup.setText(bundle.getString("popupmenu.contact.addInGroup"));
        contactDeleteFromFriends.setText(bundle.getString("popupmenu.contact.deleteFromFriends"));
        contactAddInBlackList.setText(bundle.getString("popupmenu.contact.addInBlackList"));
        contactProfile.setText(bundle.getString("popupmenu.contact.profile"));
        jPopupMenu.add(contactAddInGroup);
        jPopupMenu.add(contactDeleteFromFriends);
        jPopupMenu.add(contactAddInBlackList);
        jPopupMenu.add(contactProfile);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuContactFriendInGroup(long userId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem contactChangeGroup = new JMenuItem();
        JMenuItem contactDeleteFromFriends = new JMenuItem();
        JMenuItem contactDeleteFromGroup = new JMenuItem();
        JMenuItem contactAddInBlackList = new JMenuItem();
        JMenuItem contactProfile = new JMenuItem();
        contactChangeGroup.addActionListener(e -> changeGroupForContact(userId));
        contactDeleteFromFriends.addActionListener(e -> deleteContactFromFriends(userId));
        contactDeleteFromGroup.addActionListener(e -> deleteContactFromGroup(userId));
        contactAddInBlackList.addActionListener(e -> addInBlackList(userId));
        contactProfile.addActionListener(e -> getContactProfile(userId));
        contactChangeGroup.setText(bundle.getString("popupmenu.contact.changeGroup"));
        contactDeleteFromFriends.setText(bundle.getString("popupmenu.contact.deleteFromFriends"));
        contactDeleteFromGroup.setText(bundle.getString("popupmenu.contact.deleteFromGroup"));
        contactAddInBlackList.setText(bundle.getString("popupmenu.contact.addInBlackList"));
        contactProfile.setText(bundle.getString("popupmenu.contact.profile"));
        jPopupMenu.add(contactChangeGroup);
        jPopupMenu.add(contactDeleteFromGroup);
        jPopupMenu.add(contactDeleteFromFriends);
        jPopupMenu.add(contactAddInBlackList);
        jPopupMenu.add(contactProfile);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuContactGroup(long userId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem contactGroupDelete = new JMenuItem();
        JMenuItem contactGroupChange = new JMenuItem();
        contactGroupDelete.addActionListener(e -> deleteContactGroup(userId));
        contactGroupChange.addActionListener(e -> changeContactGroup(userId));
        contactGroupDelete.setText(bundle.getString("popupmenu.delete"));
        contactGroupChange.setText(bundle.getString("popupmenu.change"));
        jPopupMenu.add(contactGroupChange);
        jPopupMenu.add(contactGroupDelete);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuTask(long taskId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem taskDelete = new JMenuItem();
        JMenuItem taskAdd = new JMenuItem();
        taskDelete.setText(bundle.getString("popupmenu.delete"));
        taskAdd.setText(bundle.getString("popupmenu.add"));
        taskDelete.addActionListener(e -> deleteTask(taskId));
        taskAdd.addActionListener(e -> addTask(taskId));
        jPopupMenu.add(taskDelete);
        jPopupMenu.add(taskAdd);
        return jPopupMenu;
    }

    public static JPopupMenu popupMenuCalendar(long calendarId) {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.popupMenuHelper", Locale.getDefault());
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem calendarDelete = new JMenuItem();
        JMenuItem calendarChange = new JMenuItem();
        calendarDelete.setText(bundle.getString("popupmenu.delete"));
        calendarChange.setText(bundle.getString("popupmenu.change"));
        calendarChange.addActionListener(e -> changeCalendar(calendarId));
        calendarDelete.addActionListener(e -> deleteCalendar(calendarId));
        jPopupMenu.add(calendarChange);
        jPopupMenu.add(calendarDelete);
        return jPopupMenu;
    }

    private static void deleteFromList(long userId) {
        try {
            ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class).
                    createContact(new Contact(
                            ControllerProvider.getProvider()
                                    .getController(ApplicationControllerInterface.class)
                                    .getCurrentUser(), userId,
                            ContactType.NONE));
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void addInFriends(long userId) {
        try {
            if (ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .getContactByUsers(ControllerProvider.getProvider()
                            .getController(ApplicationControllerInterface.class)
                            .getCurrentUser(), userId)
                    .getContactType() != ContactType.FRIEND)
                ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class).
                        createContact(new Contact(
                                ControllerProvider.getProvider()
                                        .getController(ApplicationControllerInterface.class)
                                        .getCurrentUser(), userId, ContactType.FRIEND_REQUEST));
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    public static void deleteContactFromGroup(long userId) {
        try {
            Contact contact = ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .getContactByUsers(ControllerProvider.getProvider()
                            .getController(ApplicationControllerInterface.class)
                            .getCurrentUser(), userId);
            contact.setContactGroupId(0);
            ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .updateContact(contact);
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void deleteContactFromFriends(long userId) {
        try {
            Contact contactNone = ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .getContactByUsers(ControllerProvider.getProvider()
                            .getController(ApplicationControllerInterface.class)
                            .getCurrentUser(), userId);
            contactNone.setContactType(ContactType.NONE);
            ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .updateContact(contactNone);
            Contact contactFriendsRequest = ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .getContactByUsers(userId,
                            ControllerProvider.getProvider().getController(
                                    ApplicationControllerInterface.class)
                                    .getCurrentUser());
            contactFriendsRequest.setContactType(ContactType.FRIEND_REQUEST);
            ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .updateContact(contactFriendsRequest);
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void addInBlackList(long userId) {
        try {
            ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class).
                    createContact(new Contact(
                            ControllerProvider.getProvider()
                                    .getController(ApplicationControllerInterface.class)
                                    .getCurrentUser(), userId,
                            ContactType.BLACK_LIST));
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void getContactProfile(long userId) {
        new DialogUserDetails(DialogUserDetails.VIEW_PROFILE,
                userId);
    }

    private static void addInGroup(long userId) {
        try {
            new DialogListContactGroup().setVisible(true);
            Contact contact = ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class)
                    .getContactByUsers(ControllerProvider.getProvider()
                            .getController(ApplicationControllerInterface.class)
                            .getCurrentUser(), userId);
            if (contact.getContactType() == ContactType.FRIEND
                    && DialogListContactGroup.getSelectContactGroup() != null) {
                contact.setContactGroupId(DialogListContactGroup
                        .getSelectContactGroup().getContactGroupId());
                ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class)
                        .updateContact(contact);
            }
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void changeGroupForContact(long userId) {
        try {
            new DialogListContactGroup().setVisible(true);
            Contact contact = ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class).getContactByUsers(ControllerProvider.getProvider()
                    .getController(ApplicationControllerInterface.class)
                    .getCurrentUser(), userId);
            if(DialogListContactGroup
                    .getSelectContactGroup()!=null)
            contact.setContactGroupId(DialogListContactGroup
                    .getSelectContactGroup().getContactGroupId());
            ControllerProvider.getProvider().
                    getController(ContactControllerInterface.class).updateContact(contact);
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void deleteContactGroup(long contactGroupId) {
        try {
            ControllerProvider.getProvider().
                    getController(ContactGroupControllerInterface.class)
                    .deleteContactGroup(contactGroupId);
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void changeContactGroup(long contactGroupId) {
        new ContactGroupDialog(ContactGroupDialog
                .CHANGE_CONTACT_GROUP,contactGroupId).setVisible(true);
    }
    private static void deleteTask(long taskId) {
        try {
            ControllerProvider.getProvider().
                    getController(TaskControllerInterface.class)
                    .deleteTask(taskId);
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void addTask(long taskId) {
        new TaskDialog(TaskDialog.CREATE_TASK,taskId).setVisible(true);
    }

    private static void deleteCalendar(long calendarId){
        try {
            if(calendarId!=ControllerProvider.getProvider().
                    getController(UserControllerInterface.class)
                    .getUserById(ControllerProvider.getProvider().
                    getController(ApplicationControllerInterface.class)
                    .getCurrentUser()).getCalendarId()) {
                MainForm.getCalendars().remove(((ArrayList<Calendar>) ControllerProvider.getProvider().
                        getController(CalendarControllerInterface.class)
                        .getCalendarsByUserId(ControllerProvider.getProvider().
                                getController(ApplicationControllerInterface.class)
                                .getCurrentUser())).indexOf(ControllerProvider.getProvider().
                        getController(CalendarControllerInterface.class).getCalendarById(calendarId)));
            }
            ControllerProvider.getProvider().
                    getController(CalendarControllerInterface.class)
                    .deleteCalendar(calendarId);
        } catch (TaskManagerException err) {
            err.printStackTrace();
        }
    }

    private static void changeCalendar(long calendarId){
        new CalendarDialog(CalendarDialog.CHANGE_CALENDAR,calendarId).setVisible(true);
    }
}
