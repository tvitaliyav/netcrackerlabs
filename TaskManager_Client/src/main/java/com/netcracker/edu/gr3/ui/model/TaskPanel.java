package com.netcracker.edu.gr3.ui.model;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.TaskControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Task;
import com.netcracker.edu.gr3.model.TaskPriority;
import com.netcracker.edu.gr3.model.TaskStatus;
import com.netcracker.edu.gr3.ui.dialog.TaskDialog;
import com.netcracker.edu.gr3.util.ControllerProvider;
import com.toedter.calendar.JDateChooser;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;
import java.util.ResourceBundle;

public class TaskPanel extends JPanel {
    private String action;
    private JLabel labelName;
    private JTextField textName;
    private JLabel labelDescription;
    private JTextField textDescription;
    private JLabel labelPriority;
    private JLabel labelStatus;
    private JLabel labelStartDate;
    private JLabel labelEndDate;
    private JDateChooser chooserEndDate;
    private JDateChooser chooserStartDate;
    private JLabel labelDueDate;
    private JDateChooser chooserDueDate;
    private JLabel labelEstimationExecutionTime;
    private JTextField textEstimationExecutionTime;
    private JLabel labelError;
    private JButton buttonCreateTask;
    private JButton buttonCancel;

    public TaskPanel(String action,long taskId) {
        this.action=action;
        setLayout(new MigLayout());
        labelName = new JLabel();
        textName = new JTextField();
        labelPriority = new JLabel();
        JComboBox<TaskPriority> taskPriority = new JComboBox<>(TaskPriority.values());
        labelStatus = new JLabel();
        JComboBox<TaskStatus> taskStatus = new JComboBox<>(TaskStatus.values());
        taskStatus.setRenderer(new ListCellRenderer<TaskStatus>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends TaskStatus> list, TaskStatus value, int index, boolean isSelected, boolean cellHasFocus) {
                JLabel result = new JLabel();
                ResourceBundle bundle = ResourceBundle.getBundle(
                        "com.netcracker.edu.gr3.ui.treeModelTask", Locale.getDefault());
                switch (value) {
                    case NEW:
                        result.setText(bundle.getString("NEW"));
                        break;
                    case PLANNING:
                        result.setText(bundle.getString("PLANNING"));
                        break;
                    case PLANNED:
                        result.setText(bundle.getString("PLANNED"));
                        break;
                    case IN_PROGRESS:
                        result.setText(bundle.getString("IN_PROGRESS"));
                        break;
                    case ON_HOLD:
                        result.setText(bundle.getString("ON_HOLD"));
                        break;
                    case DONE:
                        result.setText(bundle.getString("DONE"));
                        break;
                    case NOT_DONE:
                        result.setText(bundle.getString("NOT_DONE"));
                        break;
                }
                return result;
            }
        });
        labelDescription = new JLabel();
        textDescription = new JTextField();
        labelStartDate = new JLabel();
        chooserStartDate = new JDateChooser();
        labelDueDate = new JLabel();
        chooserDueDate = new JDateChooser();
        labelEndDate = new JLabel();
        chooserEndDate = new JDateChooser();
        labelEstimationExecutionTime = new JLabel();
        textEstimationExecutionTime = new JTextField();
        labelError = new JLabel();
        labelError.setForeground(Color.RED);
        buttonCreateTask = new JButton();
        buttonCancel = new JButton();
        localizeView();
        if (action.equals(TaskDialog.CHANGE_TASK)) {
            try {
                Task currentTask = ControllerProvider.getProvider().getController(
                        TaskControllerInterface.class).getTasksByTaskId(taskId);
                textName.setText(currentTask.getName());
                taskPriority.setSelectedItem(currentTask.getPriority());
                taskStatus.setSelectedItem(currentTask.getStatus());
                textDescription.setText(currentTask.getDescription());
                if (currentTask.getStartDate() != null)
                    chooserStartDate.setDate(Date.valueOf(currentTask.getStartDate().toLocalDate()));
                if (currentTask.getDueDate() != null)
                    chooserDueDate.setDate(Date.valueOf(currentTask.getDueDate().toLocalDate()));
                if (currentTask.getEndDate() != null)
                    chooserEndDate.setDate(Date.valueOf(currentTask.getEndDate().toLocalDate()));
                if (currentTask.getEstimationExecutionTime() != null)
                    textEstimationExecutionTime.setText(String.valueOf(currentTask.getEstimationExecutionTime().getSeconds() / 3600));
            } catch (TaskManagerException e) {
                handleException(e);
            }
        }
        add(labelName);
        add(textName, "growx,pushx,wrap");
        add(labelPriority);
        add(taskPriority, "growx,pushx,wrap");
        add(labelStatus);
        add(taskStatus, "growx,pushx,wrap");
        add(labelDescription);
        add(textDescription, "growx,pushx,wrap");
        add(labelStartDate);
        add(chooserStartDate, "growx,pushx,wrap");
        add(labelDueDate);
        add(chooserDueDate, "growx,pushx,wrap");
        add(labelEndDate);
        add(chooserEndDate, "growx,pushx,wrap");
        add(labelEstimationExecutionTime);
        add(textEstimationExecutionTime, "growx,pushx,wrap");
        add(buttonCreateTask);
        if(action.equals(TaskDialog.CREATE_TASK)) add(buttonCancel);

        buttonCreateTask.addActionListener(e -> {
            try {
                if (!textName.getText().equals("")) {

                    if ((TaskDialog.CREATE_TASK).equals(action)) {
                        Task task = new Task();
                        task.setName(textName.getText());
                        task.setDescription(textDescription.getText());
                        task.setPriority((TaskPriority) taskPriority.getSelectedItem());
                        task.setStatus((TaskStatus) taskStatus.getSelectedItem());
                        if (chooserStartDate.getDate() != null)
                            task.setStartDate(LocalDateTime
                                    .ofInstant(Instant.ofEpochMilli(
                                            chooserStartDate.getDate().getTime()), ZoneId.systemDefault()));
                        if (chooserDueDate.getDate() != null)
                            task.setDueDate(LocalDateTime
                                    .ofInstant(Instant.ofEpochMilli(
                                            chooserDueDate.getDate().getTime()), ZoneId.systemDefault()));
                        if (chooserEndDate.getDate() != null)
                            task.setEndDate(LocalDateTime
                                    .ofInstant(Instant.ofEpochMilli(
                                            chooserEndDate.getDate().getTime()), ZoneId.systemDefault()));
                        task.setUserId(ControllerProvider.getProvider().getController(
                                ApplicationControllerInterface.class).getCurrentUser());
                        if (taskId != 0) {
                            task.setParentId(taskId);
                        } else task.setParentId(0);
                        task.setEndDate(null);
                        if (!textEstimationExecutionTime.getText().equals(""))
                            task.setEstimationExecutionTime(Duration.ofHours(Long
                                    .parseLong(textEstimationExecutionTime.getText())));
                        ControllerProvider.getProvider().getController(
                                TaskControllerInterface.class).createTask(task);
                    } else {
                        Task task = ControllerProvider.getProvider().getController(
                                TaskControllerInterface.class).getTasksByTaskId(taskId);
                        task.setName(textName.getText());
                        task.setDescription(textDescription.getText());
                        task.setPriority((TaskPriority) taskPriority.getSelectedItem());
                        task.setStatus((TaskStatus) taskStatus.getSelectedItem());
                        if (chooserStartDate.getDate() != null)
                            task.setStartDate(LocalDateTime
                                    .ofInstant(Instant.ofEpochMilli(
                                            chooserStartDate.getDate().getTime()), ZoneId.systemDefault()));
                        if (chooserDueDate.getDate() != null)
                            task.setDueDate(LocalDateTime
                                    .ofInstant(Instant.ofEpochMilli(
                                            chooserDueDate.getDate().getTime()), ZoneId.systemDefault()));
                        if (chooserEndDate.getDate() != null)
                            task.setEndDate(LocalDateTime
                                    .ofInstant(Instant.ofEpochMilli(
                                            chooserEndDate.getDate().getTime()), ZoneId.systemDefault()));
                        task.setUserId(ControllerProvider.getProvider().getController(
                                ApplicationControllerInterface.class).getCurrentUser());
                        if (!textEstimationExecutionTime.getText().equals(""))
                            task.setEstimationExecutionTime(Duration.ofHours(Long
                                    .parseLong(textEstimationExecutionTime.getText())));
                        ControllerProvider.getProvider().getController(
                                TaskControllerInterface.class).updateTask(task);
                    }
                }
            } catch (TaskManagerException e1) {
                handleException(e1);
            }
        });
    }
    private void handleException(TaskManagerException exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        new JOptionPane(bundleError.getString(
                String.valueOf(exception.getErrorCode()))).setVisible(true);

    }
    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.taskDialog", Locale.getDefault());
        labelName.setText(bundle.getString("labelName.name"));
        labelDescription.setText(bundle.getString("labelDescription.name"));
        labelPriority.setText(bundle.getString("labelPriority.name"));
        labelStatus.setText(bundle.getString("labelStatus.name"));
        labelStartDate.setText(bundle.getString("labelStartDate.name"));
        labelDueDate.setText(bundle.getString("labelDueDate.name"));
        labelEndDate.setText(bundle.getString("labelEndDate.name"));
        labelEstimationExecutionTime.setText(bundle.getString("labelEstimationExecutionTime.name"));
        if ((TaskDialog.CREATE_TASK).equals(action)) buttonCreateTask.setText(bundle.getString("buttonCreateTask.name"));
        else buttonCreateTask.setText(bundle.getString("buttonChangeTask.name"));
        buttonCreateTask.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCreateTask.width")),
                Integer.parseInt(bundle.getString("buttonCreateTask.height"))));
        buttonCancel.setText(bundle.getString("buttonCancel.name"));
        buttonCancel.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCancel.width")),
                Integer.parseInt(bundle.getString("buttonCancel.height"))));
        labelError.setText(" ");
    }


    public JButton getButtonCreateTask() {
        return buttonCreateTask;
    }

    public JButton getButtonCancel() {
        return buttonCancel;
    }
}
