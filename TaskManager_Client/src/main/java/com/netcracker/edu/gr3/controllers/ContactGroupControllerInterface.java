package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.ContactGroup;

import java.util.Collection;

/**
 * Interface "ContactGroupControllerInterface" defines actions
 * with contact group.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public interface ContactGroupControllerInterface {
    /**
     * Method for creating a new contact group.
     *
     * @param contactGroup - contact group for creating
     * @return contact group
     * @throws TaskManagerException Inherited from class "Exception"
     */
    ContactGroup createContactGroup(ContactGroup contactGroup)
            throws TaskManagerException;

    /**
     * Method to get contact group by ID.
     *
     * @param id - unique contact group`s ID
     * @return contact group
     * @throws TaskManagerException Inherited from class "Exception"
     */
    ContactGroup getContactGroupById(long id) throws TaskManagerException;

    /**
     * Method to get сontact groups by user ID.
     *
     * @param userId - unique user id
     * @return Collection of сontact groups
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<ContactGroup> getContactGroupsByUserId(long userId)
            throws TaskManagerException;

    /**
     * Method for update contact group.
     *
     * @param contactGroup - contact group for creating
     * @return contact group
     * @throws TaskManagerException Inherited from class "Exception"
     */
    ContactGroup updateContactGroup(ContactGroup contactGroup)
            throws TaskManagerException;

    /**
     * Method for delete contact group by id.
     *
     * @param id - unique contact group`s ID
     * @throws TaskManagerException Inherited from class "Exception"
     */
    void deleteContactGroup(long id) throws TaskManagerException;

}

