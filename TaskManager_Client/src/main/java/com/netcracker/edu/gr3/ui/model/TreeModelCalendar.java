package com.netcracker.edu.gr3.ui.model;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.CalendarControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.ui.MainForm;
import com.netcracker.edu.gr3.util.ControllerProvider;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;

public class TreeModelCalendar implements TreeModel {
    private Object rootNode;
    public static final Object ROOT_CALENDAR = new Object();

    public TreeModelCalendar(Object rootNode) {
        this.rootNode = rootNode;
    }

    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent.equals(ROOT_CALENDAR))
                return MainForm.getCalendars().get(index);
        return null;
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent.equals(ROOT_CALENDAR))
            try {
                return ControllerProvider.getProvider().getController(
                        CalendarControllerInterface.class).getCalendarsByUserId(ControllerProvider
                        .getProvider().getController((ApplicationControllerInterface.class))
                        .getCurrentUser()).size();
            } catch (TaskManagerException e) {
                return 0;
            }
        return 0;
    }

    @Override
    public boolean isLeaf(Object node) {
        return node instanceof Calendar;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent == null || child == null)
            return -1;
        if (parent.equals(ROOT_CALENDAR))
            try {
                return ((ArrayList<Calendar>) ControllerProvider.getProvider().getController(
                        CalendarControllerInterface.class).getCalendarsByUserId(ControllerProvider
                        .getProvider().getController((ApplicationControllerInterface.class))
                        .getCurrentUser())).indexOf(child);
            } catch (TaskManagerException e) {
                return -1;
            }
        return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
