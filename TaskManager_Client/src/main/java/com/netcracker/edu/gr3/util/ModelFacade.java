/**
 * Info about this package doing something for package-info.java file.
 */
package com.netcracker.edu.gr3.util;

import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.DataStore;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

/**
 * Class "ModelFacade" provides work with DataStore data com.netcracker.edu.gr3.model.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public final class ModelFacade {

    /**
     * Static variable defines an instance of the class.
     */
    private static ModelFacade instance = null;
    /**
     * Path to the file to save or load the com.netcracker.edu.gr3.model.
     */
    private static final String FILE_PATH = "data.xml";
    /**
     * Variable for com.netcracker.edu.gr3.model autosave.
     */
    private MyTimerTask myTimerTask;
    /**
     * Variable that is used to schedule the execution of a task.
     */
    private Timer timer;
    /**
     * Data com.netcracker.edu.gr3.model.
     */
    private DataStore model;
    /**
     * Property is LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(ModelFacade.class);
    /**
     * Saving period of the data com.netcracker.edu.gr3.model.
     */
    private static final int SAVING_PERIOD = 15000;

    /**
     * Default private constructor - сreates a new object.
     *
     * @throws TaskManagerException if failed to load object from file
     */
    private ModelFacade() throws TaskManagerException {
        LOGGER.info("Create ModelFacade object");
        loadModelAndStartTimer();
    }

    /**
     * Method to get field value {@link ModelFacade#instance}.
     *
     * @return Model instance
     * @throws TaskManagerException if failed to get com.netcracker.edu.gr3.model instance
     */
    public static synchronized ModelFacade getInstance()
            throws TaskManagerException {
        try {
            LOGGER.info("Method getInstance called");
            if (instance == null) {
                LOGGER.info("Object not initialized");
                instance = new ModelFacade();
                LOGGER.info("Object has been initialized");
            }
            return instance;
        } catch (TaskManagerException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .OBJECT_INITIALIZATION_EXCEPTION);
        } finally {
            LOGGER.info("Method getInstance end");
        }
    }

    /**
     * Method to get field value {@link ModelFacade#model}.
     *
     * @return Returns a data com.netcracker.edu.gr3.model.
     */
    public DataStore getModel() {
        return model;
    }

    /**
     * Method for determining a data com.netcracker.edu.gr3.model {@link ModelFacade#model}.
     *
     * @param model - data com.netcracker.edu.gr3.model
     */
    public void setModel(DataStore model) {
        this.model = model;
    }

    /**
     * Method that, when an object is created, loads the com.netcracker.edu.gr3.model and starts a
     * timer for AutoSave {@link ModelFacade#timer}.
     * {@link ModelFacade#myTimerTask}.
     *
     * @throws TaskManagerException if failed to load object from file
     */
    private void loadModelAndStartTimer() throws TaskManagerException {
        try {
            LOGGER.info("Method loadModelAndStartTimer called");
            loadModel();
        } catch (TaskManagerException ex) {
            if (ex.getErrorCode() != ErrorsCodes.DATA_FILE_NOT_EXISTS) {
                LOGGER.error(ex.getMessage(), ex);
                throw new TaskManagerException(ErrorsCodes
                        .LOAD_MODEL_FROM_FILE_EXCEPTION);
            } else {
                model = new DataStore();
            }
        }
        myTimerTask = new MyTimerTask();
        timer = new Timer();
        timer.schedule(myTimerTask, SAVING_PERIOD);
        LOGGER.info("Method loadModelAndStartTimer end");
    }

    /**
     * Method to save an com.netcracker.edu.gr3.model to file.
     *
     * @throws TaskManagerException if failed to save com.netcracker.edu.gr3.model to file
     */
    private void saveModel() throws TaskManagerException {
        try {
            LOGGER.info("Method saveModel called");
            if (model != null) {
                JaxbParser.saveToFile(FILE_PATH, getModel());
            } else {
                LOGGER.warn("Model not initialized");
            }
        } catch (TaskManagerException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new TaskManagerException(ErrorsCodes
                    .SAVE_MODEL_TO_FILE_EXCEPTION);
        } finally {
            LOGGER.info("Method saveModel end");
        }
    }

    /**
     * Method to load an com.netcracker.edu.gr3.model from file.
     *
     * @throws TaskManagerException if failed to load com.netcracker.edu.gr3.model from file
     */
    private void loadModel() throws TaskManagerException {
        try {
            LOGGER.info("Method loadModel called");
            model = JaxbParser.loadFromFile(FILE_PATH, DataStore.class);
        } finally {
            LOGGER.info("Method loadModel end");
        }
    }

    /**
     * Method to disable the timer and force save the com.netcracker.edu.gr3.model to file.
     *
     * @throws TaskManagerException if failed to force save the com.netcracker.edu.gr3.model to file
     */
    public void closeModel() throws TaskManagerException {
        LOGGER.info("Method closeModel called");
        if (timer != null) {
            timer.cancel();
        }
        saveModel();
        LOGGER.info("Method closeModel end");
    }

    /**
     * Private class "MyTimerTask" defines a task that can be scheduled for a
     * single or repeated execution by a timer. Inherited from TimerTask.
     *
     * @author Olga Sudakova
     * @version 1.0.0
     */
    private class MyTimerTask extends TimerTask {

        /**
         * Overriding the method Runnable.run() to define a recurring task.
         * Saves the com.netcracker.edu.gr3.model to a file depending on the assigned time.
         */
        @Override
        public void run() {
            try {
                LOGGER.info("Method autosaving com.netcracker.edu.gr3.model called");
                saveModel();
            } catch (TaskManagerException ex) {
                LOGGER.error(ex.getMessage(), ex);
            } finally {
                LOGGER.info("Method autosaving com.netcracker.edu.gr3.model end");
            }
        }
    }
}
