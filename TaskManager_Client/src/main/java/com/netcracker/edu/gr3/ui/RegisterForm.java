package com.netcracker.edu.gr3.ui;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.UserControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.ui.model.LanguageComboBox;
import com.netcracker.edu.gr3.util.ControllerProvider;
import com.toedter.calendar.JDateChooser;
import org.apache.log4j.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.SwingUtilities;
import javax.swing.BorderFactory;
import javax.swing.Box;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ResourceBundle;
import java.util.Locale;

/**
 * This class is needed to create RegisterForm.
 *
 * @author Denisova Ekaterina
 * @version 1.0.
 */
public class RegisterForm extends JFrame {
    /**
     * Property is LOGGER.
     *
     * @see Logger .
     */
    private static final Logger LOGGER = Logger
            .getLogger(LoginForm.class);
    /**
     * Property is ROWS_PANEL_INFO.
     */
    private static final int ROWS_PANEL_INFO = 8;
    /**
     * Property is COLS_PANEL_INFO.
     */
    private static final int COLS_PANEL_INFO = 2;
    /**
     * Property is TEMP_FOR_CENTER.
     */
    private static final int TEMP_FOR_CENTER = 2;
    /**
     * Property is labelError.
     *
     * @see JLabel .
     */
    private JLabel labelError;
    /**
     * Property is labelUsername.
     *
     * @see JLabel .
     */
    private JLabel labelUsername;
    /**
     * Property is labelPassword.
     *
     * @see JLabel .
     */
    private JLabel labelPassword;
    /**
     * Property is labelRepeatPassword.
     *
     * @see JLabel .
     */
    private JLabel labelRepeatPassword;
    /**
     * Property is labelFirstName.
     *
     * @see JLabel .
     */
    private JLabel labelFirstName;
    /**
     * Property is labelLastName.
     *
     * @see JLabel .
     */
    private JLabel labelLastName;
    /**
     * Property is labelDateOfBirth.
     *
     * @see JLabel .
     */
    private JLabel labelDateOfBirth;
    /**
     * Property is labelPhoneNumber.
     *
     * @see JLabel .
     */
    private JLabel labelPhoneNumber;
    /**
     * Property is labelEmail.
     *
     * @see JLabel .
     */
    private JLabel labelEmail;
    /**
     * Property is buttonRegister.
     *
     * @see JButton .
     */
    private JButton buttonRegister;
    /**
     * Property is buttonCancel.
     *
     * @see JButton .
     */
    private JButton buttonCancel;
    /**
     * Property is registerFormPanel.
     *
     * @see JPanel .
     */
    private JPanel registerFormPanel;
    /**
     * Property is dateChooser.
     *
     * @see JDateChooser .
     */
    private JDateChooser dateChooser;

    /**
     * Create new object.
     *
     * @see RegisterForm#RegisterForm().
     */
    public RegisterForm() {
        LOGGER.info("Create RegisterForm object called");
        setResizable(false);
        dateChooser = new JDateChooser();
        registerFormPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(
                registerFormPanel, BoxLayout.Y_AXIS);
        registerFormPanel.setLayout(boxLayout);
        JPanel panelLang = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JComboBox comboBoxLang = null;
        try {
            comboBoxLang = new LanguageComboBox();
        } catch (TaskManagerException e) {
            handleException(e);
        }
        JPanel panelInfo = new JPanel(new GridLayout(
                ROWS_PANEL_INFO, COLS_PANEL_INFO));
        labelUsername = new JLabel();
        JTextField textUsername = new JTextField();
        labelPassword = new JLabel();
        JPasswordField passwordField = new JPasswordField();
        labelRepeatPassword = new JLabel();
        JPasswordField repeatPasswordField = new JPasswordField();
        labelFirstName = new JLabel();
        JTextField textFirstName = new JTextField();
        labelLastName = new JLabel();
        JTextField textLastName = new JTextField();
        labelDateOfBirth = new JLabel();
        labelPhoneNumber = new JLabel();
        JTextField textPhoneNumber = new JTextField();
        labelEmail = new JLabel();
        JTextField textEmail = new JTextField();
        JPanel panelError= new JPanel();
        panelError.setLayout(new BoxLayout(panelError, BoxLayout.X_AXIS));
        labelError = new JLabel();
        labelError.setForeground(Color.RED);
        JPanel panelButton = new JPanel();
        panelButton.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonRegister = new JButton();
        buttonCancel = new JButton();
        localizeView();
        labelError.setAlignmentX(Component.LEFT_ALIGNMENT);
        panelLang.add(comboBoxLang);
        registerFormPanel.add(panelLang);
        panelInfo.add(labelUsername);
        panelInfo.add(textUsername);
        panelInfo.add(labelPassword);
        panelInfo.add(passwordField);
        panelInfo.add(labelRepeatPassword);
        panelInfo.add(repeatPasswordField);
        panelInfo.add(labelFirstName);
        panelInfo.add(textFirstName);
        panelInfo.add(labelLastName);
        panelInfo.add(textLastName);
        panelInfo.add(labelDateOfBirth);
        panelInfo.add(dateChooser);
        panelInfo.add(labelPhoneNumber);
        panelInfo.add(textPhoneNumber);
        panelInfo.add(labelEmail);
        panelInfo.add(textEmail);
        registerFormPanel.add(panelInfo);
        panelError.add(labelError);
        panelError.add(Box.createHorizontalGlue());
        registerFormPanel.add(panelError);
        panelButton.add(buttonRegister);
        panelButton.add(buttonCancel);
        registerFormPanel.add(panelButton);
        add(registerFormPanel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        buttonCancel.addActionListener(e -> SwingUtilities
                .invokeLater(() -> {
                    new LoginForm().setVisible(true);
                    dispose();
                }));
        buttonRegister.addActionListener(e -> {
            try {
                StringBuilder password = new StringBuilder();
                for(char pas: passwordField.getPassword()){
                    password.append(pas);
                }
                StringBuilder repeatPassword = new StringBuilder();
                for(char pas: repeatPasswordField.getPassword()){
                    repeatPassword.append(pas);
                }
                if (password.toString().equals(
                        repeatPassword.toString())) {
                        User user = new User();
                        user.setLogin(textUsername.getText());
                        user.setPassword(password.toString());
                        user.setFirstName(textFirstName.getText());
                        user.setLastName(textLastName.getText());
                        user.setPhoneNumber(textPhoneNumber.getText());
                        Instant instant = Instant.ofEpochMilli(
                                dateChooser.getDate().getTime());
                        LocalDate localDateBirth = LocalDateTime
                                .ofInstant(instant, ZoneId.systemDefault())
                                .toLocalDate();
                        user.setDateOfBirth(localDateBirth);
                        user.setEmail(textEmail.getText());
                        ControllerProvider.getProvider().getController(
                                ApplicationControllerInterface.class).
                                setCurrentUser(ControllerProvider
                                        .getProvider().getController(
                                                UserControllerInterface.class).
                                                createUser(user).getUserId());
                        new MainForm().setVisible(true);
                        dispose();
                } else {
                    throw new TaskManagerException(ErrorsCodes
                            .DIFFERENT_PASSWORD_ERROR);
                }
            } catch (TaskManagerException exception) {
                handleException(exception);
            } catch (Exception exception) {
                handleException(new TaskManagerException(ErrorsCodes
                        .UNKNOWN));
            }
        });
        assert comboBoxLang != null;
        comboBoxLang.addActionListener(event -> {
            localizeView();
            dateChooser.setLocale(Locale.getDefault());
        });
        LOGGER.info("Create RegisterForm object end");
    }

    /**
     * This method handles errors by substituting
     * them into a labelError.
     *
     * @param exception is Exception .
     */
    private void handleException(TaskManagerException exception) {
        LOGGER.info("Method handleException called");
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error",
                        Locale.getDefault());
        LOGGER.error(exception.getMessage(), exception);
        labelError.setText(bundleError.getString(
                String.valueOf(exception.getErrorCode())));
        LOGGER.info("Method handleException end");
    }

    /**
     * This method localize view in RegisterForm.
     */
    private void localizeView() {
        LOGGER.info("Method localizeView called");
        ResourceBundle bundle = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.ui.registerForm",
                        Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth = Integer.parseInt(bundle
                .getString("window.width"));
        int windowHeight = Integer.parseInt(bundle
                .getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit()
                .getScreenSize();
        int locationX = (screenSize.width - windowWidth) / TEMP_FOR_CENTER;
        int locationY = (screenSize.height - windowHeight) / TEMP_FOR_CENTER;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        registerFormPanel.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        labelUsername.setText(bundle.getString("labelUsername.name"));
        labelPassword.setText(bundle.getString("labelPassword.name"));
        labelRepeatPassword.setText(bundle
                .getString("labelRepeatPassword.name"));
        labelFirstName.setText(bundle.getString("labelFirstName.name"));
        labelLastName.setText(bundle.getString("labelLastName.name"));
        labelDateOfBirth.setText(bundle.getString("labelDateOfBirth.name"));
        labelPhoneNumber.setText(bundle.getString("labelPhoneNumber.name"));
        labelEmail.setText(bundle.getString("labelEmail.name"));
        buttonRegister.setText(bundle.getString("buttonRegister.name"));
        buttonRegister.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonRegister.width")),
                Integer.parseInt(bundle.getString("buttonRegister.height"))));
        buttonCancel.setText(bundle.getString("buttonCancel.name"));
        buttonCancel.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCancel.width")),
                Integer.parseInt(bundle.getString("buttonCancel.height"))));
        labelError.setText(" ");
        LOGGER.info("Method localizeView end");
    }
}
