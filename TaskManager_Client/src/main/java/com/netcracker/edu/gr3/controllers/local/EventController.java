package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.EventControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.model.Event;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class "EventController" implements the interface EventControllerInterface.
 *
 * @author Ekaterina Denisova
 * @version 1.0.0
 */
@Component
public class EventController implements EventControllerInterface {
    /**
     * Object that provides work with DataStore.
     */
    @Autowired
    private DataStore dataStore;
    /**
     * Object that provides work with ApplicationControllerInterface.
     */
    @Autowired
    private ApplicationControllerInterface appControllerInterface;
    /**
     * Property is LOGGER.
     */
    private static final Logger LOGGER = Logger
            .getLogger(EventController.class);

    /**
     * @inheritDoc
     */
    @Override
    public Event createEvent(Event event) throws TaskManagerException {
        LOGGER.info("Method createEvent called");
        if(event.getBeginDateTime().isAfter(event.getEndDateTime())){
            throw new TaskManagerException(ErrorsCodes.INCORRECT_EVENT_TIME_SPAN);
        }
        event.setEventId(dataStore.getMap()
                .get(Event.class).generateID());
        LOGGER.debug("Event's ID was generated");
        dataStore.getEvents().add(event);
        LOGGER.info("Method createEvent end");
        appControllerInterface.notifyObservers(new ObserverEvent());
        return event;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Event getEventById(long eventId) throws TaskManagerException {
        try {
            LOGGER.info("Method getEventById called");
            return dataStore.getEvents().stream()
                    .filter(event -> event.getEventId() == eventId)
                    .findFirst().orElseThrow(() ->
                            new TaskManagerException(ErrorsCodes.
                                    EVENT_NOT_FOUND_EXCEPTION));
        } finally {
            LOGGER.info("Method getEventById end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Event> getEventsByCalendarIdForDay(long calendarId, LocalDate date) throws TaskManagerException {
        try {
            LOGGER.info("Method getEventsByCalendarIdForDay called");
            return dataStore.getEvents().stream().filter(
                    event -> event.getCalendarId() == calendarId &&
                            (!(date.isBefore(event.getBeginDateTime().toLocalDate())
                                    || date.isAfter(event.getEndDateTime().toLocalDate())))).
                    collect(Collectors.toCollection(ArrayList::new));
        } finally {
            LOGGER.info("Method getEventsByCalendarIdForDay end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Event updateEvent(Event event) throws TaskManagerException {
        LOGGER.info("Method updateEvent called");
        if(event.getBeginDateTime().isAfter(event.getEndDateTime())){
            throw new TaskManagerException(ErrorsCodes.INCORRECT_EVENT_TIME_SPAN);
        }
        Event updatedEvent = getEventById(event.getEventId());
        LOGGER.debug("Change title event");
        updatedEvent.setTitleEvent(event.getTitleEvent());
        LOGGER.debug("Change description");
        updatedEvent.setDescription(event.getDescription());
        LOGGER.debug("Change calendar's Id");
        updatedEvent.setCalendarId(event.getCalendarId());
        LOGGER.debug("Change begin date and time");
        updatedEvent.setBeginDateTime(event.getBeginDateTime());
        LOGGER.debug("Change end date and time");
        updatedEvent.setEndDateTime(event.getEndDateTime());
        LOGGER.debug("Change interval repetition");
        updatedEvent.setIntervalRepetition(event.getIntervalRepetition());
        LOGGER.debug("Change end date repetition");
        updatedEvent.setEndDateRepetition(event.getEndDateRepetition());
        LOGGER.info("Method updateCalendar end");
        appControllerInterface.notifyObservers(new ObserverEvent());
        return updatedEvent;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteEvent(long EventId) throws TaskManagerException {
        LOGGER.info("Method deleteEvent called");
        dataStore.getEvents()
                .removeIf(event->event.getEventId()==EventId);
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("Method deleteEvent end");

    }
}
