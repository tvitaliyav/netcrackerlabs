package com.netcracker.edu.gr3.ui.render;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;
import java.util.Locale;

/**
 * This class is needed to render LanguageComboBox.
 *
 * @author Denisova Ekaterina
 * @version 1.0.0
 */
public class RenderingComboBox extends BasicComboBoxRenderer {
    /**
     * Property is WIDTH.
     */
    private static final int WIDTH = 136;
    /**
     * Property is HEIGHT.
     */
    private static final int HEIGHT = 20;
    /**
     * This method gets the value of the RenderingComboBox.
     *
     * @param list is JList.
     * @param value is Object ComboBox.
     * @param index is int.
     * @param isSelected is boolean.
     * @param cellHasFocus is boolean.
     * @return the value outputs as a Component.
     * @see Component .
     */
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected,
                                                  boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected,
                cellHasFocus);
        setText(((Locale) value).getDisplayName());
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        return this;
    }
}
