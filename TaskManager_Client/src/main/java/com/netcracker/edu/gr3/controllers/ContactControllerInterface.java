package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Contact;

import java.util.Collection;

/**
 * Interface "ContactControllerInterface" defines actions with contact.
 *
 * @author Vitaliya Tikhonova, Olga Sudakova
 * @version 1.0.0
 */
public interface ContactControllerInterface {
    /**
     * Method for creating a new user.
     *
     * @param contact - contact for creating
     * @return contact
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Contact createContact(Contact contact) throws TaskManagerException;

    /**
     * Method to get contacts by user ID and contact group.
     *
     * @param contactGroupId - id contact group to which the contact belongs.
     * @param userId - unique user id
     * @return Collection of contacts
     * @throws TaskManagerException Inherited from class "Exception"
     */

    Collection<Contact> getFriendsByUserIdAndContactGroup(long userId,
                                                          long contactGroupId)
            throws TaskManagerException;

    /**
     * Method to get contacts who consist in black list.
     *
     * @param userId - unique user id
     * @return Collection of contacts
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Contact> getBlackListContacts(long userId)
            throws TaskManagerException;

    /**
     * Method to get contacts to which the user has sent a request.
     *
     * @param fromUserId - unique user id
     * @return Collection of contacts
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Contact> getOutcomingFriendRequest(long fromUserId)
            throws TaskManagerException;

    /**
     * Method to get contacts who sent the request to the user's friends.
     *
     * @param toUserId - unique user id
     * @return Collection of contacts
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Contact> getIncomingFriendRequest(long toUserId)
            throws TaskManagerException;

    /**
     * Method to get contacts by user ID.
     *
     * @param userId - unique user id
     * @return Collection of contacts
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Contact> getContactsByUserId(long userId)
            throws TaskManagerException;
    /**
     * Method to get contacts by two users.
     *
     * @param fromUserId - unique user id
     * @param toUserId - unique user id
     * @return Contact
     * @throws TaskManagerException Inherited from class "Exception"
     */

    Contact getContactByUsers(long fromUserId, long toUserId)
            throws TaskManagerException;;

    /**
     * Method for update contact.
     *
     * @param contact - new contact
     * @return contact
     * @throws TaskManagerException Inherited from class "Exception"
     */

    Contact updateContact(Contact contact) throws TaskManagerException;

}
