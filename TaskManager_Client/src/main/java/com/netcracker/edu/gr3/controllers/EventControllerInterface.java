package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Event;

import java.time.LocalDate;
import java.util.Collection;

/**
 * Interface "EventControllerInterface" defines actions
 * with events.
 *
 * @author Ekaterina Denisova
 * @version 1.0.0
 */
public interface EventControllerInterface {
    /**
     * Method for creating a new event.
     *
     * @param event - event for creating
     * @return event
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Event createEvent(Event event) throws TaskManagerException;

    /**
     * Method to get event by ID.
     *
     * @param eventId - unique event's ID
     * @return event
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Event getEventById(long eventId) throws TaskManagerException;

    /**
     * Method to get event list by calendar's ID on a specific day.
     *
     * @param calendarId - unique calendar's ID
     * @param date       - a specific day
     * @return Collection of event
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Event> getEventsByCalendarIdForDay(long calendarId, LocalDate date) throws TaskManagerException;

    /**
     * Method for update event.
     *
     * @param event - event for updating
     * @return event
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Event updateEvent(Event event) throws TaskManagerException;

    /**
     * Method for delete event by ID.
     *
     * @param EventId - unique event`s ID
     * @throws TaskManagerException Inherited from class "Exception"
     */
    void deleteEvent(long EventId) throws TaskManagerException;
}
