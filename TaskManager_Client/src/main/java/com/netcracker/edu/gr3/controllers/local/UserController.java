package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.UserControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.model.User;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collection;

import com.netcracker.edu.gr3.observer.ObserverEvent;
import com.netcracker.edu.gr3.ui.model.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class "UserController" implements the interface UserControllerInterface.
 *
 * @author Vitaliya Tikhonova, Ekaterina Denisova
 * @version 1.0.0
 */
@Component
public class UserController implements UserControllerInterface {
    /**
     * Property is dataStore.
     *
     * @see DataStore .
     */
    @Autowired
    private DataStore dataStore;
    /**
     * Property is dataStore.
     *
     * @see ApplicationControllerInterface .
     */
    @Autowired
    private ApplicationControllerInterface appControllerInterface;
    /**
     * Property is LOGGER.
     *
     * @see Logger .
     */
    private static final Logger LOGGER = Logger
            .getLogger(UserController.class);
    /**
     * Property is MAX_AGE_USER.
     */
    private static final int MAX_AGE_USER = 110;
    /**
     * Property is MIN_AGE_USER.
     */
    private static final int MIN_AGE_USER = 5;

    /**
     * @inheritDoc
     */
    @Override
    public User createUser(User user) throws TaskManagerException {
        LOGGER.info("Method createUser called");
        if (getAllUsers().stream().anyMatch(currentUser ->
                currentUser.getLogin().
                        equals(user.getLogin()))) {
            throw new TaskManagerException(ErrorsCodes
                    .NOT_UNIQUE_LOGIN_EXCEPTION);
        }
        LOGGER.debug("Login uniqueness verified");
        if (Period.between(user.getDateOfBirth(),
                LocalDate.now()).getYears() < MIN_AGE_USER
                || Period.between(user.getDateOfBirth(),
                LocalDate.now()).getYears() > MAX_AGE_USER) {
            throw new TaskManagerException(ErrorsCodes
                    .INCORRECT_DATA_BIRTH_ERROR);
        }
        LOGGER.debug("Age entered correctly");
        if (getAllUsers().stream().anyMatch(currentUser ->
                currentUser.getEmail().
                        equals(user.getEmail()))) {
            throw new TaskManagerException(ErrorsCodes
                    .NOT_UNIQUE_EMAIL_EXCEPTION);
        }
        LOGGER.debug("Email uniqueness verified");
        EmailValidator emailValidator = new EmailValidator();
        if (!emailValidator.validate(user.getEmail()))
            throw new TaskManagerException(ErrorsCodes
                    .NOT_VALIDATED_EMAIL);
        LOGGER.debug("Email is validated");
        user.setUserId(dataStore.getMap().get(User.class).generateID());
        LOGGER.debug("ID was generated");
        Calendar defaultCalendar = new Calendar();
        defaultCalendar.setCalendarId(dataStore.getMap()
                .get(Calendar.class).generateID());
        defaultCalendar.setUserId(user.getUserId());
        defaultCalendar.setName(new StringBuilder(
                user.getFirstName()).append(" ").append(
                        user.getLastName()).toString());
        dataStore.getCalendars().add(defaultCalendar);
        user.setCalendarId(defaultCalendar.getCalendarId());
        getAllUsers().add(user);
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("User was added, method createUser end");

        return user;
    }

    /**
     * @inheritDoc
     */
    @Override
    public User auth(String login, String password)
            throws TaskManagerException {
        LOGGER.info("Method auth called");
        return getAllUsers().stream()
                .filter(user -> user.getLogin().equals(login)
                        && user.getPassword().equals(password))
                .findFirst().orElseThrow(() ->
                        new TaskManagerException(ErrorsCodes.
                                USER_WITH_LOGIN_AND_PASSWORD_NOT_FOUND_EXCEPTION));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<User> getAllUsers() {
        LOGGER.info("Method getAllUsers called");
        return dataStore.getUsers();
    }

    /**
     * @inheritDoc
     */
    @Override
    public User getUserById(long id) throws TaskManagerException {
        LOGGER.info("Method getUserById called");
        return getAllUsers().stream().filter(user ->
                user.getUserId() == id).findFirst().orElseThrow(() ->
                new TaskManagerException(ErrorsCodes.
                        USER_NOT_FOUND_EXCEPTION));
    }

    @Override
    public User getUserByUsername(String username) throws TaskManagerException {
        LOGGER.info("Method getUserByUsername called");
        return getAllUsers().stream().filter(user ->
                user.getLogin().equals(username)).findFirst().orElseThrow(() ->
                new TaskManagerException(ErrorsCodes.
                        USER_NOT_FOUND_EXCEPTION));
    }

    /**
     * @inheritDoc
     */
    @Override
    public User updateUser(User user) throws TaskManagerException {
        LOGGER.info("Method getUserById called");
        User updateUser = getUserById(user.getUserId());
        if (!updateUser.getLogin().equals(user.getLogin())) {
            throw new TaskManagerException(ErrorsCodes.
                    NEW_LOGIN_EXCEPTION);
        }
        LOGGER.debug("Login change check");
        if (!updateUser.getEmail().equals(user.getEmail())) {
            throw new TaskManagerException(ErrorsCodes.
                    NEW_EMAIL_EXCEPTION);
        }
        EmailValidator emailValidator = new EmailValidator();
        if (!emailValidator.validate(user.getEmail()))
            throw new TaskManagerException(ErrorsCodes
                    .NOT_VALIDATED_EMAIL);
        LOGGER.debug("Email change check");
        updateUser.setFirstName(user.getFirstName());
        LOGGER.debug("Change first name");
        updateUser.setLastName(user.getLastName());
        LOGGER.debug("Change last name");
        if (Period.between(user.getDateOfBirth(),
                LocalDate.now()).getYears() < MIN_AGE_USER
                || Period.between(user.getDateOfBirth(),
                LocalDate.now()).getYears() > MAX_AGE_USER) {
            throw new TaskManagerException(ErrorsCodes
                    .INCORRECT_DATA_BIRTH_ERROR);
        }
        LOGGER.debug("Change age");
        updateUser.setPassword(user.getPassword());
        LOGGER.debug("Change password");
        updateUser.setPhoneNumber(user.getPhoneNumber());
        LOGGER.info("Change phone number, method getUserById end");
        appControllerInterface.notifyObservers(new ObserverEvent());
        return user;
    }
}

