package com.netcracker.edu.gr3.ui;

import com.mindfusion.common.DateTime;
import com.mindfusion.common.Duration;
import com.mindfusion.scheduling.*;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.model.Appointment;
import com.mindfusion.scheduling.model.ItemList;
import com.netcracker.edu.gr3.controllers.*;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.*;
import com.netcracker.edu.gr3.observer.Observer;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import com.netcracker.edu.gr3.ui.dialog.*;
import com.netcracker.edu.gr3.ui.model.*;
import com.netcracker.edu.gr3.ui.render.TreeCellRendererCalendar;
import com.netcracker.edu.gr3.ui.render.TreeCellRendererContact;
import com.netcracker.edu.gr3.ui.render.TreeCellRendererTask;
import com.netcracker.edu.gr3.util.ControllerProvider;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.event.*;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;


public class MainForm extends JFrame implements Observer {
    private static final int ROWS_PANEL_MAIN = 1;
    private static final int COLS_PANEL_MAIN = 2;
    private static final int ROWS_PANEL_CALENDAR_TYPE = 2;
    private static final int COLS_PANEL_CALENDAR_TYPE = 1;
    private static final int HGAP_PANEL_MAIN = 10;
    private static final int ONE_DAY = 1;
    private static final int WEEK_DAYS = 7;
    private static final int WORK_WEEK_DAYS = 5;
    private JTabbedPane tabbedPane;
    private JPanel panelMain;
    private JPanel panelCalendar;
    private JPanel panelTask;
    private JMenu user;
    private JMenuItem profile;
    private JMenuItem logout;
    private JMenuItem exit;
    private JMenu contacts;
    private JMenuItem findUser;
    private JMenuItem addContactGroup;
    private JMenu events;
    private JMenuItem addCalendar;
    private JMenuItem addEvent;
    private JMenu tasks;
    private JMenuItem addTask;
    private JTree treeContactGroup;
    private JTree treeContactType;
    private JTree treeCalendarType;
    private Calendar calendar;
    private Calendar scheduler;
    private JButton buttonDay;
    private JButton buttonWeek;
    private JButton buttonWorkWeek;
    private JButton buttonMonth;
    private JButton buttonAddEvent;
    private JButton buttonToday;
    private JTree treeTasks;
    private JPanel panelInfoTask;
    private static ArrayList<CheckNode> calendars;

    public MainForm() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        calendars = new ArrayList<>();
        try {
            for (com.netcracker.edu.gr3.model.Calendar calendar : ControllerProvider.getProvider().
                    getController(CalendarControllerInterface
                            .class).getCalendarsByUserId(ControllerProvider.getProvider().
                    getController(ApplicationControllerInterface
                            .class).getCurrentUser())) {
                calendars.add(new CheckNode(calendar, false));
            }
            createMenu();
            createPanelMain();
            createPanelCalendar();
            createPanelTask();
            tabbedPane = new JTabbedPane();
            tabbedPane.addTab("", panelMain);
            tabbedPane.addTab("", panelCalendar);
            tabbedPane.addTab("", panelTask);
            setContentPane(tabbedPane);
            localizeView();
            ControllerProvider.getProvider().
                    getController(ApplicationControllerInterface
                            .class).addObserver(this);
        } catch (TaskManagerException exception) {
            handleException(exception);
        }
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeFrame(true);
            }
        });
    }

    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        user = new JMenu();
        menuBar.add(user);
        profile = new JMenuItem();
        user.add(profile);
        logout = new JMenuItem();
        user.add(logout);
        exit = new JMenuItem();
        user.add(exit);
        contacts = new JMenu();
        menuBar.add(contacts);
        findUser = new JMenuItem();
        contacts.add(findUser);
        addContactGroup = new JMenuItem();
        contacts.add(addContactGroup);
        events = new JMenu();
        menuBar.add(events);
        addCalendar = new JMenuItem();
        events.add(addCalendar);
        addEvent = new JMenuItem();
        events.add(addEvent);
        tasks = new JMenu();
        menuBar.add(tasks);
        addTask = new JMenuItem();
        tasks.add(addTask);
        logout.addActionListener(e -> SwingUtilities.invokeLater(() -> closeFrame(false)));
        findUser.addActionListener(e -> new FindUserDialog().setVisible(true));
        profile.addActionListener(e -> {
            new DialogUserDetails(DialogUserDetails.MY_PROFILE);
            localizeView();
            calendar.setLocale(Locale.getDefault());
            scheduler.setLocale(Locale.getDefault());
            calendar.update();
            scheduler.update();
        });
        exit.addActionListener(e -> closeFrame(true));
        addContactGroup.addActionListener(e -> new ContactGroupDialog(ContactGroupDialog.CREATE_CONTACT_GROUP));
        addCalendar.addActionListener(e -> new CalendarDialog(CalendarDialog.CREATE_CALENDAR));
        addTask.addActionListener(e -> new TaskDialog(TaskDialog.CREATE_TASK));
        addEvent.addActionListener(e -> new EventDialog(EventDialog.CREATE_EVENT));
    }

    private void createPanelMain() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.mainForm", Locale.getDefault());
        panelMain = new JPanel();
        GridLayout layoutPanelMain = new GridLayout(ROWS_PANEL_MAIN, COLS_PANEL_MAIN);
        layoutPanelMain.setHgap(HGAP_PANEL_MAIN);
        panelMain.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        panelMain.setLayout(layoutPanelMain);
        treeContactGroup = new JTree(new TreeModelContact(TreeModelContact.ROOT_CONTACT_GROUP_AND_CONTACT));
        treeContactGroup.setRootVisible(false);
        treeContactGroup.setCellRenderer(new TreeCellRendererContact());
        treeContactType = new JTree(new TreeModelContact(TreeModelContact.ROOT_IN_OUT_BLACK_LIST));
        treeContactType.setRootVisible(false);
        treeContactType.setCellRenderer(new TreeCellRendererContact());
        treeContactGroup.addMouseListener( new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    TreePath path = treeContactGroup.getPathForLocation(e.getX(), e.getY());
                    treeContactGroup.setSelectionPath(path);
                    try {
                        Object lastSelectedPathComponent = treeContactGroup.getLastSelectedPathComponent();
                        if (lastSelectedPathComponent != null) {
                            JPopupMenu popupMenu = new JPopupMenu();
                            if (lastSelectedPathComponent instanceof User) {
                                if (ControllerProvider.getProvider().
                                        getController(ContactControllerInterface.class)
                                        .getContactByUsers(ControllerProvider.getProvider()
                                                .getController(ApplicationControllerInterface.class)
                                                .getCurrentUser(), ((User) lastSelectedPathComponent).getUserId()
                                        ).getContactType() == ContactType.FRIEND) {
                                    if (ControllerProvider.getProvider().
                                            getController(ContactControllerInterface.class)
                                            .getContactByUsers(ControllerProvider.getProvider()
                                                    .getController(ApplicationControllerInterface.class)
                                                    .getCurrentUser(), ((User) lastSelectedPathComponent).getUserId()
                                            ).getContactGroupId() == 0) {
                                        popupMenu = PopupMenuHelper.popupMenuContactFriend(((User)
                                                lastSelectedPathComponent).getUserId());
                                    } else popupMenu = PopupMenuHelper.popupMenuContactFriendInGroup(((User)
                                            lastSelectedPathComponent).getUserId());
                                }
                            } else if (lastSelectedPathComponent instanceof ContactGroup) {
                                popupMenu = PopupMenuHelper.popupMenuContactGroup(((ContactGroup)
                                        lastSelectedPathComponent).getContactGroupId());
                            }
                            popupMenu.show(treeContactGroup, e.getX(), e.getY());
                        }
                    } catch (TaskManagerException e1) {
                        handleException(e1);
                    }
                }
            }
        });
        treeContactType.addMouseListener( new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    TreePath path = treeContactType.getPathForLocation(e.getX(), e.getY());
                    treeContactType.setSelectionPath(path);
                    try {
                        Object lastSelectedPathComponent = treeContactType.getLastSelectedPathComponent();
                        if (lastSelectedPathComponent != null) {
                            JPopupMenu popupMenu = new JPopupMenu();
                            if (lastSelectedPathComponent instanceof User) {
                                    if (ControllerProvider.getProvider().
                                            getController(ContactControllerInterface.class)
                                            .getContactByUsers(((User) lastSelectedPathComponent).getUserId(),
                                                    ControllerProvider.getProvider()
                                                            .getController(ApplicationControllerInterface.class)
                                                            .getCurrentUser()).getContactType() == ContactType.FRIEND_REQUEST) {
                                        popupMenu = PopupMenuHelper.popupMenuIncoming(((User)
                                                lastSelectedPathComponent).getUserId());
                                    } else if (ControllerProvider.getProvider().
                                            getController(ContactControllerInterface.class)
                                            .getContactByUsers(ControllerProvider.getProvider()
                                                    .getController(ApplicationControllerInterface.class)
                                                    .getCurrentUser(), ((User) lastSelectedPathComponent).getUserId()
                                            ).getContactType() == ContactType.FRIEND_REQUEST) {
                                        popupMenu = PopupMenuHelper.popupMenuOutcoming(((User)
                                                lastSelectedPathComponent).getUserId());
                                    } else if (ControllerProvider.getProvider().
                                            getController(ContactControllerInterface.class)
                                            .getContactByUsers(ControllerProvider.getProvider()
                                                    .getController(ApplicationControllerInterface.class)
                                                    .getCurrentUser(), ((User) lastSelectedPathComponent).getUserId()
                                            ).getContactType() == ContactType.BLACK_LIST) {
                                        popupMenu = PopupMenuHelper.popupMenuBlackList(((User)
                                                lastSelectedPathComponent).getUserId());
                                    }
                                }
                            popupMenu.show(treeContactType, e.getX(), e.getY());
                            }
                        }
                    catch (TaskManagerException e1) {
                        handleException(e1);
                    }
                }
            }});
        panelMain.add(new JScrollPane(treeContactGroup));
        panelMain.add(new JScrollPane(treeContactType));

    }


    private void createPanelCalendar() {
        panelCalendar = new JPanel(new MigLayout());
        calendar = new Calendar();
        calendar.setCurrentView(CalendarView.SingleMonth);
        calendar.setDate(DateTime.today());
        calendar.getMonthSettings().getDaySettings().setShowToday(true);
        calendar.getMonthSettings().getDaySettings().setTodayColor(Color.BLUE);
        calendar.getMonthSettings().getDaySettings().setTodayFillColor(Color.BLUE);
        calendar.addCalendarListener(new CalendarAdapter() {
            @Override
            public void dateClick(ResourceDateEvent dateEvent) {
                super.monthCellClick(dateEvent);
                calendar.setDate(dateEvent.getDate());
                if (scheduler.getCurrentView().equals(CalendarView.Timetable)) {
                    createScheduler(scheduler.getTimetableSettings().getDates().size());
                } else {
                    scheduler.setDate(calendar.getDate());
                    scheduler.update();
                }
            }
        });
        //create scheduler
        JPanel panelScheduler = new JPanel(new MigLayout());
        buttonAddEvent = new JButton();
        buttonToday = new JButton();
        buttonDay = new JButton();
        buttonWeek = new JButton();
        buttonWorkWeek = new JButton();
        buttonMonth = new JButton();
        scheduler = new Calendar();
        updateItemScheduler();
        localizeHoursScheduler();
        scheduler.setCurrentView(CalendarView.Timetable);
        scheduler.getTimetableSettings().setCellTime(Duration.fromMinutes(10));
        scheduler.getTimetableSettings().setShowDayHeader(true);
        scheduler.setEnabled(false);
        createScheduler(ONE_DAY);
        JPanel groupButtonsEvent = new JPanel(new MigLayout());
        JPanel panelCalendarType = new JPanel(new MigLayout());
        treeCalendarType = new JTree(new TreeModelCalendar(TreeModelCalendar.ROOT_CALENDAR));
        treeCalendarType.setRootVisible(false);
        treeCalendarType.setCellRenderer(new TreeCellRendererCalendar());
        treeCalendarType.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                    if (treeCalendarType
                            .getLastSelectedPathComponent() != null)
                        if (((CheckNode) treeCalendarType
                                .getLastSelectedPathComponent()).isSelected()) {
                            ((CheckNode) treeCalendarType
                                    .getLastSelectedPathComponent()).setSelected(false);
                            calendars.get(calendars.indexOf(treeCalendarType
                                    .getLastSelectedPathComponent())).setSelected(false);
                        } else {
                            ((CheckNode) treeCalendarType
                                    .getLastSelectedPathComponent()).setSelected(true);
                            calendars.get(calendars.indexOf(treeCalendarType
                                    .getLastSelectedPathComponent())).setSelected(true);
                        }
                    panelCalendarType.updateUI();
                    updateItemScheduler();
                }

            @Override
            public void mouseReleased(MouseEvent e) {
                    if (e.isPopupTrigger()) {
                        Object lastSelectedPathComponent = treeCalendarType.getLastSelectedPathComponent();
                        TreePath path = treeCalendarType.getPathForLocation(e.getX(), e.getY());
                        treeCalendarType.setSelectionPath(path);
                        if (lastSelectedPathComponent != null) {
                            JPopupMenu popupMenu = new JPopupMenu();
                            if (lastSelectedPathComponent instanceof CheckNode) {
                                popupMenu = PopupMenuHelper.popupMenuCalendar(((com.netcracker.edu.gr3.model.Calendar)
                                        ((CheckNode) lastSelectedPathComponent).getObject()).getCalendarId());
                            }
                            popupMenu.show(treeCalendarType, e.getX(), e.getY());
                        }
                    }
            }
        });
        JPanel panelCalendarAndTypeCalendar = new JPanel(
                new GridLayout(ROWS_PANEL_CALENDAR_TYPE, COLS_PANEL_CALENDAR_TYPE));
        panelCalendarType.add(new JScrollPane(treeCalendarType), "pushx,growx");
        groupButtonsEvent.add(buttonAddEvent, "pushx, growx, pushy, growy");
        groupButtonsEvent.add(buttonToday, "pushx, growx, pushy, growy");
        groupButtonsEvent.add(buttonDay, "pushx, growx, pushy, growy");
        groupButtonsEvent.add(buttonWeek, "pushx, growx, pushy, growy");
        groupButtonsEvent.add(buttonWorkWeek, "pushx, growx, pushy, growy");
        groupButtonsEvent.add(buttonMonth, "pushx, growx, pushy, growy");
        panelScheduler.add(groupButtonsEvent, "pushx, growx, wrap");
        panelScheduler.add(scheduler, "pushx, growx, pushy, growy");
        panelCalendarAndTypeCalendar.add(calendar);
        panelCalendarAndTypeCalendar.add(panelCalendarType);
        panelCalendar.add(panelCalendarAndTypeCalendar, "growx 30,growy 100,pushy 100,pushx 25");
        panelCalendar.add(panelScheduler, "growx 70, growy 100, pushy 100,pushx 75");
        scheduler.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(scheduler.getItemAt(e.getPoint())!=null){
                    new EventDialog(EventDialog.CHANGE_EVENT,
                            Long.parseLong(scheduler.getItemAt(e.getPoint()).getId()))
                            .setVisible(true);
                }
            }
        });
        buttonAddEvent.addActionListener(e -> new EventDialog(EventDialog.CREATE_EVENT));
        buttonDay.addActionListener(e -> createScheduler(ONE_DAY));
        buttonWeek.addActionListener(e -> createScheduler(WEEK_DAYS));
        buttonWorkWeek.addActionListener(e -> createScheduler(WORK_WEEK_DAYS));
        buttonMonth.addActionListener(e -> {
            scheduler.setCurrentView(CalendarView.SingleMonth);
            scheduler.getMonthSettings().setShowPaddingDays(true);
            scheduler.beginInit();
            scheduler.getTimetableSettings().getDates().clear();
            for (int i = 0; i < YearMonth.of(calendar.getDate()
                    .getYear(),calendar.getDate().getMonth()).lengthOfMonth(); i++) {
                scheduler.getTimetableSettings().getDates().add(calendar.getDate().addDays(
                        calendar.getDate().getDay() * (-1) + 1).addDays(i));
            }
            scheduler.endInit();
            updateItemScheduler();
        });
        buttonToday.addActionListener(e -> {
            calendar.setDate(DateTime.today().getDate());
            createScheduler(ONE_DAY);
        });
    }

    private void updateItemScheduler() {
        try {
            ItemList items = scheduler.getSchedule().getItems();
            items.clear();
            DateList dateList = scheduler.getTimetableSettings().getDates();
            scheduler.beginInit();
            ArrayList<com.netcracker.edu.gr3.model.Calendar> currentCalendars = new ArrayList<>();
            for(CheckNode node: calendars){
                if(node.isSelected())
                currentCalendars.add((com.netcracker.edu.gr3.model.Calendar)node.getObject());
            }
            for (DateTime dateTime : dateList) {
                for (com.netcracker.edu.gr3.model.Calendar calendar : currentCalendars) {
                    ArrayList<Event> events = (ArrayList<Event>) ControllerProvider.getProvider()
                            .getController(EventControllerInterface.class).getEventsByCalendarIdForDay(
                                    calendar.getCalendarId(), dateTime.getDate().toJavaCalendar().getTime()
                                            .toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                    for (Event event : events) {
                        Appointment appointment;
                        if(items.get(String.valueOf(event.getEventId()))==null) {
                            appointment = new Appointment();
                            appointment.setId(String.valueOf(event.getEventId()));
                            appointment.setHeaderText(event.getTitleEvent());
                            if (event.getDescription() != null)
                                appointment.setDescriptionText(event.getDescription());
                            else appointment.setDescriptionText("");
                            appointment.setStartTime(new DateTime(Date.from(
                                    event.getBeginDateTime().atZone(ZoneId.systemDefault()).toInstant())));
                            appointment.setEndTime(new DateTime(Date.from(
                                    event.getEndDateTime().atZone(ZoneId.systemDefault()).toInstant())));
                            items.add(appointment);
                        }
                    }
                }
            }
            scheduler.revalidate();
            scheduler.endInit();
            scheduler.update();
        }
        catch (TaskManagerException e){
            handleException(e);
        }
    }
    private void localizeHoursScheduler(){
        if(Locale.getDefault().getLanguage().equals("ru")) {
            scheduler.getTimetableSettings().setTwelveHourFormat(false);
            scheduler.getTimetableSettings().setShowAM(false);
        }
        else {
            scheduler.getTimetableSettings().setTwelveHourFormat(true);
            scheduler.getTimetableSettings().setShowAM(true);
        }
    }
    private void createScheduler(int countDay) {
        scheduler.setCurrentView(CalendarView.Timetable);
        scheduler.beginInit();
        if (countDay == ONE_DAY) {
            scheduler.getTimetableSettings().getDates().clear();
            scheduler.getTimetableSettings().getDates().add(calendar.getDate());
        } else {
            scheduler.getTimetableSettings().getDates().clear();
            for (int i = 0; i < countDay; i++) {
                scheduler.getTimetableSettings().getDates().add(calendar.getDate().addDays(
                        calendar.getDate().getDayOfWeek().getValue() * (-1) + 1).addDays(i));
            }
        }
        scheduler.getTimetableSettings().setVisibleColumns(countDay);
        scheduler.endInit();
        scheduler.update();
        updateItemScheduler();
    }


    private void createPanelTask() {
        panelTask = new JPanel(new GridLayout(1,2));
        panelInfoTask = new JPanel();
        treeTasks = new JTree(new TreeModelTask(
                TreeModelTask.ROOT_TASK));
        treeTasks.setRootVisible(false);
        treeTasks.setCellRenderer(new TreeCellRendererTask());
        treeTasks.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                Object lastSelectedPathComponent = treeTasks.getLastSelectedPathComponent();
                if (e.isPopupTrigger()) {
                    TreePath path = treeTasks.getPathForLocation(e.getX(), e.getY());
                    if (path != null) treeTasks.setSelectionPath(path);
                    if (lastSelectedPathComponent != null) {
                        JPopupMenu popupMenu = new JPopupMenu();
                        if (lastSelectedPathComponent instanceof Task) {
                            popupMenu = PopupMenuHelper.popupMenuTask(((Task)
                                    lastSelectedPathComponent).getTaskId());
                        }
                        popupMenu.show(treeTasks, e.getX(), e.getY());
                    }
                }
                else{
                    panelInfoTask.removeAll();
                    if (lastSelectedPathComponent != null) {
                        panelInfoTask.add(new TaskPanel(TaskDialog.CHANGE_TASK, ((Task)
                                lastSelectedPathComponent).getTaskId()));
                        panelTask.updateUI();
                    }
                }
            }
        });
        panelTask.add(new JScrollPane(treeTasks));
        panelTask.add(new JScrollPane(panelInfoTask));
    }

    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.mainForm", Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        tabbedPane.setTitleAt(0, bundle.getString("tabbedPane.main"));
        tabbedPane.setTitleAt(1, bundle.getString("tabbedPane.calendar"));
        tabbedPane.setTitleAt(2, bundle.getString("tabbedPane.task"));
        user.setText(bundle.getString("menu.user"));
        profile.setText(bundle.getString("menu.user.profile"));
        logout.setText(bundle.getString("menu.user.logout"));
        exit.setText(bundle.getString("menu.user.exit"));
        contacts.setText(bundle.getString("menu.contacts"));
        findUser.setText(bundle.getString("menu.contacts.findUser"));
        addContactGroup.setText(bundle.getString("menu.contacts.addContactGroup"));
        events.setText(bundle.getString("menu.events"));
        addCalendar.setText(bundle.getString("menu.addCalendar"));
        addEvent.setText(bundle.getString("menu.addEvent"));
        tasks.setText(bundle.getString("menu.tasks"));
        addTask.setText(bundle.getString("menu.addTask"));
        buttonDay.setText(bundle.getString("buttonDay.name"));
        buttonDay.setIcon(new ImageIcon(ClassLoader.getSystemResource(
                "com/netcracker/edu/gr3/images/day.png")));
        buttonWeek.setText(bundle.getString("buttonWeek.name"));
        buttonWeek.setIcon(new ImageIcon(ClassLoader.getSystemResource(
                "com/netcracker/edu/gr3/images/week.png")));
        buttonWorkWeek.setText(bundle.getString("buttonWorkWeek.name"));
        buttonWorkWeek.setIcon(new ImageIcon(ClassLoader.getSystemResource(
                "com/netcracker/edu/gr3/images/workweek.png")));
        buttonMonth.setText(bundle.getString("buttonMonth.name"));
        buttonMonth.setIcon(new ImageIcon(ClassLoader.getSystemResource(
                "com/netcracker/edu/gr3/images/month.jpg")));
        buttonAddEvent.setText(bundle.getString("buttonAddEvent.name"));
        buttonAddEvent.setIcon(new ImageIcon(ClassLoader.getSystemResource(
                "com/netcracker/edu/gr3/images/addEvent.jpg")));
        buttonToday.setText(bundle.getString("buttonToday.name"));
        buttonToday.setIcon(new ImageIcon(ClassLoader.getSystemResource(
                "com/netcracker/edu/gr3/images/today.png")));
    }
    private void handleException(TaskManagerException exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        JOptionPane.showMessageDialog(this,
                bundleError.getString(
                        String.valueOf(exception.getErrorCode())));
    }
    private void closeFrame(boolean exited){
        try {
            ControllerProvider.getProvider().
                    getController(ApplicationControllerInterface.class)
                    .removeObserver(this);
            ControllerProvider.getProvider().
                    getController(ApplicationControllerInterface
                            .class).closeApp();
            if(exited){
                dispose();
                System.exit(0);
            }
            else{
                new LoginForm().setVisible(true);
                dispose();
            }
        } catch (TaskManagerException exception) {
            handleException(exception);
        }
    }

    @Override
    public void notify(ObserverEvent event) {
        treeTasks.updateUI();
        treeContactGroup.updateUI();
        treeContactType.updateUI();
        try {
            if(calendars.size()<ControllerProvider.getProvider().
                    getController(CalendarControllerInterface.class)
                    .getCalendarsByUserId(ControllerProvider.getProvider().
                            getController(ApplicationControllerInterface
                                    .class).getCurrentUser()).size()) {
                calendars.add(new CheckNode(((ArrayList<com.netcracker.edu.gr3.model.Calendar>) ControllerProvider.getProvider().
                        getController(CalendarControllerInterface.class)
                        .getCalendarsByUserId(ControllerProvider.getProvider().
                                getController(ApplicationControllerInterface
                                        .class).getCurrentUser())).get(calendars.size()), false));
            }
        } catch (TaskManagerException e) {
            handleException(e);
        }
        treeCalendarType.updateUI();
        if(treeTasks.getModel().getChildCount(treeTasks.getModel().getRoot())==1){
            treeTasks.expandPath(new TreePath(treeTasks.getModel().getRoot()));
        }
        updateItemScheduler();
        localizeHoursScheduler();
    }

    public static ArrayList<CheckNode> getCalendars() {
        return calendars;
    }
}
