package com.netcracker.edu.gr3.ui.render;

import com.netcracker.edu.gr3.model.Task;
import com.netcracker.edu.gr3.model.TaskPriority;

import javax.swing.*;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class TreeCellRendererTask extends JLabel implements TreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        ResourceBundle bundle = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.ui.treeModelTask", Locale.getDefault());
        if (value instanceof Task) {

            if (((Task) value).getPriority().equals(TaskPriority.BLOCKER)) {
                setIcon(new ImageIcon(ClassLoader.getSystemResource(
                        "com/netcracker/edu/gr3/images/blocker.jpg")));
            }
            if (((Task) value).getPriority().equals(TaskPriority.MAJOR)) {
                setIcon(new ImageIcon(ClassLoader.getSystemResource(
                        "com/netcracker/edu/gr3/images/major.jpg")));
            }
            if (((Task) value).getPriority().equals(TaskPriority.CRITICAL)) {
                setIcon(new ImageIcon(ClassLoader.getSystemResource(
                        "com/netcracker/edu/gr3/images/critical.jpg")));
            }
            if (((Task) value).getPriority().equals(TaskPriority.NORMAL)) {
                setIcon(new ImageIcon(ClassLoader.getSystemResource(
                        "com/netcracker/edu/gr3/images/normal.png")));
            }
            if (((Task) value).getPriority().equals(TaskPriority.LOW)) {
                setIcon(new ImageIcon(ClassLoader.getSystemResource(
                        "com/netcracker/edu/gr3/images/low.jpg")));
            }
            switch (((Task) value).getStatus()){
                case NEW:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("NEW")).toString());break;
                case PLANNING:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("PLANNING")).toString());break;
                case PLANNED:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("PLANNED")).toString());break;
                case IN_PROGRESS:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("IN_PROGRESS")).toString());break;
                case ON_HOLD:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("ON_HOLD")).toString());break;
                case DONE:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("DONE")).toString());break;
                case NOT_DONE:  setText(new StringBuilder(((Task) value).getName())
                        .append(": ")
                        .append(bundle.getString("NOT_DONE")).toString());break;
            }
        }
        setOpaque(true);
        if (selected) setBackground(Color.decode("#B0C4DE"));
        else setBackground(Color.WHITE);
        return this;
    }
}
