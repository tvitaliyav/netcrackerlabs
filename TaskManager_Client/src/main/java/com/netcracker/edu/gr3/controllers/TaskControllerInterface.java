package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Task;

import java.util.Collection;

/**
 * Interface "TaskControllerInterface" defines actions with task.
 *
 * @author Vitaliya Tikhonova
 * @version 1.0.0
 */
public interface TaskControllerInterface {
    /**
     * Method for creating a new task.
     *
     * @param task - task for creating
     * @return task
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Task createTask(Task task) throws TaskManagerException;

    /**
     * Method to get task by task ID .
     *
     * @param taskId - unique task id
     * @return Task
     * @throws TaskManagerException Inherited from class "Exception"
     */

    Task getTasksByTaskId(long taskId)
            throws TaskManagerException;

    /**
     * Method to get task by user ID and parent ID .
     *
     * @param userId   - unique user id
     * @param parentId - unique parent id
     * @return Collection of Tasks
     * @throws TaskManagerException Inherited from class "Exception"
     */

    Collection<Task> getTaskByUserIdAndParentId(long userId, long parentId)
            throws TaskManagerException;

    /**
     * Method to get tasks by parent ID.
     *
     * @param parentId - unique parent id
     * @return Collection of tasks
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Task> getTaskByParentId(long parentId)
            throws TaskManagerException;

    /**
     * Method for update task.
     *
     * @param task - new task
     * @return task
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Task updateTask(Task task) throws TaskManagerException;

    /**
     * Method for delete task.
     *
     * @param taskId - new task's ID
     * @throws TaskManagerException Inherited from class "Exception"
     */
    void deleteTask(long taskId) throws TaskManagerException;
}
