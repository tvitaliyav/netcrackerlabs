package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.PermissionControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Permission;

import java.util.Collection;

/**
 * Class "PermissionController" implements the
 * interface PermissionControllerInterface.
 *
 * @author Vitaliya Tikhonova, Ekaterina Denisova
 * @version 1.0.0
 */
public class PermissionController implements PermissionControllerInterface {
    /**
     * @inheritDoc
     */
    @Override
    public Permission setPermission(Permission permission)
            throws TaskManagerException {
        throw new TaskManagerException(ErrorsCodes.NOT_SUPPORTED_FOR_PERMISSION);
    }

    /**
     * @inheritDoc
     */

    @Override
    public Collection<Permission> getPermissionsByObjectTypeAndUserID(
            Class objectType, long userId) throws TaskManagerException {
        throw new TaskManagerException(ErrorsCodes.NOT_SUPPORTED_FOR_PERMISSION);
    }

    /**
     * @inheritDoc
     */

    @Override
    public Permission getPermissionByObjectTypeAndObjectID(
            Class objectType, long objectId) throws TaskManagerException {
        throw new TaskManagerException(ErrorsCodes.NOT_SUPPORTED_FOR_PERMISSION);
    }

}
