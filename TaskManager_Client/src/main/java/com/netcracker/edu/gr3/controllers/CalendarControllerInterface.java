package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Calendar;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

/**
 * Interface "CalendarControllerInterface" defines actions
 * with calendar.
 *
 * @author Olga Sudakova
 * @version 1.0.0
 */
public interface CalendarControllerInterface {

    /**
     * Method for creating a new calendar.
     *
     * @param calendar - calendar for creating
     * @return calendar
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Calendar createCalendar(Calendar calendar) throws TaskManagerException;

    /**
     * Method to get calendar by ID.
     *
     * @param calendarId - unique calendar's ID
     * @return calendar
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Calendar getCalendarById(long calendarId) throws TaskManagerException;

    /**
     * Method to get calendar list by user's ID.
     *
     * @param userId - unique user's ID
     * @return Collection of calendar
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Collection<Calendar> getCalendarsByUserId(long userId) throws TaskManagerException;

    /**
     * Method for update calendar.
     *
     * @param calendar - calendar for updatind
     * @return calendar
     * @throws TaskManagerException Inherited from class "Exception"
     */
    Calendar updateCalendar(Calendar calendar) throws TaskManagerException;

    /**
     * Method for delete calendar by ID.
     *
     * @param calendarId - unique calendar`s ID
     * @throws TaskManagerException Inherited from class "Exception"
     */
    void deleteCalendar(long calendarId) throws TaskManagerException;
}
