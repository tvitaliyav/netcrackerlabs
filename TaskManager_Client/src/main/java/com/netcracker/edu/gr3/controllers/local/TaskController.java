package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.TaskControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.DataStore;
import com.netcracker.edu.gr3.model.Task;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;


/**
 * Class "TaskController" implements the
 * interface TaskControllerInterface.
 *
 * @author Vitaliya Tikhonova, Ekaterina Denisova
 * @version 1.0.0
 */
@Component
public class TaskController implements TaskControllerInterface {
    /**
     * Object that provides work with DataStore.
     */
    @Autowired
    private DataStore dataStore;
    /**
     * Object that provides work with ApplicationControllerInterface.
     */
    @Autowired
    private ApplicationControllerInterface appControllerInterface;
    /**
     * Property is LOGGER.
     */
    private static final Logger LOGGER = Logger
            .getLogger(TaskController.class);
    /**
     * @inheritDoc
     */
    @Override
    public Task createTask(Task task) throws TaskManagerException {
        LOGGER.info("Method createTask called");
        if(task.getStartDate().isAfter(task.getEndDate())){
            throw new TaskManagerException(ErrorsCodes.INCORRECT_TASK_TIME_SPAN);
        }
        task.setTaskId(dataStore.getMap()
                .get(Task.class).generateID());
        LOGGER.debug("Task's ID was generated");
        dataStore.getTasks().add(task);
        LOGGER.info("Method createTask end");
        appControllerInterface.notifyObservers(new ObserverEvent());
        return task;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Task getTasksByTaskId(long taskId) throws TaskManagerException {
        try {
            LOGGER.info("Method getTasksByTaskId called");
            return dataStore.getTasks().stream()
                    .filter(task -> task.getTaskId() == taskId)
                    .findFirst().orElseThrow(() ->
                            new TaskManagerException(ErrorsCodes.
                                    TASK_NOT_FOUND_EXCEPTION));
        } finally {
            LOGGER.info("Method getTasksByTaskId end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Task> getTaskByUserIdAndParentId(long userId, long parentId) throws TaskManagerException {
        try {
            LOGGER.info("Method getTaskByUserIdAndParentId called");
            return dataStore.getTasks().stream()
                    .filter(task -> (task.getParentId() == parentId
                            &&task.getUserId()==userId))
                    .collect(Collectors.toCollection(ArrayList::new));
        } finally {
            LOGGER.info("Method getTaskByUserIdAndParentId end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Collection<Task> getTaskByParentId(long parentId) throws TaskManagerException {
        try {
            LOGGER.info("Method getTaskByParentId called");
            return dataStore.getTasks().stream()
                    .filter(task -> task.getParentId() == parentId)
                    .collect(Collectors.toCollection(ArrayList::new));
        } finally {
            LOGGER.info("Method getTaskByParentId end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public Task updateTask(Task task) throws TaskManagerException {
        LOGGER.info("Method updateTask called");
        if(task.getStartDate().isAfter(task.getEndDate())){
            throw new TaskManagerException(ErrorsCodes.INCORRECT_TASK_TIME_SPAN);
        }
        Task updatedTask = getTasksByTaskId(task.getTaskId());
        LOGGER.debug("Change name");
        updatedTask.setName(task.getName());
        LOGGER.debug("Change description");
        updatedTask.setDescription(task.getDescription());
        LOGGER.debug("Change priority");
        updatedTask.setPriority(task.getPriority());
        LOGGER.debug("Change status");
        updatedTask.setStatus(task.getStatus());
        LOGGER.debug("Change start date");
        updatedTask.setStartDate(task.getStartDate());
        LOGGER.debug("Change due date");
        updatedTask.setDueDate(task.getDueDate());
        LOGGER.debug("Change end date");
        updatedTask.setEndDate(task.getEndDate());
        LOGGER.debug("Change estimation execution time");
        updatedTask.setEstimationExecutionTime(task.getEstimationExecutionTime());
        LOGGER.debug("Change parent Id");
        updatedTask.setParentId(task.getParentId());
        LOGGER.info("Method updateTask end");
        appControllerInterface.notifyObservers(new ObserverEvent());
        return updatedTask;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteTask(long taskId) throws TaskManagerException {
        LOGGER.info("Method deleteTask called");
        dataStore.getTasks()
                .removeIf(task -> task.getTaskId() == taskId);
        appControllerInterface.notifyObservers(new ObserverEvent());
        LOGGER.info("Method deleteTask end");
    }
}
