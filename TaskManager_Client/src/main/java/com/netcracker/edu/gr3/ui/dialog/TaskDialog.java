package com.netcracker.edu.gr3.ui.dialog;

import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.ui.model.TaskPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class TaskDialog extends JDialog {
    public static final String CREATE_TASK = "Create Task";
    public static final String CHANGE_TASK = "Change Task";
    private String action;
    private long taskId;


    public TaskDialog(String action) {
        try {
            if(action.equals(CREATE_TASK)) {
                new TaskDialog(action, 0).setVisible(true);
            }
            else throw new TaskManagerException(ErrorsCodes.INCORRECT_ACTION_IN_TASK);
        }
        catch (TaskManagerException e){
            handleException(e);
        }
    }

    public TaskDialog(String action,long taskId) {
        this.action = action;
        this.taskId = taskId;
        setModal(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        localizeView();
        JPanel taskPanel = new TaskPanel(action,taskId);
        add(taskPanel);
        ((TaskPanel) taskPanel).getButtonCancel().addActionListener(e -> dispose());
        ((TaskPanel) taskPanel).getButtonCreateTask().addActionListener(e -> dispose());
    }
    private void handleException(TaskManagerException exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        JOptionPane.showMessageDialog(this,
                bundleError.getString(
                        String.valueOf(exception.getErrorCode())));

    }
    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.taskDialog", Locale.getDefault());
        if ((CREATE_TASK).equals(action)) setTitle(bundle.getString("window.title"));
        else setTitle(bundle.getString("window.title.two"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
    }
}
