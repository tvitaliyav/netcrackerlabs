package com.netcracker.edu.gr3.ui.dialog;

import com.mindfusion.common.Convert;
import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.ContactControllerInterface;
import com.netcracker.edu.gr3.controllers.UserControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.Contact;
import com.netcracker.edu.gr3.model.ContactType;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import com.netcracker.edu.gr3.util.ControllerProvider;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

import static javax.swing.JTable.AUTO_RESIZE_OFF;

public class FindUserDialog extends JDialog {
    private static final int COLUMNS_TEXT_FIELD = 20;

    private JLabel labelError;
    private JLabel labelUsername;
    private JLabel labelFirstName;
    private JLabel labelLastName;
    private JLabel labelDateOfBirth;
    private JLabel labelPhoneNumber;
    private JLabel labelEmail;
    private JLabel labelAge;
    private JButton buttonAddOfFriends;
    private JButton buttonCancel;
    private JButton buttonAddInBlackList;
    private JButton buttonOK;
    private JPanel findUserPanel;
    private Vector<String> columnParameters;
    private Vector<Vector<String>> researchUsers;
    private JTable tableUsers;
    private String[] typeContacts;

    public FindUserDialog() {
        try {
            setModal(true);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            findUserPanel = new JPanel(new MigLayout());
            JPanel panelInfo = new JPanel(new MigLayout());
            labelUsername = new JLabel();
            JTextField textUsername = new JTextField(COLUMNS_TEXT_FIELD);
            labelFirstName = new JLabel();
            JTextField textFirstName = new JTextField(COLUMNS_TEXT_FIELD);
            labelLastName = new JLabel();
            JTextField textLastName = new JTextField(COLUMNS_TEXT_FIELD);
            labelDateOfBirth = new JLabel();
            labelPhoneNumber = new JLabel();
            JTextField textPhoneNumber = new JTextField(COLUMNS_TEXT_FIELD);
            labelEmail = new JLabel();
            JTextField textEmail = new JTextField(COLUMNS_TEXT_FIELD);
            labelAge= new JLabel();
            JComboBox<String> comboBoxAge = new JComboBox<>(new String[]{"-","1-15","16-25","26-35","36-45","46-55","56-65","66-80","80-110"});
            JPanel panelAge = new JPanel(new MigLayout());
            buttonAddOfFriends = new JButton();
            buttonCancel = new JButton();
            buttonOK = new JButton();
            buttonAddInBlackList = new JButton();
            labelError = new JLabel();
            JPanel panelUsers = new JPanel(new MigLayout());
            researchUsers = new Vector<>();
            Vector<User> vectorUsers = new Vector<>(ControllerProvider.getProvider().
                    getController(UserControllerInterface.class).getAllUsers());
            vectorUsers.remove(ControllerProvider.getProvider().
                    getController(UserControllerInterface.class).getUserById(ControllerProvider.getProvider().
                    getController(ApplicationControllerInterface.class).getCurrentUser()));
            columnParameters = new Vector<>();
            for (int i = 0; i < 7; i++) {
                columnParameters.add("");
            }
            localizeView();
            inputResearchUser(vectorUsers);
            tableUsers = new JTable(researchUsers, columnParameters) {
                @Override
                public boolean isCellEditable(int i, int i1) {
                    return false;
                }
            };
            for (int i = 0; i < tableUsers.getColumnCount(); i++) {
                tableUsers.getColumnModel().getColumn(i).setPreferredWidth(160);
            }
            tableUsers.setPreferredScrollableViewportSize(
                    tableUsers.getPreferredSize());
            tableUsers.setAutoResizeMode(AUTO_RESIZE_OFF);
            tableUsers.setDragEnabled(false);
            JScrollPane paneTableUsers = new JScrollPane(tableUsers,
                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            labelError.setForeground(Color.RED);
            JPanel panelButton = new JPanel();
            panelButton.setLayout(new MigLayout());
            panelInfo.add(labelUsername);
            panelInfo.add(labelFirstName);
            panelInfo.add(labelLastName);
            panelInfo.add(labelPhoneNumber);
            panelInfo.add(labelEmail, "wrap");
            panelInfo.add(textUsername);
            panelInfo.add(textFirstName);
            panelInfo.add(textLastName);
            panelInfo.add(textPhoneNumber);
            panelInfo.add(textEmail, "wrap");
            panelAge.add(labelAge);
            panelAge.add(comboBoxAge);
            panelInfo.add(panelAge);
            panelInfo.add(buttonOK, "wrap");
            panelInfo.add(labelError);
            findUserPanel.add(panelInfo, "dock north, wrap");
            panelUsers.add(tableUsers.getTableHeader(), "wrap, pushx, growx");
            panelUsers.add(paneTableUsers, "pushx, growx,pushy,growy");
            findUserPanel.add(panelUsers, "dock center,wrap");
            panelButton.add(buttonAddOfFriends);
            panelButton.add(buttonAddInBlackList);
            panelButton.add(buttonCancel);
            findUserPanel.add(panelButton, "south");
            add(findUserPanel);
            buttonAddOfFriends.addActionListener((e -> SwingUtilities.invokeLater(() -> {
                try {
                    int numberSelectedRow = tableUsers.getSelectedRow();
                    for (User user : vectorUsers) {
                        if (user.getLogin().equals(tableUsers.getModel()
                                .getValueAt(numberSelectedRow, 0))
                                && ControllerProvider.getProvider().
                                getController(ContactControllerInterface.class)
                                .getContactByUsers(ControllerProvider.getProvider()
                                        .getController(ApplicationControllerInterface.class)
                                        .getCurrentUser(), user.getUserId())
                                .getContactType()!= ContactType.FRIEND) {
                            ControllerProvider.getProvider().
                                    getController(ContactControllerInterface.class)
                                    .createContact(new Contact(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser(), user.getUserId(),
                                            ContactType.FRIEND_REQUEST));
                            break;
                        }
                    }
                } catch (TaskManagerException e1) {
                    handleException(e1);
                }
                dispose();

            })));
            buttonAddInBlackList.addActionListener((e -> SwingUtilities.invokeLater(() -> {
                try {
                    int numberSelectedRow = tableUsers.getSelectedRow();
                    for (User user : vectorUsers) {
                        if (user.getLogin().equals(tableUsers.getModel().
                                getValueAt(numberSelectedRow, 0))) {
                            ControllerProvider.getProvider().
                                    getController(ContactControllerInterface.class).
                                    createContact(new Contact(
                                    ControllerProvider.getProvider()
                                            .getController(ApplicationControllerInterface.class)
                                            .getCurrentUser(), user.getUserId(),
                                            ContactType.BLACK_LIST));
                            break;
                        }
                    }
                } catch (TaskManagerException e1) {
                    handleException(e1);
                }
                dispose();
            })));
            buttonCancel.addActionListener(e -> SwingUtilities.invokeLater(() -> {
                dispose();
            }));

            buttonOK.addActionListener((e -> SwingUtilities.invokeLater(() -> {
                Vector<User> vectorUsersSearch = (Vector<User>) vectorUsers.clone();
                if (!textUsername.getText().equals("")) {
                    for (int i = vectorUsersSearch.size() - 1; i >= 0; i--) {
                        if (!vectorUsersSearch.get(i).getLogin().
                                startsWith(textUsername.getText())) {
                            vectorUsersSearch.remove(i);
                        }
                    }
                }

                if (!textFirstName.getText().equals("")) {
                    for (int i = vectorUsersSearch.size() - 1; i >= 0; i--) {
                        if (!vectorUsersSearch.get(i).getFirstName().
                                startsWith(textFirstName.getText())) {
                            vectorUsersSearch.remove(i);
                        }
                    }
                }
                if (!textLastName.getText().equals("")) {
                    for (int i = vectorUsersSearch.size() - 1; i >= 0; i--) {
                        if (!vectorUsersSearch.get(i).getLastName().
                                startsWith(textLastName.getText())) {
                            vectorUsersSearch.remove(i);
                        }
                    }
                }
                if (!textPhoneNumber.getText().equals("")) {
                    for (int i = vectorUsersSearch.size() - 1; i >= 0; i--) {
                        if (!vectorUsersSearch.get(i).getPhoneNumber().
                                startsWith(textPhoneNumber.getText())) {
                            vectorUsersSearch.remove(i);
                        }
                    }
                }
                if (!textEmail.getText().equals("")) {
                    for (int i = vectorUsersSearch.size() - 1; i >= 0; i--) {
                        if (!vectorUsersSearch.get(i).getEmail().
                                startsWith(textEmail.getText())) {
                            vectorUsersSearch.remove(i);
                        }
                    }
                }
                if (!(comboBoxAge.getSelectedIndex()==0)) {
                    String[] age = Objects.requireNonNull(comboBoxAge.getSelectedItem()).toString().split("-");
                    for (int i = vectorUsersSearch.size() - 1; i >= 0; i--) {
                        int yearsUser = Period.between(vectorUsersSearch.get(i).getDateOfBirth(), LocalDate.now()).getYears();
                        if (!(yearsUser>= Convert.toInt16(age[0])
                                &&yearsUser<= Convert.toInt16(age[1]))) {
                            vectorUsersSearch.remove(i);
                        }
                    }
                }
                try {
                    inputResearchUser(vectorUsersSearch);
                } catch (TaskManagerException e1) {
                    handleException(e1);
                }
                        localizeView();
                    }
            )));

        } catch (TaskManagerException e) {
            handleException(e);
        }
    }

    private void handleException(Exception exception) {
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        labelError.setText(bundleError.getString(
                String.valueOf(((TaskManagerException) exception)
                        .getErrorCode())));

    }

    private void inputResearchUser(Vector<User> vectorUsers) throws TaskManagerException {
        researchUsers.removeAllElements();
        for (User user : vectorUsers) {
            String contactTypeString = "";
            ContactType contactType = ContactType.NONE;
            try {
                contactType = ControllerProvider.getProvider().
                        getController(ContactControllerInterface.class)
                        .getContactByUsers(ControllerProvider.getProvider()
                        .getController(ApplicationControllerInterface.class)
                        .getCurrentUser(), user.getUserId()).getContactType();
            } catch (NoSuchElementException|NullPointerException e) {
                contactTypeString = typeContacts[3];
            }
            switch (contactType) {
                case FRIEND:
                    contactTypeString = typeContacts[0];
                    break;
                case FRIEND_REQUEST:
                    contactTypeString = typeContacts[1];
                    break;
                case BLACK_LIST:
                    contactTypeString = typeContacts[2];
                    break;
                case NONE:
                    contactTypeString = typeContacts[3];
                    break;
            }
            String[] array = {user.getLogin(), user.getFirstName(),
                    user.getLastName(), user.getDateOfBirth().toString(),
                    user.getPhoneNumber(), user.getEmail(), contactTypeString};
            Vector<String> vector = new Vector<>();
            Collections.addAll(vector, array);
            researchUsers.add(vector);
        }
    }

    private void localizeView() {
        ResourceBundle bundle = ResourceBundle.getBundle
                ("com.netcracker.edu.gr3.ui.findUserForm", Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        findUserPanel.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        columnParameters.setElementAt(bundle.getString("columnParameters.login"), 0);
        columnParameters.setElementAt(bundle.getString("columnParameters.firstName"), 1);
        columnParameters.setElementAt(bundle.getString("columnParameters.lastName"), 2);
        columnParameters.setElementAt(bundle.getString("columnParameters.dateOfBirth"), 3);
        columnParameters.setElementAt(bundle.getString("columnParameters.phoneNumber"), 4);
        columnParameters.setElementAt(bundle.getString("columnParameters.email"), 5);
        columnParameters.setElementAt(bundle.getString("columnParameters.contactType"), 6);
        labelUsername.setText(bundle.getString("labelUsername.name"));
        labelFirstName.setText(bundle.getString("labelFirstName.name"));
        labelLastName.setText(bundle.getString("labelLastName.name"));
        labelDateOfBirth.setText(bundle.getString("labelDateOfBirth.name"));
        labelPhoneNumber.setText(bundle.getString("labelPhoneNumber.name"));
        labelEmail.setText(bundle.getString("labelEmail.name"));
        labelAge.setText(bundle.getString("labelAge.name"));
        buttonAddOfFriends.setText(bundle.getString("buttonAddOfFriends.name"));
        buttonAddOfFriends.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonAddOfFriends.width")),
                Integer.parseInt(bundle.getString("buttonAddOfFriends.height"))));
        buttonAddInBlackList.setText(bundle.getString("buttonAddInBlackList.name"));
        buttonAddInBlackList.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonAddInBlackList.width")),
                Integer.parseInt(bundle.getString("buttonAddInBlackList.height"))));
        buttonOK.setText(bundle.getString("buttonOK.name"));
        buttonOK.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonOK.width")),
                Integer.parseInt(bundle.getString("buttonOK.height"))));
        buttonCancel.setText(bundle.getString("buttonCancel.name"));
        buttonCancel.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonCancel.width")),
                Integer.parseInt(bundle.getString("buttonCancel.height"))));
        typeContacts = new String[]{bundle.getString("contactType.friends"),
                bundle.getString("contactType.friendsRequest"),
                bundle.getString("contactType.blackList"),
                bundle.getString("contactType.none")};
        labelError.setText("");
        validate();
    }
}
