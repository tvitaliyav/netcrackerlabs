package com.netcracker.edu.gr3.controllers;

import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.model.User;
import com.netcracker.edu.gr3.observer.Observable;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Interface " ApplicationControllerInterface" defines actions with application.
 *
 * @author Vitaliya Tikhonova
 * @version 1.0.0
 */
public interface ApplicationControllerInterface extends Observable {
    /**
     * Method for close application.
     *
     * @throws TaskManagerException Inherited from class "Exception"
     */
    void closeApp() throws TaskManagerException;

    /**
     * Method for change user.
     *
     * @param userId new current user's ID
     * @throws TaskManagerException Inherited from class "Exception"
     */
    void setCurrentUser(long userId) throws TaskManagerException;

    /**
     * Method for get current user.
     *
     * @return current user's ID
     * @throws TaskManagerException Inherited from class "Exception"
     */
    long getCurrentUser() throws TaskManagerException;
}
