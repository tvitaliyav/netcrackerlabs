package com.netcracker.edu.gr3.controllers.local;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.observer.Observer;
import com.netcracker.edu.gr3.observer.ObserverEvent;
import com.netcracker.edu.gr3.util.ModelFacade;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class "ApplicationController" implements
 * the interface ApplicationControllerInterface.
 *
 * @author Ekatherina Denisova, Olga Sudakova, Vitaliya Tikhonova
 * @version 1.0.0
 */
@Component
public class ApplicationController implements ApplicationControllerInterface {
    /**
     * User that currently active now.
     */
    private long currentUserId;
    /**
     * List of observers who want to be notified of events.
     */
    private Collection<Observer> observers;
    /**
     * Object that provides work with DataStore.
     */
    @Autowired
    private ModelFacade modelFacade;
    /**
     * Property is LOGGER.
     */
    private static final Logger LOGGER = Logger
            .getLogger(ApplicationController.class);

    /**
     * Default constructor - сreates a new object.
     */
    public ApplicationController() {
        LOGGER.info("Create ApplicationController object");
        observers = new ArrayList<>();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void closeApp() throws TaskManagerException {
        LOGGER.info("Method closeApp called");
        currentUserId = 0;
        observers.clear();
        modelFacade.closeModel();
        LOGGER.info("Method closeApp end");
    }

    /**
     * @inheritDoc
     */
    @Override
    public void setCurrentUser(long userId) throws TaskManagerException {
        LOGGER.info("Method setCurrentUser called");
        currentUserId = userId;
        LOGGER.info("Method setCurrentUser end");
    }

    /**
     * @inheritDoc
     */
    @Override
    public long getCurrentUser() throws TaskManagerException {
        try {
            LOGGER.info("Method getCurrentUser called");
            return currentUserId;
        } finally {
            LOGGER.info("Method getCurrentUser end");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void addObserver(Observer observer) {
        LOGGER.info("Method addObserver called");
        observers.add(observer);
        LOGGER.info("Method addObserver end");
    }

    /**
     * @inheritDoc
     */
    @Override
    public void removeObserver(Observer observer) {
        LOGGER.info("Method removeObserver called");
        observers.remove(observer);
        LOGGER.info("Method removeObserver end");
    }

    /**
     * @inheritDoc
     */
    @Override
    public void notifyObservers(ObserverEvent event) {
        LOGGER.info("Method notifyObservers called");
        observers.forEach(observer -> observer.notify(event));
        LOGGER.info("Method notifyObservers end");
    }
}
