package com.netcracker.edu.gr3.ui;

import com.netcracker.edu.gr3.controllers.ApplicationControllerInterface;
import com.netcracker.edu.gr3.controllers.UserControllerInterface;
import com.netcracker.edu.gr3.exception.ErrorsCodes;
import com.netcracker.edu.gr3.exception.TaskManagerException;
import com.netcracker.edu.gr3.ui.model.LanguageComboBox;
import com.netcracker.edu.gr3.util.ControllerProvider;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.Label;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.Component;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class is needed to create LoginForm.
 *
 * @author Denisova Ekaterina
 * @version 1.0.
 */
public class LoginForm extends JFrame {
    /**
     * Property is LOGGER.
     *
     * @see Logger .
     */
    private static final Logger LOGGER = Logger
            .getLogger(LoginForm.class);
    /**
     * Property is ROWS_PANEL_INFO.
     */
    private static final int ROWS_PANEL_INFO = 2;
    /**
     * Property is COLS_PANEL_INFO.
     */
    private static final int COLS_PANEL_INFO = 2;
    /**
     * Property is loginFormPanel.
     *
     * @see JPanel .
     */
    private JPanel loginFormPanel;
    /**
     * Property is labelUsername.
     *
     * @see JLabel .
     */
    private JLabel labelUsername;
    /**
     * Property is textUsername.
     *
     * @see JTextField .
     */
    private JTextField textUsername;
    /**
     * Property is labelPassword.
     *
     * @see JLabel .
     */
    private JLabel labelPassword;
    /**
     * Property is passwordField.
     *
     * @see JPasswordField .
     */
    private JPasswordField passwordField;
    /**
     * Property is labelError.
     *
     * @see JLabel .
     */
    private JLabel labelError;
    /**
     * Property is buttonSignIn.
     *
     * @see JButton .
     */
    private JButton buttonSignIn;
    /**
     * Property is buttonRegister.
     *
     * @see JButton .
     */
    private JButton buttonRegister;
    /**
     * Property is buttonExit.
     *
     * @see JButton .
     */
    private JButton buttonExit;

    /**
     * Create new object.
     *
     * @see LoginForm#LoginForm().
     */
    public LoginForm() {
        LOGGER.info("Create LoginForm object called");
        //Setting LoginForm
        setResizable(false);
        //Setting fields
        loginFormPanel = new JPanel();
        loginFormPanel.setLayout( new BoxLayout(loginFormPanel,
                BoxLayout.Y_AXIS));
        JPanel panelLang = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JComboBox comboBoxLang = null;
        try {
            comboBoxLang = new LanguageComboBox();
        } catch (TaskManagerException e) {
            handleException(e);
        }
        JPanel panelInfo = new JPanel(new GridLayout(
                ROWS_PANEL_INFO, COLS_PANEL_INFO));
        labelUsername = new JLabel();
        textUsername = new JTextField();
        labelPassword = new JLabel();
        passwordField = new JPasswordField();
        JPanel panelError= new JPanel();
        panelError.setLayout(new BoxLayout(panelError, BoxLayout.X_AXIS));
        labelError = new JLabel();
        labelError.setForeground(Color.RED);
        buttonRegister = new JButton();
        buttonRegister.setAlignmentX(Component.CENTER_ALIGNMENT);
        JPanel panelButton = new JPanel();
        panelButton.setLayout(new FlowLayout(FlowLayout.CENTER));
        panelButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        JPanel panelButtons = new JPanel();
        panelButtons.setLayout(new BoxLayout(panelButtons,
                BoxLayout.Y_AXIS));
        buttonSignIn = new JButton();
        buttonExit = new JButton();
        localizeView();
        labelError.setAlignmentX(Component.LEFT_ALIGNMENT);
        //Adding fields on LoginForm
        panelLang.add(comboBoxLang);
        loginFormPanel.add(panelLang);
        panelInfo.add(labelUsername);
        panelInfo.add(textUsername);
        panelInfo.add(labelPassword);
        panelInfo.add(passwordField);
        loginFormPanel.add(panelInfo);
        panelError.add(labelError);
        panelError.add(Box.createHorizontalGlue());
        loginFormPanel.add(panelError);
        panelButton.add(buttonSignIn);
        panelButton.add(buttonExit);
        panelButtons.add(panelButton);
        panelButtons.add(buttonRegister);
        loginFormPanel.add(panelButtons);
        add(loginFormPanel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        buttonRegister.addActionListener(e ->
                SwingUtilities.invokeLater(() -> {
                    new RegisterForm().setVisible(true);
                    dispose();
                }));
        buttonExit.addActionListener(e -> {
            try {
                ControllerProvider.getProvider().
                        getController(ApplicationControllerInterface
                                .class).closeApp();
                SwingUtilities.invokeLater(this::dispose);
                System.exit(0);
            } catch (TaskManagerException exception) {
                LOGGER.error(exception.getMessage());
                handleException(exception);
            } catch (Exception exception) {
                LOGGER.error(exception.getMessage());
                handleException(new TaskManagerException(
                        ErrorsCodes.UNKNOWN));
            }
        });
        buttonSignIn.addActionListener(e -> {
            try {
                StringBuilder password = new StringBuilder();
                for(char pas: passwordField.getPassword()){
                    password.append(pas);
                }
                ControllerProvider.getProvider().
                        getController(ApplicationControllerInterface.class).
                        setCurrentUser(ControllerProvider.getProvider().
                                getController(UserControllerInterface.class).
                                auth(textUsername.getText(), password.toString()).getUserId());
                new MainForm().setVisible(true);
                dispose();
            } catch (TaskManagerException exception) {
                LOGGER.error(exception.getMessage());
                handleException(exception);
            }
            catch (Exception ex){
                LOGGER.error(ex.getMessage());
                handleException(new TaskManagerException(
                        ErrorsCodes.UNKNOWN));
            }
        });
        comboBoxLang.addActionListener(event -> localizeView());
        LOGGER.info("Create LoginForm object end");
    }

    /**
     * This method handles errors by substituting
     * them into a labelError.
     *
     * @param exception is Exception .
     */
    private void handleException(TaskManagerException exception) {
        LOGGER.info("Method handleException called");
        ResourceBundle bundleError = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.error.error", Locale.getDefault());
        LOGGER.error(exception.getMessage(), exception);
        labelError.setText(bundleError.getString(
                String.valueOf(exception.getErrorCode())));
        LOGGER.info("Method handleException end");
    }

    /**
     * This method localize view in LoginForm.
     */
    private void localizeView() {
        LOGGER.info("Method localizeView called");
        ResourceBundle bundle = ResourceBundle.getBundle(
                "com.netcracker.edu.gr3.ui.loginForm", Locale.getDefault());
        setTitle(bundle.getString("window.title"));
        int windowWidth = Integer.parseInt(bundle.getString("window.width"));
        int windowHeight = Integer.parseInt(bundle.getString("window.height"));
        setSize(windowWidth, windowHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int locationX = (screenSize.width - windowWidth) / 2;
        int locationY = (screenSize.height - windowHeight) / 2;
        setBounds(locationX, locationY, windowWidth, windowHeight);
        loginFormPanel.setBorder(BorderFactory.createEmptyBorder(
                Integer.parseInt(bundle.getString("window.border.top")),
                Integer.parseInt(bundle.getString("window.border.left")),
                Integer.parseInt(bundle.getString("window.border.bottom")),
                Integer.parseInt(bundle.getString("window.border.right"))));
        labelUsername.setText(bundle.getString("labelUsername.name"));
        labelPassword.setText(bundle.getString("labelPassword.name"));
        buttonSignIn.setText(bundle.getString("buttonSignIn.name"));
        buttonSignIn.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonSignIn.width")),
                Integer.parseInt(bundle.getString("buttonSignIn.height"))));
        buttonRegister.setText(bundle.getString("buttonRegister.name"));
        buttonRegister.setMaximumSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonRegister.width")),
                Integer.parseInt(bundle.getString("buttonRegister.height"))));
        buttonExit.setText(bundle.getString("buttonExit.name"));
        buttonExit.setPreferredSize(new Dimension(
                Integer.parseInt(bundle.getString("buttonExit.width")),
                Integer.parseInt(bundle.getString("buttonExit.height"))));
        labelError.setText(" ");
        LOGGER.info("Method localizeView end");
    }

    /**
     * This method is main method.
     *
     * @param args is String[]
     */
    public static void main(String[] args) {
        LOGGER.info("Method main called");
        SwingUtilities.invokeLater(() -> new LoginForm().setVisible(true));
        LOGGER.info("Method main end");
    }
}
